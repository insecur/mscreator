#!/bin/bash
new_year=2017
author="Tobias Hommel"

files=( mscreate LICENSE doc/create_module_stubs.py )
files+=( $(find mscreator tests -name '*.py') )

sed \
    -e "s/^# Copyright (c) \(....\)-\(....\), ${author}/# Copyright (c) \1-${new_year}, ${author}/g" \
    -e "s/^# Copyright (c) \(....\), ${author}/# Copyright (c) \1-${new_year}, ${author}/g" \
    -e "s/^# Copyright (c) ${new_year}-${new_year}, ${author}/# Copyright (c) ${new_year}, ${author}/g" \
    -i ${files[*]}

sed -e "s/^copyright.*$/copyright = u'2013-${new_year}, Tobias Hommel'/g" -i doc/conf.py
