#!/bin/bash
# vim: set ts=8 sts=8 sw=8 smarttab noexpandtab :
PATH=$PATH:/usr/local/bin

TMPDIR=./.nosetests_temp
mkdir -p $TMPDIR
mount -t tmpfs tmpfs $TMPDIR || {
	echo "could not mount temp dir, running as root?"
	exit 1
}

exit_handler() {
	umount $TMPDIR
}

trap exit_handler EXIT TERM HUP INT QUIT

pyversions="${pyversions:-3.4 3.5 3.6}"
for i in $pyversions;do
	eval "start_time_${i/./_}=$(date +%s.%N)"
	if ! which nosetests-$i &>/dev/null;then
		eval "stop_time_${i/./_}=$(date +%s.%N)"
		continue
	fi

	report_file=results/$i/report.html
	cover_dir=results/$i/cover
	mkdir -p $cover_dir
	if $(python$i -c 'import nosetimer' &>/dev/null);then
		timer_args='--with-timer --timer-ok=300ms --timer-top-n=10'
	else
		timer_args=''
	fi
	TMP=$TMPDIR skipslow=$skipslow nosetests-$i \
		$timer_args \
		-s \
		--with-coverage3 --cover3-html --cover3-erase -v --cover3-package=mscreator --cover3-html-dir=$cover_dir --cover3-branch \
		--with-html --html-file=$report_file #&>/dev/null
#		--processes=-1 \
	#--with-doctest
	eval "rc_${i/./_}=$?"
	eval "stop_time_${i/./_}=$(date +%s.%N)"
	echo '=============================================================================='
done

echo 'Results for python version (version: runtime in seconds => result)'
exitcode=0
for i in $pyversions;do
	echo -n " * $i: "
	eval rc=\$rc_${i/./_}
	eval start_time=\$start_time_${i/./_}
	eval stop_time=\$stop_time_${i/./_}
	if [ -z $rc ];then
		str_result='not available'
	elif [ "$rc" != 0 ];then
		str_result=ERRORS
		exitcode=1
	else
		str_result=OK
	fi
	echo -n $stop_time $start_time | awk '{printf "%f ", $1-$2}'
	echo " => $str_result"
done
exit $exitcode
