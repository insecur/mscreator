.. _coding-style:

Coding Style
============

Docstrings
----------

Here is an example for how to document a simple function..

.. code-block:: python
    :linenos:
    
    def path_function(input_path, counter=None):
        """This is some text describing the functionality of the documented function/class.
    
        The description may also contain code examples:
        >>> x = path_function("/some/path")
        >>> print(x)
        "some other path"
    
    
        Following is the description of parameters, return value and possibly raised exceptions.
         * Type hints for parameters and the return value are mandatory.
         * Descriptions of parameters and the return value are not mandatory and may be omitted if the description above is
           detailed enough or the name of the parameter is self-explanatory.
         * Exception (raise) specifications are mandatory.
         * Continuation lines must be indented by 4 spaces.
    
        :param input_path: The full path to the input file. The path must be absolute and must already be fully expanded, i.e.
            it must not contain variables or ~/~user constructions.
        :type input_path: str
        :param counter: Does something funny. Is ignored if None.
        :type counter: int|None
        :return: some other path
        :rtype: str
        :raise PathNotFound: if input_path does not exist
        """
        return "some other path"
