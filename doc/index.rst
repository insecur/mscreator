Welcome to the API documentation of MSCreator
=============================================

This currently only includes some automatically created source code
documentation but will hopefully be enhanced over time.

Contents
--------

 * build instructions
 * architecture
 * :ref:`coding-style`
 * some more stuff
 * even more stuff

Modules
-------

.. toctree::
        :hidden:
        :maxdepth: 2
        :glob:

        static/*
        modules/*

.. include:: autosummary.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
