#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os


def list_modules(path):
	if path[-1] == '/':
		path = path[:-1]
	package_name = os.path.basename(path)
	pathlen = len(path) + 1
	for root_dir, subdirs, files in os.walk(path):
		for filename in sorted(files):
			if filename.endswith('.py'):
				pyname = '%s.%s' % (package_name, os.path.join(root_dir, filename)[pathlen:-3])
				pyname = pyname.translate({ord('/'): '.'})
				if pyname.endswith('.__init__'):
					yield (pyname[:-9], True)
				else:
					yield (pyname, False)


def main():
	autosummary_lines = ['.. autosummary::\n']
	if not os.path.exists('modules'):
		os.makedirs('modules')
	for modname, ispackage in list_modules('../mscreator'):
		rst_path = os.path.join('modules', modname+'.rst')
		with open(rst_path, 'w') as fd:
			if ispackage:
				mname = modname+'.__init__'
				mtype = 'Package'
				autosummary_lines.append('\t%s.__init__\n' % modname)
			else:
				mname = modname
				mtype = 'Module'
				autosummary_lines.append('\t%s\n' % modname)
			fd.writelines([
				':mod:`%s` %s\n' % (modname, mtype),
				'%s\n' % ('-'*(8 + len(modname)+ len(mtype))),
				'\n',
				'.. automodule:: %s\n' % mname,
				'\t:members:\n',
				'\t:undoc-members:\n',
				'\t:show-inheritance:\n',
			])

	with open('autosummary.rst', 'w') as fd:
		fd.writelines(autosummary_lines)


if __name__ == '__main__':
	main()
