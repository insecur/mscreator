#!/bin/bash
# vim: set ts=8 sts=8 sw=8 smarttab noexpandtab :
PATH="$PATH:/usr/local/bin"
ERR=1

for pylint_binary in $(which pylint-3 2>/dev/null) $(which python3-pylint 2>/dev/null) /usr/lib/python-exec/python3.*/pylint;do
	[ -x "${pylint_binary}" ] || continue
	PYLINT_EXE="${pylint_binary}"
	break
done

if [ -z "${PYLINT_EXE}" ];then
    echo "Could not find a suitable pylint binary!"
    exit 1
fi
"${PYLINT_EXE}" --rcfile=.pylintrc mscreate mscreator | tee pylint.report ; ERR="${PIPESTATUS[0]}"

exit "${ERR}"
