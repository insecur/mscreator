# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module contains functions and classes for handling MSC configuration files."""
import os
import weakref
from abc import ABCMeta

from .exceptions import PathNotFoundError, MSCError


class ConfigError(MSCError):
    """Raised if an error occurs while parsing an msc configuration file.
    The error can either be syntactical, e.g. too few "::" in a section header,
    or a semantic error, e.g. a section required by another section is missing."""

    def __init__(self, message, location=None, strip=-1, exact=True):
        if location is None:
            pass
        elif hasattr(location, 'configline'):
            message += ' %s' % get_config_traceback_string(location.configline, exact=exact)
        elif hasattr(location, 'configfile'):
            message += ' %s' % get_config_traceback_string(location, exact=exact)
        else:
            message += ' "%s"' % location
        super().__init__(message, strip)


class ConfigValue(object):
    """This is the base class for entries in a msc config file."""
    __metaclass__ = ABCMeta

    def __init__(self, configline):
        super().__init__()
        self._configline = weakref.ref(configline)

    @property
    def configline(self):
        """The ConfigFileLine object from which this value was created.

        :rtype: ConfigFileLine
        """
        try:
            return self._configline()
        except TypeError:
            return None


class ConfigRaw(ConfigValue):
    """A plain string in a config file."""

    def __repr__(self):  # pragma: no cover
        return 'ConfigValue("%s")' % self.configline.content


class ConfigEmpty(ConfigValue):
    """Represents an empty line in a config file."""

    def __repr__(self):  # pragma: no cover
        return 'ConfigEmpty()'


class ConfigComment(ConfigValue):
    """Represents a comment in a config file."""

    def __init__(self, configline):
        super().__init__(configline)
        self._value = configline.content[1:]
        self._comment_char = configline.content[0]

    def __repr__(self):  # pragma: no cover
        return 'ConfigComment("%s")' % self._value


class KeyValueEntry(ConfigValue):
    """Represents a key=value pair in a config file."""

    def __init__(self, configline, key, value):
        super().__init__(configline)
        self.key = key
        self.value = value

    def __repr__(self):  # pragma: no cover
        return '%s=%s' % (self.key, self.value)


class ConfigFileLine(object):
    """A wrapper for a line of a config file.
    Records some data important for refering to the file this line belongs to, e.g. line number.

    :param content: the raw data of the line
    :type content: str
    :param lineno: the line number in the file this line belongs to
    :type lineno: int
    :param configfile: the ConfigFile object of the file this line belongs to
    :type configfile: ConfigFile
    """

    def __init__(self, content, lineno=None, configfile=None):
        super().__init__()
        self.lineno = lineno
        self.content = content
        if configfile is None:
            self._configfile = None
        else:
            self._configfile = weakref.ref(configfile)

    def __repr__(self):  # pragma: no cover
        return 'ConfigFileLine(%s, lineno=%s, configfile=%s)' % (self.content, self.lineno, self._configfile)

    @property
    def configfile(self):
        """The ConfigFile object to which this line belongs.

        :rtype: ConfigFile
        """
        try:
            return self._configfile()
        except TypeError:
            return None

    @property
    def is_section_header(self):
        return self.content.lstrip(' \t').startswith('::')

    @property
    def is_source_command(self):
        return self.content.lstrip(' \t')[:9] == '#>source='

    def get_sourced_file(self):
        """Assume this line is a source line and return the file path of the file to be sourced.

        :rtype: str
        :raise ConfigError: if the line is not a valid source command
        """
        if not self.is_source_command:
            raise ConfigError('Section line (%s) is not a source command!' % self.content, location=self)
        _, data = self.content.split('=')
        return data

    def as_header(self):
        """Parse the line as header and return a tuple with the components (section_name, section_id, section_data).

        :rtype: tuple[str, str, str]
        :raise ConfigError: if the line is not a valid header
        """
        try:
            _, section_name, section_id, section_data = self.content.split('::')
        except ValueError:
            raise ConfigError('Section line (%s) is not of form "::name::type::data"!' % self.content, location=self)
        return section_name, section_id, section_data


class ConfigFile(object):
    """A simple wrapper for a configuration file which makes it easier to handle sourced files.

    :param filename: path to a file from which to load the contents
    :type filename: str
    :param sourced_from: the line from where this file was sourced
    :type sourced_from: ConfigFileLine

    :ivar lines: all lines in this file
    :type lines: list[ConfigFileLine]
    """

    def __init__(self, filename=None, sourced_from=None):
        super().__init__()
        self._sourced_files = []
        if filename is None:
            self.filename = None
        else:
            self.filename = os.path.abspath(filename)
        self.lines = []
        if sourced_from is None:
            self._sourced_from = None
        else:
            self._sourced_from = weakref.ref(sourced_from)
        if filename is not None:
            self.from_file(filename)
        self.len = len(self.lines)

    def from_file(self, filename):
        """Use a file's contents as input.

        :param filename: the path to the configuration file
        :type filename: str
        """
        self.filename = os.path.abspath(filename)
        try:
            with open(self.filename) as fd:
                self.from_strings(fd.readlines())
        except IOError:
            raise PathNotFoundError('Can not read file "%s"! %s' % (filename,
                                                                    get_config_traceback_string(self)))

    def from_strings(self, lines, buffername='<buffer>'):
        """Take a list of strings as input. Strips trailing "\n".

        :param lines: the configuration's data
        :type lines: list[str]
        :param buffername: an optional name to be displayed as file name in error messages
        :type buffername: str
        """
        if self.filename is None:
            self.filename = buffername
        lineno = -1
        for line in lines:
            lineno += 1
            self.lines.append(ConfigFileLine(line.rstrip('\n'), lineno=lineno + 1, configfile=self))
        self.len = len(self.lines)

    @property
    def sourced_from(self):
        """The config line object from where this file was sourced.

        :rtype: ConfigFileLine or None
        """
        try:
            return self._sourced_from()
        except TypeError:
            return None

    def __getitem__(self, item):
        return self.lines[item]

    def get_merged_lines(self):
        """Return a list of all lines in this file. All "#>source= lines" are interpreted,
        i.e. the referenced file is inserted instead of the "#>source= line".
        Parsing of the "#>source= lines" is done recursively.

        :rtype: list[ConfigFileLine]
        """
        def get_source_path(path, parent_path):
            if os.path.isabs(path):
                return path
            else:
                return os.path.abspath(os.path.join(os.path.dirname(parent_path), path))
        all_lines = []
        for line in self.lines:
            if line.is_source_command:
                sourcefile = line.get_sourced_file()
                source_config = ConfigFile(filename=get_source_path(sourcefile, self.filename),
                                           sourced_from=line)
                self._sourced_files.append(source_config)
                all_lines.extend(source_config.get_merged_lines())
            else:
                all_lines.append(line)
        return all_lines


def get_config_traceback(config):
    """Return a list of (path, line number) pairs for all ancestors of a given config file object.

    :param config: The config file object.
    :type config: mscreator.config_handlers.ConfigFile
    :rtype: list[(str, int)]
    """
    traceback_info = []
    if isinstance(config, ConfigFile):
        sourced_from_line = config.sourced_from
        if sourced_from_line is not None:
            traceback_info.append((sourced_from_line.configfile.filename, sourced_from_line.lineno))
            traceback_info.extend(get_config_traceback(sourced_from_line.configfile))
    return traceback_info


def get_config_traceback_string(config, exact=True):
    """Get the traceback to a config file, i.e. from where it was sourced,
    and return a string suitable for using in log messages.

    :param exact: If True, config denotes the exact location of the error.
        If False, config points to the section start in which the error occurred.
    :type exact: bool
    :param config: The config file object.
    :type config: mscreator.config_handlers.ConfigFile or mscreator.config_handlers.ConfigFileLine
    :rtype: str
    """
    def get_section_name(configline):
        """
        :type configline: mscreator.config_handlers.ConfigFileLine
        :rtype: str
        """
        all_lines = configline.configfile.lines
        current_line = configline
        next_lineno = configline.lineno
        while not current_line.is_section_header:
            next_lineno -= 1
            if next_lineno <= 1:
                return 'main'
            current_line = all_lines[next_lineno]
        # We must not use current_line.as_header() here as this will result
        # in endless recursion if we got called by as_header() itself
        return current_line.content.split('::')[1]
    if isinstance(config, ConfigFileLine):
        section_name = get_section_name(config)
        if exact:
            where = ''
        else:
            where = 'starting at '
        traceback_info = get_config_traceback(config.configfile)
        prefix = '(In section "%s"(%s"%s:%s")' % (section_name, where, config.configfile.filename, config.lineno)
        if len(traceback_info) > 0:
            prefix += ': '
    else:
        traceback_info = get_config_traceback(config)
        prefix = '('
    return prefix + ' => '.join(['sourced from "%s:%s"' % t for t in traceback_info]) + ')'
