# -*- coding: utf-8 -*-
# Copyright (c) 2012-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Provide mechanisms for rollback transactions."""

from .msclogging import get_logger

logger = get_logger()


class transaction(object):  # pylint: disable=invalid-name
    # noinspection PyUnreachableCode,PyBroadException
    """This class implements a context manager that allows to record a list of actions to be executed (in reverse order)
    if an exception occurs. Optionally an action can also be marked to be called when leaving the with context and
    no exception occurred.

    :examples:
        >>> try:
        ...     with transaction() as t:
        ...         print('111')
        ...         t.add(print, args=('undoing action 1',))
        ...         print('222')
        ...         t.add(print, args=['undoing action 2'])
        ...         print('333')
        ...         t.add(print, args='undoing action 3')
        ... except Exception:
        ...     pass
        111
        222
        333

        >>> try:
        ...     with transaction() as t:
        ...         print('111')
        ...         t.add(print, args=('undoing action 1',))
        ...         print('222')
        ...         t.add(print, args=['undoing action 2'])
        ...         raise Exception('error during transaction')
        ...         print('333')
        ...         t.add(print, args='undoing action 3')
        ... except Exception:
        ...     pass
        111
        222
        undoing action 2
        undoing action 1

        >>> try:
        ...     with transaction() as t:
        ...         print('111')
        ...         t.add(print, args=('undoing action 1',))
        ...         print('222')
        ...         t.add(print, undo_on_success=True, args=['undoing action 2'])
        ... except Exception:
        ...     pass
        111
        222
        undoing action 2
        """

    def __init__(self, name=None, undo_on_success=False):
        super().__init__()
        self._undo_actions = []
        self._name = name
        self._undo_on_success = undo_on_success
        if name is None:
            self._strname = ''
        else:
            self._strname = ' "%s"' % name

    def add(self, undo_function, args=None, kwargs=None, undo_on_success=None):
        """Add a function and it's arguments to the transaction.
        All functions will be called in reverse order of adding when leaving the scope of the with statement.

        :param undo_function: a pointer to a function
        :type undo_function: function
        :param args: the arguments to the function as tuple or list, if undo_function takes only 1 argument,
            you may specify it directly
        :param kwargs: keyword arguments to the function
        :type kwargs: dict
        :param undo_on_success: overrides the default value for undo_on_success
        :type undo_on_success: bool
        """
        if undo_on_success is None:
            undo_on_success = self._undo_on_success
        if args is None:
            args = tuple()
        elif isinstance(args, (list, tuple)):
            pass
        else:
            args = (args,)
        if kwargs is None:
            kwargs = dict()
        self._undo_actions.insert(0, (undo_function, args, kwargs, undo_on_success))

    def __enter__(self):
        logger.debug('Transaction%s started.', self._strname)
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            logger.debug('Transaction%s completed successfully.', self._strname)
            for func, args, kwargs, undo_on_success in self._undo_actions:
                if undo_on_success:
                    func(*args, **kwargs)
            logger.debug('Transaction%s finalized successfully.', self._strname)
            return False
        logger.debug('Error occurred during transaction%s, attempting rollback...', self._strname)
        for func, args, kwargs, _ in self._undo_actions:
            func(*args, **kwargs)
        logger.debug('Transaction%s successfully rolled back.', self._strname)
        return False
