# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module contains functions and classes for accessing a classic(DOS) MBR.
The obsolete CHS addressing is currently completely ignored, this may give some
ugly output in fdisk and so but should not give any problems (at least I hope so).

No code currently writes to any disk/file/device, so playing around should be quite safe.
"""

import struct


def bytes_to_long(buf):
    """Interpret a bytearray as an unsigned long (4 LE bytes).

    :type buf: bytearray or bytes
    :rtype: int
    """
    return struct.unpack(b'<L', buf)[0]


def bytes_to_short(buf):
    """Interpret a bytearray as an unsigned short (2 LE bytes).

    :type buf: bytearray or bytes
    :rtype: int
    """
    return struct.unpack(b'<H', buf)[0]


def long_to_bytes(number):
    """Return the 4 bytes Little-Endian representation of an integer.

    :type number: int
    :rtype: bytes
    """
    return struct.pack(b'<L', number)


def short_to_bytes(number):
    """Return the 2 bytes Little-Endian representation of an integer.

    :type number: int
    :rtype: bytes
    """
    return struct.pack(b'<H', number)


class PartitionEntry(object):
    """Represents a partition table entry of a classic MBR.

    :param index: the partition index in the partition table
    :type index: int
    :param buf: the raw bytes of the partition entry
    :type buf: bytearray
    :param sector_size: the sector size of the underlying disk
    :type sector_size: int
    """

    def __init__(self, index, buf, sector_size=512):
        super().__init__()
        self.bytes = buf
        self.sector_size = sector_size
        self.index = index

    @property
    def active(self):
        return self.bytes[0] & 0x80 != 0

    @active.setter
    def active(self, new_value):
        if new_value:
            self.bytes[0] |= 0x80
        else:
            self.bytes[0] &= 0x7f

    @property
    def partition_type(self):
        return self.bytes[4]

    @partition_type.setter
    def partition_type(self, new_value):
        self.bytes[4] = new_value

    @property
    def start_sector(self):
        return bytes_to_long(self.bytes[8:12])

    @start_sector.setter
    def start_sector(self, new_value):
        self.bytes[8:12] = long_to_bytes(new_value)

    @property
    def size_in_sectors(self):
        return bytes_to_long(self.bytes[12:16])

    @size_in_sectors.setter
    def size_in_sectors(self, new_value):
        self.bytes[12:16] = long_to_bytes(new_value)

    @property
    def size_in_bytes(self):
        return bytes_to_long(self.bytes[12:16]) * self.sector_size

    @size_in_bytes.setter
    def size_in_bytes(self, new_value):
        set_value = new_value // self.sector_size
        if new_value % self.sector_size != 0:
            set_value += 1
        self.bytes[12:16] = long_to_bytes(set_value)


class Partitions(object):
    """Represents the partition table of a classic MBR.

    :param partition_table_bytes: the raw bytes of the partition table
    :type partition_table_bytes: bytearray
    :type _partitions: list[PartitionEntry]
    """

    def __init__(self, partition_table_bytes):
        """

        :type partition_table_bytes: bytearray
        """
        super().__init__()
        self._bytes = partition_table_bytes
        self._partitions = []
        for i in (0, 1, 2, 3):
            self._partitions.append(PartitionEntry(i + 1, self._bytes[16 * i:16 * i + 16]))

    def __len__(self):
        return 4

    def __getitem__(self, item):
        if isinstance(item, int):
            if not 1 <= item <= 4:
                raise ValueError('Partition index must be in [1, 2, 3, 4]!')
        else:
            raise TypeError('Partition index must be an integer!')
        return self._partitions[item - 1]

    def __iter__(self):
        for i in (0, 1, 2, 3):
            yield self._partitions[i]

    def __setitem__(self, key, value):
        if isinstance(key, int):
            if not 1 <= key <= 4:
                raise ValueError('Partition index must be in [1, 2, 3, 4]!')
        else:
            raise TypeError('Partition index must be an integer!')
        if not isinstance(value, PartitionEntry):
            raise TypeError('Partitions must be of type PartitionEntry!')
        self._partitions[key - 1] = value


class Mbr(object):
    """Represents a classic MBR.

    :param buf: the raw bytes of the MBR
    :type buf: bytearray or None
    :param path: the path to a file from which the first 512 bytes are read and treated as MBR
    :type path: str or None
    :param sector_size: the sector size of the underlying disk
    :type sector_size: int
    :raise ValueError: if both path and buf are None
    """

    def __init__(self, buf=None, path=None, sector_size=512):
        if path is None and buf is None:
            raise ValueError('At least one of path and buf must be not None!')
        super().__init__()
        if path is None:
            self._bytes = buf
            self.path = 'disk'
        else:
            with open(path, 'rb') as fd:
                self._bytes = bytearray(fd.read(512))
            self.path = path
        self.sector_size = sector_size
        self._partitions = Partitions(self._bytes[446:510])

    @property
    def disk_signature(self):
        return bytes_to_long(self._bytes[440:444])

    @disk_signature.setter
    def disk_signature(self, new_value):
        self._bytes[440:444] = long_to_bytes(new_value)

    @property
    def partitions(self):
        return self._partitions

    @property
    def boot_signature(self):
        return bytes_to_short(self._bytes[510:512])

    @boot_signature.setter
    def boot_signature(self, new_value):
        self._bytes[510:512] = short_to_bytes(new_value)

    @property
    def bootable(self):
        return self.boot_signature == 0xaa55

    def dump(self):  # pragma: no cover
        from .utils import bytes_to_sizestring

        lines = ['Disk:    %s' % self.path,
                 'Disk ID: 0x%08x' % self.disk_signature,
                 '',
                 ' device    active type startsector size(sector) size(byte)']

        for partition in self.partitions:
            active = '*' if partition.active else ' '
            path = '%s%s' % (self.path, partition.index)
            if partition.partition_type != 0:
                lines.append(' %-9s %6s 0x%02x %11s %12s %s' %
                             (path, active, partition.partition_type, partition.start_sector,
                              partition.size_in_sectors, bytes_to_sizestring(partition.size_in_bytes, precision=9)))
        return lines

    @property
    def bytes(self):
        new_bytes = self._bytes[:446]
        for partition in self.partitions:
            new_bytes += partition.bytes
        new_bytes += self._bytes[510:512]
        return new_bytes


def main():  # pragma: no cover
    import sys
    sys.path.extend(['..', '.'])
    if len(sys.argv) < 2:
        path = '/dev/sda'
    else:
        path = sys.argv[1]
    mbr = Mbr(path=path)
    for line in mbr.dump():
        print(line)

if __name__ == '__main__':  # pragma: no cover
    main()
