# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module holds some data initialized by mscreator.init().
The following "variables" are put into an extra module to avoid circular dependency hell as much as possible.
"""

import logging
import os

MSC_VERSION = "2.0.0"

CONFIG = None
""":type: MscConfig"""

PSEUDO_FILESYSTEMS = {}
""":type: dict[str, mount.Mount]"""

MOUNTINFO_BY_ID = {}
""":type: dict[int, mount.Mount]"""

MOUNTINFO_BY_PATH = {}
""":type: dict[str, mount.Mount]"""

DEBUG_ON = False

LOGLEVEL = logging.INFO

KEEPTEMP = False

OVERWRITE_TARGETS = False

INITIAL_CWD = os.getcwd()

WORKSPACE_DIRECTORY = './.mscreator'
DEFAULT_TEMPORARY_DIRECTORY = os.path.join(WORKSPACE_DIRECTORY, 'tmp')
DEFAULT_CACHE_DIRECTORY = os.path.join(WORKSPACE_DIRECTORY, 'cache')
DEFAULT_DOWNLOAD_CACHE_DIRECTORY = os.path.join(DEFAULT_CACHE_DIRECTORY, 'downloads')
DEFAULT_BUILD_CACHE_DIRECTORY = os.path.join(DEFAULT_CACHE_DIRECTORY, 'build')

ROOT_PATH = os.sep

INITIAL_ROOT = ROOT_PATH

PSEUDO_FILESYSTEM_TYPES = ('proc', 'sysfs', 'cgroup', 'devpts', 'devtmpfs', 'securityfs', 'selinuxfs', 'tmpfs',
                           'debugfs', 'configfs', 'hugetlbfs')
