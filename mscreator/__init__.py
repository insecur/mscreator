# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Defines the basic functionality like functions for initializing and plugin loading."""

import argparse
import errno
import os
import shutil
from collections import OrderedDict
from importlib import import_module

from . import mscglobals, msclogging
from .config import ConfigError
from .config import ConfigFileLine, ConfigFile, get_config_traceback_string
from .exceptions import PathExistsError
from .mount import get_mountinfo
from .utils import Version

logger = msclogging.get_logger()
HANDLERS_PATH = os.path.join(os.path.dirname(__file__), 'config_handlers')


def load_config_section_handlers(path=None):
    """Loads all section handler plugins from a given path or the builtin handlers if path is omitted.

    :param path:
    :type path: str or None
    :return: None
    """
    import pkgutil

    if path is None:
        path = HANDLERS_PATH
    importers = pkgutil.iter_modules(path=[path], prefix='mscreator.config_handlers.')
    for _, name, _ in importers:
        logger.debug('Attempting to load section handlers from "%s"', name)
        import_module(name)


class ConfigNamespace(argparse.Namespace):
    """A Namespace object holding, at least, the commandline arguments used by MSCreator.

    :type loglevel: int
    :type config: str
    :type keep_temp: bool
    :type overwrite_targets: bool
    :type list_sections: bool
    :type section_info: str
    """
    loglevel = msclogging.INFO
    config = None
    keep_temp = False
    overwrite_targets = False
    list_sections = False
    section_info = None


def init(namespace=ConfigNamespace()):
    """Initialize the MSCreator engine, e.g. apply commandline arguments, enable logging and load plugins.
    Returns an MscConfig object for the provided configuration file.

    :param namespace: an object holding the argument values
    :type namespace: ConfigNamespace
    :rtype: MscConfig
    """

    mscglobals.INITIAL_ROOT = os.readlink('/proc/self/root')
    mscglobals.OVERWRITE_TARGETS = namespace.overwrite_targets
    mscglobals.DEBUG_ON = namespace.loglevel <= msclogging.DEBUG
    if mscglobals.DEBUG_ON:
        msclogging.enable_debug_logging()
    mscglobals.LOGLEVEL = namespace.loglevel
    mscglobals.KEEPTEMP = namespace.keep_temp
    logger.setLevel(namespace.loglevel)
    load_config_section_handlers()
    if namespace.config is not None:
        mscglobals.CONFIG = MscConfig(filename=namespace.config)
        mscglobals.CONFIG.parse()
    mscglobals.MOUNTINFO_BY_ID, mscglobals.MOUNTINFO_BY_PATH = get_mountinfo()
    for mountpoint in mscglobals.MOUNTINFO_BY_ID.values():
        if mountpoint.filesystem_type in mscglobals.PSEUDO_FILESYSTEM_TYPES:
            logger.debug('Adding mount point "%s" as pseudo file system!', mountpoint.mount_point)
            mscglobals.PSEUDO_FILESYSTEMS[mountpoint.mount_point] = mountpoint
    return mscglobals.CONFIG


def check_target_overwrite(path):
    """Check if the given path already exists. If it exists and OVERWRITE_TARGETS is False, raise an exception.

    :param path: the path to check
    :type path: str
    :rtype: None
    :raise PathExistsError: raised if path exists and must not be overwritten
    """
    if os.path.exists(path) and not mscglobals.OVERWRITE_TARGETS:
        raise PathExistsError('Target "%s" already exists, use -O to force overwrite.' % path)


def cleanup_build_environment():
    """Remove all build directories."""
    directories = []
    if mscglobals.CONFIG.main is not None:
        directories.append(mscglobals.CONFIG.main.temporary_directory)
    for directory in directories:
        if os.path.exists(directory):
            logger.debug('Removing old directory "%s"...', directory)
            try:
                shutil.rmtree(directory)
            except OSError as exc:
                if exc.errno == errno.ENOENT:
                    logger.debug('Directory "%s" already gone...', directory)
                else:
                    raise


def prepare_build_environment():
    """Create temporary directories:
     - temporary directory
    """
    cleanup_build_environment()
    for directory in (mscglobals.CONFIG.main.temporary_directory,
                      mscglobals.CONFIG.main.download_cache_directory):
        logger.debug('Creating directory "%s"...', directory)
        try:
            os.makedirs(directory)
        except OSError as exc:
            if exc.errno == errno.EEXIST:
                logger.debug('Directory "%s" already exists!', directory)
            else:
                raise


def check_section_header(section_id, section_name, sections, line):
    if section_id == 'hooks' and section_name != 'hooks':
        raise ConfigError('Section of type "hooks" must be named "hooks"!', location=line)
    if section_id == 'variables' and section_name != 'variables':
        raise ConfigError('Section of type "variables" must be named "variables"!', location=line)
    if section_name == 'hooks' and section_id != 'hooks':
        raise ConfigError('Special section "hooks" must be of type "hooks"!', location=line)
    if section_name == 'variables' and section_id != 'variables':
        raise ConfigError('Special section "variables" must be of type "variables"!', location=line)
    if section_name in sections:
        raise ConfigError('Duplicate section name "%s"!' % section_name, location=line)


class MscConfig(object):
    """A singleton class representing the loaded config file.

    Each section begins with a header:
    ::Name::Type::Arguments
    Each section ends where the next begins.

    Some default line formats:
    - key=value
    - string
    - =string
    - #comment
    - ;comment
    Some sections interpret the whole section content by themselves, e.g. all lines of an inlinescript section
    are used as the script.

    :
    :type sections: dict[str, config_handlers.ConfigSectionHandlerBase]
    :type all_config: list[mscreator.config_handlers.ConfigFileLine]
    :type main: config_handlers.MainSectionHandler
    """

    def __init__(self, filename=None, lines=None):
        super().__init__()
        self.sections = OrderedDict()
        self.main = None
        self.hooks = None
        self.variables = {}
        self.filename = filename
        self.configfile = ConfigFile()
        self.all_config = []
        if filename is None:
            if lines is None:
                raise ValueError('At least one of filename and lines must be not None!')
            elif isinstance(lines, str):
                line = lines.split('\n')
                if line[-1].lstrip(' \t') == '':
                    line.pop()
                self.configfile.from_strings(line)
            else:
                self.configfile.from_strings(lines)
        else:
            self.configfile.from_file(self.filename)

    def parse(self):
        """Starts the actual parsing of the config file."""
        self._parse_lines()
        if self.main.msc_min_version > Version(mscglobals.MSC_VERSION):
            raise ConfigError('Configuration requires at least mscreator version "%s", the current version is "%s"!' % (
                self.main.msc_min_version, mscglobals.MSC_VERSION))
        try:
            self.hooks = self.sections['hooks']
        except KeyError:
            from .config_handlers import HooksSectionHandler
            self.hooks = HooksSectionHandler('hooks', None, [], 0, 0)
        self.variables = self.sections.get('variables')
        if self.variables is None:
            from .config_handlers import VariablesSectionHandler
            self.variables = VariablesSectionHandler('variables', None, [], 0, 0)
        self.variables.update(self.main.items())

        dynamic_variables = {}
        for section in self.sections.values():
            if section.section_id == 'dynamicvariables':
                section.run()
                dynamic_variables.update(section.items())
        self.variables.update(dynamic_variables)

        self.main.post_load_init()
        for section in self.sections.values():
            if section.name != 'main':
                section.post_load_init()

    def _parse_lines(self):
        """Parses all lines from a MSC configuration file into an object structure.
        First the main section, i.e. every line before the first section is parsed. Afterwards all relevant section
        handlers are called to parse the remaining sections.

        :raise ConfigError: Raised if a syntactical or semantic error occurs while parsing.
        """
        from .config_handlers import MainSectionHandler, list_config_section_handlers
        sections = OrderedDict()

        self.all_config = [ConfigFileLine('::main::main::', lineno=0, configfile=self.configfile)]
        self.all_config += self.configfile.get_merged_lines()

        len_lines = len(self.all_config)
        i = 1

        # read main section (everything before the first section)
        while i < len_lines and not self.all_config[i].is_section_header:
            i += 1
        if i < len_lines:
            line = self.all_config[i]
            if line.as_header()[0] == 'main':
                raise ConfigError('The header for the "main" section must not be specified!', location=line)
        self.main = MainSectionHandler('main', None, self.all_config, 0, i - 1)
        sections['main'] = self.main
        current_line = i

        # read remaining sections
        while current_line < len_lines:
            line = self.all_config[current_line]
            section_name, section_id, section_data = line.as_header()
            check_section_header(section_id, section_name, sections, line)

            i = current_line + 1
            while i < len_lines and not self.all_config[i].is_section_header:
                i += 1
            handler = list_config_section_handlers().get(section_id, None)
            if handler is None:
                raise ConfigError('Unknown section type "%s"!' % section_id, location=line)
            logger.debug('Adding section of type "%s"', section_id)
            if section_id in ('main', 'hooks'):
                if section_id in [section.section_id for section in sections.values()]:
                    raise ConfigError('Section of type "%s" must only be defined once!' % section_id, location=line)
            if i >= len_lines:
                end = len_lines - 1
            else:
                end = i - 1
            section = handler(section_name, section_data, self.all_config, current_line, end)
            sections[section_name] = section
            current_line = i

        self.sections = sections

    def _section_is_referenced(self, section):
        for target in self.main.targets:
            target_section_names = [t.name for t in target.filesources]
            if section.name in target_section_names:
                return True
        return False

    def build_targets(self):
        prepare_build_environment()

        self.hooks.call('pre_handle_sections')
        logger.info(' ===== Calculating dependencies =====')
        for section in self.sections.values():
            if not self._section_is_referenced(section):
                continue
            if hasattr(section, 'calculate_file_dependencies'):
                logger.info('Processing section "%s"...', section.name)
                section.calculate_file_dependencies()
            if hasattr(section, 'filter_pseudo_mounts'):
                section.filter_pseudo_mounts()
        for target_system in self.main.targets:
            logger.info(' ===== Building "%s" =====', target_system.name)
            self.hooks.call('pre_build_system', target=target_system)
            target_system.build_system()
            self.hooks.call('post_build_system', target=target_system)
        self.hooks.call('pre_cleanup')
        if not mscglobals.KEEPTEMP:
            cleanup_build_environment()
