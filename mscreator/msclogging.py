# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Some basic logging settings, a module or plugin within this package can access the msclogging logger
by using the following code:

    >>> from mscreator.msclogging import get_logger
    >>> logger = get_logger()
    >>> logger.debug('Some message.')
"""

import inspect
import logging

CRITICAL = logging.CRITICAL
ERROR = logging.ERROR
WARN = logging.WARN
INFO = logging.INFO
DEBUG = logging.DEBUG
TRACE = 5


def parse_loglevel(loglevel):
    if isinstance(loglevel, int):
        return loglevel
    elif isinstance(loglevel, str):
        try:
            level = logging.getLevelName(loglevel.upper())
            if isinstance(level, int):
                return level
            else:
                return int(loglevel)
        except (ValueError, TypeError):
            pass
    _logger.warning('Unknown loglevel "%s", setting to "INFO"!', loglevel)
    return INFO


def enable_debug_logging():
    _console_handler.setFormatter(_debug_formatter)


def get_logger():
    """Return a logger object for the module of the function that is calling this method. E.g. when calling from module
    config_handlers, this will return the logger with the name "mscreator.config_handlers".

    :rtype: TraceLogger
    """
    caller = inspect.stack()[1]
    module = inspect.getmodule(caller[0])
    return logging.getLogger(module.__name__)


class TraceLogger(logging.Logger):

    def trace(self, msg, *args, **kwargs):
        if self.isEnabledFor(TRACE):
            self._log(TRACE, msg, args, **kwargs)

logging.setLoggerClass(TraceLogger)
logging.addLevelName(TRACE, 'TRACE')
_logger = logging.getLogger(__package__)

_debug_formatter = logging.Formatter(
    "%(asctime)s: %(levelname)-5.5s %(threadName)s::%(name)-20s(%(lineno)3d) - %(message)s")
_simple_formatter = logging.Formatter("%(message)s")

_console_handler = logging.StreamHandler()
_console_handler.setFormatter(_simple_formatter)
_logger.addHandler(_console_handler)
_logger.setLevel(WARN)
