# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module provides classes and functions for accessing the mount points of a system."""

import os
from .mscglobals import ROOT_PATH
from .utils import partition_from_base, get_uuid_by_partition_path


class CommaList(list):
    """A wrapper around list which helps maintaining a string containing a comma separated list."""

    def __init__(self, initial=None):
        if initial is None:
            super().__init__()
        elif isinstance(initial, str):
            super().__init__(initial.split(','))
        elif isinstance(initial, list):
            super().__init__(initial)
        else:
            raise TypeError('Initial argument to CommaList must be either None, list or a string.')

    def __str__(self):
        return ','.join(self)

    def __repr__(self):  # pragma: no cover
        return "CommaList('%s')" % self

    def __getitem__(self, item):
        return CommaList(super().__getitem__(item))

    def update(self, other):
        for item in other:
            if item not in self:
                self.append(item)


class Mount(object):
    """Stores all information for a mountpoint from /proc/self/mountinfo.
    :type children: dict[int, Mount]
    """

    def __init__(self, initial=None, blockdevice=None, partitionnumber=None):
        super().__init__()
        if initial is None:
            initial = [None] * 9

        self.blockdevice = blockdevice
        self.partitionnumber = partitionnumber
        self.mount_id = int(initial[0])
        self.parent_id = int(initial[1])
        self.major, self.root, self.mount_point = initial[2:5]
        self.mount_options = CommaList(initial[5])
        (
            self.filesystem_type,
            self.mount_source
        ) = initial[-3:-1]
        self.super_options = CommaList(initial[-1])
        self.children = {}

    def _get_source(self, device_base):
        if device_base is None:
            return self.mount_source
        else:
            return partition_from_base(device_base, self.partitionnumber)

    def fstab_entry(self, dump=0, order=0, device_base=None, by_uuid=False):
        """Return the mount point as it would appear in an /etc/fstab file.

        :param dump: the 5. field in fstab(fs_freq) see "man 5 fstab"
        :type dump: int
        :param order: the 5. field in fstab(fs_freq) see "man 5 fstab"
        :type order: int
        :param device_base:
        :type device_base: str
        :param by_uuid: if True, the entry will be mounted by the partitions UUID instead of the path
        :type by_uuid: bool
        :rtype: str
        """
        partition_path = self._get_source(device_base=device_base)
        if by_uuid:
            source = 'UUID=%s' % get_uuid_by_partition_path(partition_path)
        else:
            source = partition_path
        mount_options = self.mount_options[:]
        mount_options.update(self.super_options)
        return '%s\t%s\t%s\t%s\t%d %d' % (source, self.mount_point, self.filesystem_type,
                                          mount_options, dump, order)

    def mount_command(self, root=ROOT_PATH, device_base=None):
        """Return a list containing a call to the mount utility for this mount point.

        :param root: a path to be prepended to `self.mount_point`
        :type root: str
        :param device_base:
        :type device_base: str
        :rtype: list[str]
        """
        source = self._get_source(device_base=device_base)
        target = os.path.join(root, self.mount_point.lstrip(os.sep))
        mount_options = self.mount_options[:]
        mount_options.update(self.super_options)
        return ['mount', '-t', self.filesystem_type, '-o', str(mount_options), source, target]


def create_tree_from_mountinfo(mounts):
    """Adds a dictionary of all submounts to each mount point and returns the root node.

    :param mounts: a dictionary as returned by `get_mountinfo()`
    :type mounts: dict[int, Mount]
    :return:
    :rtype: Mount
    """
    mount_root = None
    for mount in mounts.values():
        if mount.parent_id in mounts:
            mounts[mount.parent_id].children[mount.mount_id] = mount
        else:
            mount_root = mount
    return mount_root


def create_mountsbypath_from_tree(root):
    """Create a dictionary containing all mountpoints under `root` indexed by their mountpoint.

    :param root: the root Mount object
    :type root: Mount or MountTree
    :rtype: dict[str, Mount]
    """
    mounts_by_path = {root.mount_point: root}
    for mount in root.children.values():
        mounts_by_path[mount.mount_point] = mount
    return mounts_by_path


def get_mountinfo():
    """Returns 2 dictionaries for all mount points, the first one indexed by mount ID, the second by mount point.

    :rtype: (dict[int, Mount], dict[str, Mount])
    """
    mounts_by_id = {}
    mounts_by_path = {}
    with open('/proc/self/mountinfo') as mounts_fd:
        for line in mounts_fd.readlines():
            mount = line.strip().split(' ')
            mount = Mount(mount)
            mounts_by_id[mount.mount_id] = mount
            mounts_by_path[mount.mount_point] = mount
    return mounts_by_id, mounts_by_path


def sort_mountpoints(mounts_by_path):
    """Sort a mounts_by_path dictionary by path and return a list of the mountpoints.

    :param mounts_by_path: the mount points to be sorted
    :type mounts_by_path: dict[str, Mount]
    :return: a sorted list of the mount points
    :rtype: list[Mount]
    """
    return [v for k, v in sorted(mounts_by_path.items())]
