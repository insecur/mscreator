# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module defines the custom exceptions used in MSCreator."""

import traceback

from . import mscglobals
from .msclogging import get_logger

logger = get_logger()


class MSCError(Exception):
    """Base class for exceptions which emit a critical log message containing the exception message and also a
    traceback if mscglobals.DEBUG_ON is True.

    :param message: A message which describes the reason for raising in more detail.
    :type message: str
    :param strip: how many levels of the traceback are to be stripped
    :type strip: int
    """

    def __init__(self, message, strip=-1):
        super().__init__(message)
        if mscglobals.DEBUG_ON:
            logger.critical('%s: %s\n%s', self.__class__.__name__, message, ''.join(traceback.format_stack()[:strip]))
        else:
            logger.critical('%s: %s', self.__class__.__name__, message)


class ProgrammerTooStupidError(MSCError):
    """If this exception ever gets raised, some algorithmic error occurred, e.g. use of a class which must be
    initialized first. Use this when defining an abstract method which needs some other step to be called in advance.
    """
    pass


class PathExistsError(MSCError):
    """Raised if a file object already exists."""
    pass


class PathNotFoundError(MSCError):
    """Raised if a file object cannot be found."""
    pass


class DependencyError(MSCError):
    """Raised if a dependency for a file cannot be found."""
    pass


class CommandError(MSCError):
    """Raised if a call to an external tool causes an error."""

    def __init__(self, message, strip=-1, returncode=None):
        super().__init__(message, strip=strip)
        self.returncode = returncode


class CriticalError(MSCError):
    """Raised if a situation is detected where safe operation cannot be guaranteed, e.g. a path is generated which
    points to the users root directory and would be used for deleting files there."""
    pass
