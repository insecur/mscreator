# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os

from . import BootloaderHandlerBase, KnownValue
from .kernel import KernelSectionHandler
from .. import mscglobals
from ..commands import rsync, Grub
from ..config import ConfigError
from ..exceptions import PathNotFoundError
from ..msclogging import get_logger
from ..utils import temporary_file, symlink, partition_from_base, safe_dir_join

logger = get_logger()


def _convert_boot_entry(entry):
    if entry is None:
        return None
    kernel = mscglobals.CONFIG.sections[entry]
    if isinstance(kernel, KernelSectionHandler):
        return kernel
    else:
        raise ConfigError('A boot entry must reference a kernel section. "%s" is a "%s" section!' %
                          (kernel.name, kernel.__class__.__name__))


class Grub1SectionHandler(BootloaderHandlerBase):
    """A provider for good old "legacy" grub 1.

    """

    description = 'A provider for the good old "legacy" GRUB 1.'
    section_id = 'grub1'
    known_values = (
        KnownValue('stage1', 'The path to the stage1 file.',
                   convert=os.path.abspath),
        KnownValue('stage2', 'The path to the stage2 file.',
                   convert=os.path.abspath),
        KnownValue('grub', 'The path to the grub binary.',
                   convert=os.path.abspath),
        KnownValue('boot_entry', 'The section name of a kernel, the first boot entry will be the default.',
                   attribute_name='boot_entries', post_load_convert=_convert_boot_entry,
                   required=False, unique=False),
        KnownValue('timeout', 'The timeout after which grub will boot the default entry.',
                   required=False, default=None)
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.stage1 = None
        self.stage2 = None
        self.grub = None
        self.boot_entries = []
        self.timeout = self['timeout']

    def post_load_init(self):
        super().post_load_init()
        for entry in self.boot_entries:
            if entry is None:
                self.boot_entries.remove(entry)
        if not os.path.exists(self.grub):
            raise PathNotFoundError('Grub binary "%s" not found!' % self.grub)

    def deploy_files(self, path):
        """Copies stage1 and stage2 to the /boot/grub/ directory at a specified target location and
        creates necessary symlink(s).

        :param path: path where the dependencies should be stored
        """
        boot_target_path = os.path.join(path, 'boot')
        grub_target_path = safe_dir_join(boot_target_path, 'grub')
        os.makedirs(grub_target_path, exist_ok=True)
        boot_path = os.path.join(boot_target_path, 'boot')
        symlink('.', boot_path, force=True)
        rsync([self.stage1, self.stage2], grub_target_path)

    def write_config(self, path, root_partition_index=1, boot_partition_index=1):
        lines = ['default=0\n']
        if self.timeout is not None:
            lines.append('timeout=%s\n' % self.timeout)
        for entry in self.boot_entries:
            lines.extend([
                '\n'
                'title %s\n' % entry.name,
                'root (hd0,%d)\n' % (boot_partition_index - 1),
            ])
            if entry.cmdline is not None:
                lines.append('kernel (hd0,%d)/boot/%s %s\n' % (
                    boot_partition_index - 1, os.path.basename(entry.image), entry.cmdline))
            else:
                lines.append('kernel (hd0,%d)/boot/%s\n' % (
                    boot_partition_index - 1, os.path.basename(entry.image)))
            if entry.initrd is not None:
                lines.append('initrd /boot/%s\n' % os.path.basename(entry.initrd))
        grub_boot_path = os.path.join(path, 'boot', 'grub')
        os.makedirs(grub_boot_path, exist_ok=True)
        with open(os.path.join(grub_boot_path, 'menu.lst'), 'w') as fd:
            fd.writelines(lines)

    def install(self, target_device_path, boot_partition_index=1):
        # grub does not recognize loop0p1 as partition 1 of loop0, but seems to work only with unnumbered devices
        boot_device_path = partition_from_base(target_device_path, boot_partition_index)
        grubdisk_path = os.path.join(mscglobals.CONFIG.main.temporary_directory, 'grubdisk')
        symlink(target_device_path, grubdisk_path, force=True)
        symlink(boot_device_path, '%s%s' % (grubdisk_path, boot_partition_index), force=True)
        with temporary_file('device.map-', keep=mscglobals.KEEPTEMP) as tempfile:
            tempfile.fd.writelines(['(hd0) %s\n' % grubdisk_path])
            tempfile.close()
            grub = Grub(command_path=self.grub)
            grub(device_map=tempfile.filename,
                 stdin='root (hd0,%d)\nsetup (hd0)\n' % (boot_partition_index - 1),
                 parameters=['--batch'])
