# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import glob
import os
import re

from . import FileSourceHandlerBase, KnownValue
from ..commands import ldd
from ..config import ConfigRaw
from ..exceptions import DependencyError, PathNotFoundError
from ..utils import chroot

RE_LDLINUX = re.compile(r'/lib(32|64)?/ld-(linux(-x86-64)?|[0-9]+\.?[0-9]+)\.so(\.[1-9])?')
REDIRECTED_LD_RE = re.compile(r'^/lib.*/ld-.* => (.*) \(.+$')
DIRECT_RE = re.compile(r'^(/.+) \(.+\)$')
RELATIVE_RE = re.compile(r'^.+=> (/.+) \(.+$')
UNRESOLVED_RE = re.compile(r'^(.+) => .+$')


class LddFilesSectionHandler(FileSourceHandlerBase):
    """This class represents a list of binaries whose dependencies should be calculated via glibc's dynamic linker"""

    description = '''A list of binaries whose dependencies are calculated via glibc's dynamic linker'''
    section_id = 'ldd_files'
    known_values = (
        KnownValue('arch', 'The target architecture, "amd64" or "x86". The default is not to change the architecture.',
                   required=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.arch = self['arch']

    def calculate_file_dependencies(self):
        filenames = set()
        with chroot(self.chroot_path, arch=self.arch):
            for filename in self._values:
                if not isinstance(filename, ConfigRaw):
                    continue
                filename = filename.configline.content.lstrip(' \t')
                names = glob.glob(filename)
                if len(names) == 0:
                    raise PathNotFoundError('"%s" did not match any existing files!' % filename)
                filenames.update(names)
            self.calculated_dependencies = get_all_dependencies_for_files(filenames)
        self._dependencies_are_calculated = True


def get_linker_dependencies_for_file(filepath):
    """Returns a list of all direct dynamic link dependencies of a binary.
    Symlinks are not resolved.

    NOTE: Linux ldd already resolves dependencies recursively.

    :type filepath: str
    :rtype: set[str]
    """
    deps = set()

    stdout = ldd(filepath)
    if stdout is None:
        return deps
    lines = [line.strip() for line in stdout.split('\n')]

    for line in lines:
        if line.startswith('linux-gate.so.1') or line.startswith('linux-vdso.so.1'):
            # ignore linux-gate/linux-vdso (linux virtual dynamic shared object)
            continue
        match = REDIRECTED_LD_RE.match(line)
        if match is not None:
            deps.add(match.groups()[0])
            continue
        match = DIRECT_RE.match(line)
        if match is not None:
            deps.add(match.groups()[0])
            continue
        match = RELATIVE_RE.match(line)
        if match is not None:
            deps.add(match.groups()[0])
            continue
        match = UNRESOLVED_RE.match(line)
        if match is not None:
            raise DependencyError('Could not resolve dependency "%s" for "%s"!' % (match.groups()[0], filepath))
        # ignore everything else (e.g. statically linked)
    return deps


def get_intermediaries(path):
    """Return a list of all parent paths (except /) starting with the shortest parent.

    :type path: str
    :rtype: list[str]
    """
    paths = []
    while True:
        path = os.path.split(path)[0]
        if path == '/':
            break
        paths.append(path)
    return paths[::-1]


def get_all_dependencies_for_files(filenames, include_filenames=True):
    dependencies = set()
    binaries = {os.path.abspath(f) for f in filenames}
    if include_filenames:
        unresolved_deps = binaries.copy()
    else:
        unresolved_deps = set()

    for path in binaries:
        unresolved_deps.update(get_linker_dependencies_for_file(path))

    while len(unresolved_deps) > 0:
        current_unresolved = sorted(unresolved_deps)[0]
        needs_recalculation = False
        for current_path in get_intermediaries(current_unresolved) + [current_unresolved]:
            dependencies.add(current_path)
            if os.path.islink(current_path):
                target = os.path.abspath(os.path.join(os.path.dirname(current_path), os.readlink(current_path)))
                unresolved_deps.discard(current_path)
                if os.path.realpath(target) == target:
                    dependencies.add(target)
                else:
                    unresolved_deps.add(target)
                if os.path.isdir(current_path):
                    tmp_unresolved_deps = unresolved_deps.copy()
                    for path in unresolved_deps:
                        if path.startswith(current_path + '/'):
                            tmp_unresolved_deps.remove(path)
                            tmp_unresolved_deps.add(target + path[len(current_path):])
                    unresolved_deps = tmp_unresolved_deps
                needs_recalculation = True
                break
        if not needs_recalculation:
            unresolved_deps.remove(current_unresolved)

    return dependencies
