# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
import shutil

from . import TargetSystemHandlerBase, KnownValue
from .. import check_target_overwrite, mscglobals
from ..commands import losetup, mount, partx, mkfs, qemu_img, retry_umount
from ..config_handlers import get_section_by_name
from ..mount import sort_mountpoints, create_mountsbypath_from_tree
from ..msclogging import get_logger
from ..transactions import transaction
from ..utils import partition_from_base, boolean, safe_dir_join

logger = get_logger()


class DiskImageSectionHandler(TargetSystemHandlerBase):
    """This handler is responsible for creating a disk image which can be used as a hard disk for a virtual machine.
    :type disk_layout: DiskLayoutSectionHandler
    :type target_path: str or None
    """

    description = 'The disk image target system handler.'
    section_id = 'diskimage'
    known_values = (
        KnownValue('target_path', 'The path to the target image.',
                   convert=os.path.abspath),
        KnownValue('image_type',
                   'The type of the disk image, e.g. qcow2, vdi, vmdk. For a full list see '
                   '`qemu-img --help | grep "Supported formats"`.'),
        KnownValue('disk_layout', 'The section name of the disklayout used for partitioning.'),
        KnownValue('device_base', 'The path to the block device this partition will represent, i.e. the how the device '
                                  'is named in the target. (default: /dev/sda)',
                   required=False, default='/dev/sda'),
        KnownValue('bootloader', 'The section name of the bootloader to install.',
                   post_load_convert=get_section_by_name,
                   required=False),
        KnownValue('create_bootloader_config',
                   'If set to anything but true/1/yes/on/enabled, do not write the bootloader config.',
                   required=False, default=True, convert=boolean),
    ) + TargetSystemHandlerBase.known_values

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.device_base = self['device_base']
        self.image_type = self['image_type']
        self.disk_layout = None
        self.create_bootloader_config = self['create_bootloader_config']
        self.target_path = None

    def post_load_init(self):
        super().post_load_init()
        temp_dir = mscglobals.CONFIG.main.temporary_directory
        self.livedir = safe_dir_join(temp_dir, 'diskmount')
        check_target_overwrite(self.target_path)
        self.disk_layout = mscglobals.CONFIG.sections.get(self['disk_layout'])

    def build_system(self):
        if os.path.exists(self.target_path):
            logger.info('Removing old disk image "%s"...', self.target_path)
            os.unlink(self.target_path)

        with transaction('build-image') as undo_transaction:
            logger.info('Creating image...')
            raw_path = self._create_image(self.disk_layout, undo_transaction)

            loop_path = losetup.attach(raw_path)
            undo_transaction.add(losetup.detach, args=(loop_path,), undo_on_success=True)
            logger.info('Creating partitions...')
            self.disk_layout.apply(loop_path)
            partx.update(loop_path)

            logger.info('Creating filesystems...')
            for partition in self.disk_layout.partitions:
                device_path = partition_from_base(loop_path, partition.partitionnumber)
                mkfs(device_path, partition.filesystem)

            logger.info('Mounting filesystems...')
            fstab_entries = self._mount_filesystems(self.disk_layout, loop_path, undo_transaction)

            self.deploy_filesource_files(self.livedir)
            self._deploy_kernel_and_bootloader()

            for mountpoint in sort_mountpoints(self.required_mounts):
                fstab_entries.append(mountpoint.fstab_entry())
            logger.info('Writing /etc/fstab...')
            self._write_fstab(fstab_entries)

            self._install_bootloader(loop_path)

            self.call_hook('pre_finalize')

            logger.info('Unmounting filesystems...')

        logger.info('Converting image to "%s"...', self.image_type)
        if self.image_type == 'raw':
            shutil.move(raw_path, self.target_path)
        else:
            qemu_img.convert(raw_path, self.target_path, self.image_type)

    def _install_bootloader(self, loop_path):
        if self.bootloader is not None:
            logger.info('Installing bootloader...')
            self.bootloader.install(loop_path, boot_partition_index=self.disk_layout.boot_index)

    def _deploy_kernel_and_bootloader(self):
        if self.bootloader is not None:
            self.deploy_kernel_files(self.livedir)

            logger.info('Copying bootloader files...')
            self.bootloader.deploy_files(self.livedir)
            if self.create_bootloader_config:
                self.bootloader.write_config(self.livedir,
                                             root_partition_index=self.disk_layout.root_index,
                                             boot_partition_index=self.disk_layout.boot_index)
            else:
                logger.debug('Writing of menu.lst disabled via config.')

    def _create_image(self, disk_layout, undo_transaction):
        """Create a RAW image file from the given disk layout.

        :param disk_layout:
        :type disk_layout:
        :param undo_transaction:
        :type undo_transaction:
        :return:
        :rtype:
        """
        temp_dir = mscglobals.CONFIG.main.temporary_directory
        raw_path = os.path.join(temp_dir, os.path.basename(self.target_path) + '.raw')
        os.makedirs(temp_dir, exist_ok=True)
        with open(raw_path, 'w') as fd:
            fd.truncate(disk_layout.disksize)
        if not mscglobals.KEEPTEMP:
            undo_transaction.add(os.remove, args=(raw_path,))
        return raw_path

    def _mount_filesystems(self, disk_layout, loop_path, undo_transaction):
        mount_tree = disk_layout.get_mount_tree(device_base=self.device_base)
        mounts = create_mountsbypath_from_tree(mount_tree)
        fstab_entries = []
        for mountpoint in sort_mountpoints(mounts):
            fstab_entries.append(mountpoint.fstab_entry(by_uuid=True))
            mount_command = mountpoint.mount_command(root=self.livedir, device_base=loop_path)
            os.makedirs(mount_command[-1], exist_ok=True)
            mount(mount_command[-2], mount_command[-1])
            undo_transaction.add(retry_umount, args=(mount_command[-1],), undo_on_success=True)
        return fstab_entries

    def _write_fstab(self, fstab_entries):
        etc_dir = safe_dir_join(self.livedir, 'etc')
        os.makedirs(etc_dir, exist_ok=True)
        with open(os.path.join(etc_dir, 'fstab'), 'w') as fd:
            fd.writelines(['%s\n' % i for i in fstab_entries])
