# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import glob
import os
import re
import shlex
import subprocess
import sys
import tempfile
import time

from . import FileSourceHandlerBase, KnownValue
from .. import mscglobals
from ..config import ConfigRaw, KeyValueEntry, ConfigError
from ..msclogging import get_logger, TRACE
from ..utils import which, chroot

logger = get_logger()

KEY_VALUE_RE = re.compile(r'^([a-zA-Z0-9_]+)=(.*)$')
TERMINATE_TIMEOUT = 0.5


class StraceFile(object):

    def __init__(self, commandline, options):
        super().__init__()
        self._commandline = commandline
        self.timeout = None
        self.stopcommand = None
        for value in options:
            try:
                k = value.key
                v = value.value
            except AttributeError:
                continue
            if k == 'timeout':
                if v is not None:
                    self.timeout = float(v)
            elif k == 'stop_command':
                self.stopcommand = v
            else:
                raise ConfigError('Unknown value "%s" in strace_files section!' % k)
        self._values = options

    @property
    def commandline(self):
        """Return the commandline as a list of strings.

        :rtype: list[str]
        """
        return shlex.split(self._commandline)


class StraceFilesSectionHandler(FileSourceHandlerBase):
    """A class using strace to examine the syscalls to find the file dependencies used by a command."""

    description = 'A list of command lines that are executed and dependencies are gathered by examining syscalls.'
    section_id = 'strace_files'
    ignore_value_attributes = ['timeout', 'stop_command']
    known_values = (
        KnownValue('arch', 'The target architecture, "amd64" or "x86". The default is not to change the architecture.',
                   required=False),
        KnownValue('timeout', 'Timeout in seconds after which the traced process is stopped if it is still running.',
                   required=False, unique=False),
        KnownValue('stop_command',
                   'The command used to terminate the straced binary. Straced process is terminated with SIGTERM '
                   'if omitted.',
                   required=False, unique=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.arch = self['arch']

    def _parse_lines(self, lines, start, end):
        super()._parse_lines(lines, start, end)
        values = []
        commandline = None
        options = []
        for value in self._values:
            if isinstance(value, ConfigRaw):
                if commandline is not None:
                    values.append(StraceFile(commandline, options))
                    options = []
                commandline = value.configline.content.lstrip(' \t')
            elif isinstance(value, KeyValueEntry):
                if value.key in ['arch']:
                    values.append(value)
                else:
                    options.append(value)
        if commandline is not None:
            values.append(StraceFile(commandline, options))
        self._values = values

    def calculate_file_dependencies(self):
        # noinspection PyBroadException
        try:
            # NOTE: if a module is missing while in chroot, python will not be able to load it,
            # so we ensure it is loaded before entering the chroot environment, in this case we are going to use
            # subprocess.Popen, which uses the escape functionality from the encodings module
            # noinspection PyUnresolvedReferences
            import encodings.raw_unicode_escape  # pylint: disable=unused-variable
            # noinspection PyUnresolvedReferences
            import encodings.unicode_escape  # pylint: disable=unused-variable
        except ImportError:  # pragma: no cover
            pass
        with chroot(self.chroot_path, self.arch):
            for stracefile in self._values:
                if isinstance(stracefile, StraceFile):
                    self.calculated_dependencies.update(get_dependencies_from_strace(stracefile))

            # filter non-existing files so we do not try to sync temp files later
            deps = set(self.calculated_dependencies)
            for path in deps:
                if not os.path.exists(path):
                    logger.debug('Removing non-existing file "%s"', path)
                    self.calculated_dependencies.discard(path)
        self._dependencies_are_calculated = True


def get_link_target(path):
    target = os.readlink(path)
    return os.path.normpath(os.path.join(os.path.dirname(path), target))


def get_loader_paths(command):
    for loader in glob.glob('/lib*/ld-linux*'):
        try:
            returncode = subprocess.call([loader, '--verify', command], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except OSError:
            continue
        if returncode == 0:
            return [loader, get_link_target(loader)]
    return []


ENOENT_RE = re.compile(r'.*(\).*=.*ENOENT|\+\+\+ exited with ).*')
SYSCALL_RE = re.compile(r'.*(open|execve|readlink)\("(?P<name>[^"]*)", .*')


def is_on_pseudo_mount(path):
    for pseudo_path in mscglobals.PSEUDO_FILESYSTEMS:
        if path.startswith(pseudo_path):
            return True
    return False


def run_external_command(cmdline, stopcommand=None, timeout=None):
    if mscglobals.LOGLEVEL <= TRACE:
        stdout = None
        stderr = None
    else:
        stdout = subprocess.PIPE
        stderr = subprocess.PIPE
    proc = subprocess.Popen(cmdline, stdout=stdout, stderr=stderr)
    if timeout is not None:
        elapsed = 0
        while elapsed < timeout:
            if proc.poll() is not None:
                break
            elapsed += 0.1
            time.sleep(0.1)
        if elapsed >= timeout:
            if stopcommand is None:
                logger.info('Command timed out after %s seconds, terminating...', timeout)
                proc.terminate()
                time.sleep(TERMINATE_TIMEOUT)
                proc.kill()
            else:
                cmdline = shlex.split(stopcommand % {'PID': proc.pid})
                logger.info('Command timed out after %s seconds, stopping with "%s"...', timeout, cmdline)
                subprocess.call(cmdline)
    else:
        proc.wait()


def get_dependencies_from_strace(stracefile):
    command = stracefile.commandline
    timeout = stracefile.timeout
    stopcommand = stracefile.stopcommand
    strace_output_file_prefix = tempfile.mktemp(prefix='strace_', dir=mscglobals.ROOT_PATH)
    logger.info('Running command "%s"...', command)
    pid = os.fork()  # necessary to survive "kill 0" in command
    if pid == 0:  # pragma: no cover
        # noinspection PyArgumentList
        os.setsid()
        cmdline = ['strace', '-o', strace_output_file_prefix, '-ff', '-e', 'execve,open,readlink'] + command
        run_external_command(cmdline, stopcommand, timeout)
        sys.exit(0)
    else:
        logger.debug('waiting for child to terminate...')
        os.waitpid(pid, 0)
        logger.debug('child terminated...')

    full_command = which(command[0])
    dependencies = {full_command}
    for path in get_loader_paths(full_command):
        dependencies.add(path)

    for strace_output_file in glob.glob(strace_output_file_prefix + '*'):
        with open(strace_output_file) as strace_output_fd:
            for line in strace_output_fd.readlines():
                match = ENOENT_RE.match(line)
                if match is not None:
                    continue
                match = SYSCALL_RE.match(line)
                if match is not None:
                    dependencies.add(os.path.normpath(match.groupdict()['name']))
                    continue
        os.unlink(strace_output_file)

    file_dependencies = dependencies.copy()
    for path in dependencies:
        if os.path.islink(path):
            if not is_on_pseudo_mount(path):
                file_dependencies.add(get_link_target(path))
            else:
                logger.debug('Not adding link target for "%s" from a pseudo filesystem.', path)

    return file_dependencies
