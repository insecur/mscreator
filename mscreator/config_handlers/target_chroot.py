# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module defines the chroot target system.
It is the simplest of all systems and merely needs a target location where the system should be installed."""

import os
import shutil

from . import TargetSystemHandlerBase, KnownValue
from .. import check_target_overwrite
from ..mount import sort_mountpoints
from ..msclogging import get_logger

logger = get_logger()


class ChrootSectionHandler(TargetSystemHandlerBase):
    """Represents a chroot target system. A chroot is the simplest target system, as it simply copies all
    file dependencies to a target directory and has no dependencies on hardware, kernel or other external entities."""

    description = 'The chroot target system handler.'
    section_id = 'chroot'
    known_values = (
        KnownValue('target_directory', 'The path were to copy all the required files.'),
        KnownValue('start_script', 'The path to a shell script which is generated and can be used to enter the chroot.',
                   required=False)
    ) + TargetSystemHandlerBase.known_values

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.target_directory = None
        self.start_script = None

    def post_load_init(self):
        super().post_load_init()
        self.livedir = os.path.abspath(self.target_directory)
        check_target_overwrite(self.livedir)

    def build_system(self):
        if os.path.exists(self.livedir):
            logger.info('Removing old chroot directory "%s"...', self.livedir)
            shutil.rmtree(self.livedir)
        start_script = self['start_script']
        self.deploy_filesource_files(self.livedir)
        umount_paths = []
        if start_script is not None:
            basedir = os.path.dirname(start_script)
            logger.info('Generating start script "%s" for chroot.', start_script)
            lines = ['#!/bin/bash',
                     'cd $(dirname $0)',
                     'cd %s' % os.path.relpath(self.livedir, start=basedir)]
            for mount in sort_mountpoints(self.required_mounts):
                mount_path = mount.mount_point
                lines.append(' '.join(mount.mount_command(root='.')))
                umount_paths.insert(0, mount_path.lstrip(os.sep))
            lines.append('chroot .')
            for umount_path in umount_paths:
                lines.append('umount %s' % umount_path)
            os.makedirs(basedir, exist_ok=True)
            with open(start_script, 'w') as fd:
                fd.writelines(['%s\n' % line for line in lines])
            os.chmod(start_script, 0o755)

        self.call_hook('pre_finalize')
