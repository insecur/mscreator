# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os

from . import ConfigSectionHandlerBase, KnownValue
from ..commands import rsync
from ..utils import safe_dir_join


class KernelSectionHandler(ConfigSectionHandlerBase):
    """The description of a kernel and its associated files."""

    category = 'kernel'
    description = __doc__
    section_id = 'kernel'
    known_values = (
        KnownValue('kernelversion', 'The kernel release string as shown by `uname -r`.'),
        KnownValue('image', 'The path to the kernel image.'),
        KnownValue('cmdline', "The kernel's commandline parameters.",
                   required=False),
        KnownValue('initrd', 'The path to an initrd image.',
                   required=False),
        KnownValue('modules_path', "The path to a directory containing the kernel's loadable modules.",
                   required=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.kernelversion = None
        self.image = None
        self.cmdline = self['cmdline']
        self._boot_files = []
        self.initrd = None
        self.modules_path = None

    def post_load_init(self):
        super().post_load_init()
        self._boot_files = [self.image]
        if self.initrd is not None:
            self._boot_files.append(self.initrd)

    def deploy_files(self, path):
        """Copies the kernel, initrd and the modules to a target location using the external rsync utility.
        Kernel and initrd images go to $path/boot/ and the modules to $path/lib/modules/$kernelversion/

        :param path: path where the dependencies should be stored
        """
        modules_target_path = safe_dir_join(path, 'lib', 'modules', self.kernelversion)
        boot_target_path = safe_dir_join(path, 'boot')
        os.makedirs(modules_target_path, exist_ok=True)
        os.makedirs(boot_target_path, exist_ok=True)
        if self.modules_path is not None:
            rsync(self.modules_path + os.sep, modules_target_path)
        rsync(self._boot_files, boot_target_path)
