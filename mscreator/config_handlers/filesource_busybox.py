# -*- coding: utf-8 -*-
# Copyright (c) 2016-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""
import multiprocessing
import os
import shutil
import stat

from . import FileSourceHandlerBase, KnownValue
from .filesource_lddfiles import get_all_dependencies_for_files
from .. import mscglobals
from ..commands import Command, rsync
from ..msclogging import get_logger
from ..utils import boolean, safe_dir_join

logger = get_logger()

_url_template = 'https://busybox.net/downloads/busybox-%(bb_version)s.tar.bz2'


class BusyBoxSectionHandler(FileSourceHandlerBase):
    description = '''Download and build busybox binary with a given config.'''
    section_id = 'busybox'
    known_values = (
        KnownValue('bb_version', 'The busybox version that is downloaded from https://www.busybox.net/downloads/.'),
        KnownValue('download', 'If set to False, do not try to download the busybox sources.',
                   required=False, convert=boolean, default=True),
        KnownValue('url',
                   'The download URL template, %%(bb_version)s will be replaced with the value of the version '
                   'parameter. (default: %s)' % _url_template,
                   required=False, default=_url_template),
        KnownValue('configuration',
                   'Path to the busybox configuration file to be used. (default: a config with all options '
                   '(`make defconfig`))',
                   required=False),
        KnownValue('make_jobs', 'Number of parallel build jobs. (default: number of CPU cores+1)',
                   required=False, default=multiprocessing.cpu_count() + 1),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.bb_version = None
        self.url = _url_template
        self.download = True
        self.configuration = None
        self.make_jobs = None
        self.archive_name = None
        self.archive_path = None
        self.build_path = None
        self.binary_path = None

    def post_load_init(self):
        super().post_load_init()
        self.archive_name = os.path.basename(self.url)
        self.archive_path = os.path.join(mscglobals.CONFIG.main.download_cache_directory, self.archive_name)
        self.build_path = safe_dir_join(mscglobals.CONFIG.main.build_cache_directory,
                                        self.archive_name[:-8])  # strip .tar.bz2
        self.binary_path = os.path.join(self.build_path, 'busybox')
        if self.configuration is not None:
            self.configuration = os.path.abspath(self.configuration)

    def _download(self):
        if not os.path.exists(self.archive_path):
            wget = Command('wget', detect=True)
            wget(['-c', '-t1', self.url, '-O', self.archive_name + '.temp'],
                 cwd=mscglobals.CONFIG.main.download_cache_directory)
            shutil.move(self.archive_path + '.temp', self.archive_path)

    def _unpack(self):
        build_base = mscglobals.CONFIG.main.build_cache_directory
        if not os.path.exists(build_base):
            os.makedirs(build_base)
            tar = Command('tar', detect=True)
            tar(['xf', self.archive_path, '-C', build_base])

    def _configure(self):
        make = Command('make', detect=True)
        target_config_path = os.path.join(self.build_path, '.config')
        if self.configuration is None:
            if not os.path.exists(target_config_path):
                make(['defconfig'], cwd=self.build_path)
        else:
            shutil.copyfile(self.configuration, target_config_path)

    def _build(self):
        make = Command('make', detect=True)
        make(['-j%s' % self.make_jobs, '-l%s' % self.make_jobs], cwd=self.build_path)

    def calculate_file_dependencies(self):
        logger.info(' * Downloading busybox archive...')
        self._download()
        logger.info(' * Extracting busybox archive...')
        self._unpack()
        logger.info(' * Configuring busybox...')
        self._configure()
        logger.info(' * Compiling busybox...')
        self._build()
        logger.info(' * Calculating busybox dependencies...')
        self.calculated_dependencies = get_all_dependencies_for_files([self.binary_path], include_filenames=False)

        self._dependencies_are_calculated = True

    def deploy_files(self, path):
        super().deploy_files(path)
        target_path = safe_dir_join(path, 'bin')
        os.makedirs(target_path, exist_ok=True)
        rsync([self.binary_path], target_path)
        binary_target_path = os.path.join(target_path, 'busybox')
        file_stats = os.stat(binary_target_path)
        os.chmod(binary_target_path, file_stats.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
