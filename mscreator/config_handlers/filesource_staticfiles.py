# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os

from . import FileSourceHandlerBase, KnownValue
from .. import mscglobals
from ..commands import rsync
from ..config import ConfigRaw
from ..utils import safe_dir_join


class StaticFilesSectionHandler(FileSourceHandlerBase):
    """This class represents a list of files for which no dependencies have to be calculated, e.g. configuration
    files in /etc."""

    description = '''A list of files that are included without needing to calculate dependencies.'''
    section_id = 'static_files'

    def calculate_file_dependencies(self):
        variables = mscglobals.CONFIG.variables
        self.calculated_dependencies = set([
            v.configline.content.lstrip(' \t') % variables for v in self._values if isinstance(v, ConfigRaw)])
        self._dependencies_are_calculated = True


class StaticDirSectionHandler(FileSourceHandlerBase):
    """A section handler containing a source directory which is completely copied to a target path."""

    description = 'A section handler containing a source directory which is completely copied to a target path.'
    section_id = 'static_dir'

    known_values = (
        KnownValue('source', 'The source path, relative to the root path in the section header.'),
        KnownValue('target', 'The path relative to the target system.'),
        KnownValue('exclude',
                   'Files matching this pattern will not be copied. For further information on pattern '
                   'see `man 1 rsync`.',
                   default=[],
                   attribute_name='excludes',
                   required=False, unique=False)
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.source = None
        self.target = None
        self.excludes = []

    def calculate_file_dependencies(self):
        # ignore calculation here, we will rsync the whole directory in deploy_files
        self._dependencies_are_calculated = True

    def deploy_files(self, path):
        if not self._dependencies_are_calculated:
            raise RuntimeError('Dependencies were not calculated correctly for section "%s"!' % self.name)
        exclude_params = []
        for exclude_pattern in self.excludes:
            exclude_params.append('--exclude=%s' % exclude_pattern)
        if self.target.startswith(os.sep):
            # target path is always relative to the target system path, so we strip leading / here
            self.target = self.target[1:]
        target = safe_dir_join(path, self.target)
        os.makedirs(target, exist_ok=True)
        rsync(self.source + os.sep, target, parameters=['-ax', '--keep-dirlinks'] + exclude_params)
