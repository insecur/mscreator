# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

from . import ConfigSectionHandlerBase, KnownValue
from ..commands import sfdisk
from ..config import ConfigError
from ..mount import Mount, create_tree_from_mountinfo
from ..mscglobals import ROOT_PATH
from ..utils import sizestring_to_bytes


class Partition(object):
    """This class represents a disk partition."""

    def __init__(self, partitionnumber, partitiontype, partitionsize, filesystem, mountpoint, mountoptions):
        self.partitionnumber = partitionnumber
        self.partitiontype = partitiontype
        self.size = partitionsize
        self.filesystem = filesystem
        self.mountpoint = mountpoint
        self.mountoptions = mountoptions

    def __str__(self):
        return '%s:%s:%s:%s:%s:%s' % (self.partitionnumber, self.partitiontype, self.size, self.filesystem,
                                      self.mountpoint, self.mountoptions)

    def __repr__(self):  # pragma: no cover
        return 'Partition("%s", "%s", "%s", "%s", "%s", "%s")' % (
            self.partitionnumber, self.partitiontype, self.size,
            self.filesystem, self.mountpoint, self.mountoptions)


def pad_to_sectorsize(current_size, sector_size):
    padding_size = sector_size - current_size % sector_size
    if padding_size == sector_size:
        return current_size
    else:
        return current_size + padding_size


class DiskLayoutSectionHandler(ConfigSectionHandlerBase):
    """This class describes the layout of a virtual hard disk, i.e. the partitioning data.

    Configuration example:
    :Disk Image:disklayout:
    disksize=4 GiB
    partition=83:100 MiB:ext2:/boot:ro
    partition=83:50%:ext4:/:
    partition=83:remaining:ext4:/home:

    :type partitions: list[Partition]
    :type disksize: int or None
    """

    category = 'disklayout'
    description = '''The layout of a virtual hard disk, i.e. the partitioning data.'''
    section_id = 'disklayout'
    known_values = (
        KnownValue('disksize', 'The total size of the disk image.',
                   convert=sizestring_to_bytes),
        KnownValue('sectorsize',
                   'Specifies the sector size to be used. Currently only 512 is allowed. (default: 512).',
                   required=False, convert=int, default=512),
        KnownValue('gap',
                   'Specifies how many sectors (including MBR/GPT) to reserve at the beginning of the disk '
                   '(default: 2048).',
                   required=False, convert=sizestring_to_bytes, default=2048),
        KnownValue('partition',
                   'A string describing a partition (partitiontype:size:filesystem:mountpoint:mountoptions).',
                   required=False, unique=False)
    )

    def __init__(self, name, data, content_lines, start, end):
        self.disksize = None
        self.gap = 2048
        self.partition = []
        self.sectorsize = 512
        self.partitions = []
        super().__init__(name, data, content_lines, start, end)

    def _check_mountpoints(self):
        """Check if mount points make sense. Currently we only check if a root partition is specified.

        :raise ConfigError: if no root partition is given
        """
        partitions_by_mountpoint = {partition.mountpoint: partition for partition in self.partitions}
        mountpoints = sorted(partitions_by_mountpoint.keys())
        if mountpoints[0] != ROOT_PATH:
            raise ConfigError('No root partition given!', location=self.content_lines[self.start], exact=False)

    def post_load_init(self):
        super().post_load_init()
        if len(self.partition) > 4:
            raise ConfigError('A disk can have a maximum of 4 partitions. Extended Partitioning is not supported, yet!',
                              location=self.content_lines[self.start], exact=False)
        if self.gap < 1:
            raise ConfigError('Gap must be at least 1 (to allow MBR)!',
                              location=self.content_lines[self.start], exact=False)
        if self.sectorsize != 512:
            raise ConfigError('Currently the sector size must be 512!',
                              location=self.content_lines[self.start], exact=False)
        self._parse_partitions(self.partition)
        self._check_mountpoints()

    def apply(self, device_path):
        partition_start = self.gap
        sfdisk_stdin = ''
        for partition in self.partitions:
            partition_size = int(partition.size / self.sectorsize)
            sfdisk_stdin += '%s,%s,%s\n' % (partition_start, partition_size, partition.partitiontype)
            partition_start += partition_size
        sfdisk.partition_disk(device_path, sfdisk_stdin)

    def _parse_partitions(self, partition_strings):
        partitions = []
        remaining_partition = None
        gapsize = self.gap * self.sectorsize
        total_size = gapsize
        for i, partition in enumerate(partition_strings):
            partitiontype, sizestring, filesystem, mountpoint, mountoptions = partition.split(':')
            partitionnumber = i + 1
            if sizestring == 'remaining':
                # do not reserve bytes, the real size will be calculated later
                remaining_partition = i
                size = -1
            elif sizestring[-1] == '%':
                size = pad_to_sectorsize((self.disksize - gapsize) / 100 * int(sizestring[:-1]), self.sectorsize)
                total_size += size
            else:
                size = pad_to_sectorsize(sizestring_to_bytes(sizestring), self.sectorsize)
                total_size += size
            partitions.append(Partition(partitionnumber, partitiontype, size, filesystem, mountpoint, mountoptions))

        if self.disksize < total_size:
            raise ConfigError('Configured partitions need more space than disk size!',
                              location=self.content_lines[self.start], exact=False)
        if remaining_partition is not None:
            remaining_size = self.disksize - total_size
            if remaining_size <= 0:
                raise ConfigError('Size for "remaining" partition would be 0!',
                                  location=self.content_lines[self.start], exact=False)
            # no need to pad remaining size...
            partitions[remaining_partition].size = remaining_size
        self.partitions = partitions

    def get_mount_tree(self, device_base='/dev/loop0p'):
        """

        :rtype: Mount
        """
        partitions_by_mountpoint = {partition.mountpoint: partition for partition in self.partitions}
        mounts = {}
        mountpoints = sorted(partitions_by_mountpoint.keys())
        for mountid, mountpoint in enumerate(mountpoints):
            if mountpoint == ROOT_PATH:
                partition = partitions_by_mountpoint[mountpoint]
                mounts[mountid + 1] = Mount([1, 0, '0:0', ROOT_PATH, ROOT_PATH,
                                             partition.mountoptions, '', '-', partition.filesystem,
                                             '%s%s' % (device_base, partition.partitionnumber), 'rw'],
                                            blockdevice=True, partitionnumber=partition.partitionnumber)
                continue
            for parentid in range(mountid - 1, -1, -1):
                if mountpoint.startswith(mountpoints[parentid]):
                    partition = partitions_by_mountpoint[mountpoint]
                    mounts[mountid + 1] = Mount([mountid + 1, parentid + 1, '0:0', ROOT_PATH, mountpoint,
                                                 partition.mountoptions, '', '-', partition.filesystem,
                                                 '%s%s' % (device_base, partition.partitionnumber), 'rw'],
                                                blockdevice=True, partitionnumber=partition.partitionnumber)
                    break
        return create_tree_from_mountinfo(mounts)

    @property
    def root_index(self):
        """Returns the partition index of the root partition.

        :rtype: int
        """
        for partition in self.partitions:
            if partition.mountpoint == ROOT_PATH:
                return partition.partitionnumber

    @property
    def boot_index(self):
        for partition in self.partitions:
            if partition.mountpoint == '/boot':
                return partition.partitionnumber
        return self.root_index
