# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module defines the initramfs target system."""

import os

from . import TargetSystemHandlerBase, KnownValue
from .. import check_target_overwrite, mscglobals
from ..commands import find, Command
from ..msclogging import get_logger
from ..utils import safe_dir_join

logger = get_logger()


class InitramfsSectionHandler(TargetSystemHandlerBase):
    """Represents a initramfs target system.

    :type compression_command: str or None
    :type image_path: str
    """

    description = 'The initramfs target system handler.'
    section_id = 'initramfs'
    known_values = (
        KnownValue('image_path', 'The path of the initramfs image.',
                   convert=os.path.abspath),
        KnownValue('compression_command',
                   'The command used to compress the initramfs image, to create an uncompressed image, '
                   'set this to an empty string. (default: gzip)',
                   required=False, default='gzip')
    ) + TargetSystemHandlerBase.known_values

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.image_path = None
        self.compression_command = None

    def post_load_init(self):
        super().post_load_init()
        if not self.compression_command:
            self.compression_command = None
        temp_dir = mscglobals.CONFIG.main.temporary_directory
        self.livedir = safe_dir_join(temp_dir, 'diskmount')
        check_target_overwrite(self.image_path)

    def build_system(self):
        if os.path.exists(self.image_path):
            logger.info('Removing old initramfs image "%s"...', self.image_path)
            os.unlink(self.image_path)
        self.deploy_filesource_files(self.livedir)
        self.call_hook('pre_finalize')

        oldcwd = os.getcwd()
        os.chdir(self.livedir)
        files = list(find())
        find_output = '\n'.join(files) + '\n'
        if './init' not in files:
            logger.warning('Archive will have no /init, which is required by initramfs.')

        # we use bash -c 'cpio|gzip' or similar because connecting two processes via subprocess sucks with pythons
        # unicode encode/decode stuff and may cause strange results with certain binary data
        cmdline = 'cpio -o -H newc'
        if self.compression_command is not None:
            cmdline += ' | %s' % self.compression_command
        command = Command(command_path='/bin/sh')
        with open(self.image_path, 'w') as fd:
            command(parameters=['-c', cmdline], stdin=find_output, stdout=fd, cwd=self.livedir)
        os.chdir(oldcwd)
