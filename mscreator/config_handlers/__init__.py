# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module contains classes for handling special sections like main or hooks.
It also contains base classes for other section handlers."""

import os
from abc import abstractmethod, ABCMeta

from .. import mscglobals
from ..commands import rsync
from ..config import (ConfigRaw, ConfigEmpty, ConfigComment, KeyValueEntry, ConfigFileLine, get_config_traceback_string,
                      ConfigError)
from ..msclogging import get_logger
from ..utils import temporary_file, replace_bad_format_characters, Version

logger = get_logger()


def get_section_by_name(section_name):
    """Return a section handler object for the given name. If the given name is None, return None.

    :type section_name: str or None
    :rtype: ConfigSectionHandlerBase or InlineScriptSectionHandler or None
    :raise ConfigError: if section name cannot be found
    """
    if section_name is None:
        return None
    try:
        return mscglobals.CONFIG.sections[section_name]
    except KeyError:
        raise ConfigError('Could not find section "%s"!' % section_name)


def parse_filesource_string(filesources_string):
    """Takes a comma-separated string of section names and returns a list of section handlers for all given
    file sources.

    :type filesources_string: str
    :rtype: list[FileSourceHandlerBase]
    """
    all_sections = mscglobals.CONFIG.sections
    if '*' in filesources_string:
        return [s for s in all_sections.values() if s.category == 'filesource']
    values = [x.strip() for x in filesources_string.split(',')]
    try:
        sources = [get_section_by_name(x) for x in values if x != '']
    except KeyError:
        raise ConfigError('Invalid section reference in "filesources=%s"!' % filesources_string)
    return sources


class KnownValue(object):
    """If strict_convert is True, convert must succeed. If not and convert fails, the default value is instead
    passed to the convert function and the result is returned.
    """
    def __init__(self, name, description, required=True, default=None, unique=True, convert=None, strict_convert=True,
                 post_load_convert=None, attribute_name=None):
        super().__init__()
        self.name = name
        self.description = description
        self.required = required
        self.default = default
        self.unique = unique
        self._convert = convert
        self._strict_convert = strict_convert
        self._post_load_convert = post_load_convert
        self._attribute_name = attribute_name

    @property
    def attribute_name(self):
        if self._attribute_name is None:
            return self.name
        else:
            return self._attribute_name

    def convert(self, value):
        # noinspection PyBroadException
        try:
            if callable(self._convert):
                return self._convert(value)
            else:
                return value
        except Exception:  # pylint: disable=broad-except
            if self._strict_convert:
                raise
            else:
                return self._convert(self.default)

    def post_load_convert(self, value):
        if self._post_load_convert is None:
            return value
        else:
            return self._post_load_convert(value)

    def __lt__(self, other):
        return self.name < other.name

    @property
    def required_str(self):
        return '*' if self.required else ' '


class SectionHandlerRegistry(type):
    """A metaclass which registers all instances.

    :type handlers: dict[str, ConfigSectionHandlerBase]
    """
    section_id = None
    handlers = None

    def __new__(mcs, name, bases, attrs):
        if mcs.handlers is None:
            mcs.handlers = {}
        cls = super().__new__(mcs, name, bases, attrs)
        key = cls.section_id
        if key in mcs.handlers:
            logger.warning('Ignoring additional handler for "%s"!', key)
        else:
            logger.debug('Registering section handler "%s" for section id "%s"', name, key)
            if key is not None:
                mcs.handlers[key] = cls
        return cls


class _ConfigSectionHandlerMeta(SectionHandlerRegistry, ABCMeta):
    """This class, together with _BaseConfigSection and _AbcMixin, is necessary to use more than one metaclass."""
    pass


class ConfigSectionHandlerBase(object, metaclass=_ConfigSectionHandlerMeta):
    """The base class for all config section providers.

    :cvar section_id: a unique string identifying the section handler
    :cvar description: some text describing the purpose of the handler
    :cvar data_description: some text describing what the data part of the header is used for
    :cvar known_values: a list describing the values known to this section handler
    :type known_values: list[KnownValue]
    :cvar ignore_value_attributes: a list of value names for which the the class attribute is not updated,
        '*' to ignore all
    :type ignore_value_attributes: list[str] or str

    :param name: a descriptive name for this section
    :type name: str
    :param data: a string that may contain additional data to be passed to the section handler, e.g. the root path
        for a file section handler
    :type data: str
    :param content_lines: a list containing all lines from the config file
    :type content_lines: list[mscreator.ConfigFileLine]
    :param start: the first item from `content_lines` belonging to this section
    :type start: int
    :param end: the last item from `content_lines` belonging to this section
    :type end: int
    """
    section_id = None
    description = None
    data_description = 'not used'
    category = 'unspecified'
    known_values = []
    ignore_value_attributes = []

    def post_load_init(self):
        self.data = self.data % mscglobals.CONFIG.variables
        for entry in self._values:
            if isinstance(entry, KeyValueEntry):
                settings = self._get_value_settings(entry.key)
                entry.value = settings.post_load_convert(entry.value)
        self._expand_variables()
        if self.ignore_value_attributes == '*':
            logger.debug('Not updating any attributes from values.')
        else:
            self._set_attributes_from_values()

    def __init__(self, name, data, content_lines, start, end):
        super().__init__()
        self.name = name
        if data is None:
            self.data = ''
        else:
            self.data = data
        self.start = start
        self.end = end
        self.content_lines = content_lines
        self.calculated_dependencies = set()
        self._values = []
        self._unset_optional_values = []
        self._parse_lines(content_lines, start, end)

    def has_known_value(self, name):
        return any([value.name == name for value in self.known_values])

    def _get_value_settings(self, name):
        """Return a known value object.

        :type name: str
        :rtype: KnownValue
        """
        for value in self.known_values:
            if value.name == name:
                return value
        return KnownValue(name, None)

    def _ensure_unique_value(self, key, line):
        try:
            _ = self[key]
            raise ConfigError('Duplicate key "%s"!' % key, location=line)
        except KeyError:
            pass

    def _parse_lines(self, lines, start, end):
        current_line = start + 1
        unsatisfied_values = set([v.name for v in self.known_values if v.required])
        while current_line <= end:
            configline = lines[current_line]
            line = configline.content.lstrip(' \t')
            if line == '':
                self._values.append(ConfigEmpty(configline))
            elif line.startswith(('#', ';')):
                self._values.append(ConfigComment(configline))
            elif line.startswith('='):
                self._values.append(KeyValueEntry(configline, '', line[1:]))
            else:
                try:
                    key, value = line.split('=', 1)
                except ValueError:
                    self._values.append(ConfigRaw(configline))
                else:
                    unsatisfied_values.discard(key)
                    if self.has_known_value(key):
                        value_options = self._get_value_settings(key)
                        if value_options.unique:
                            self._ensure_unique_value(key, configline)
                        value = value_options.convert(value)
                    self._values.append(KeyValueEntry(configline, key, value))
            current_line += 1

        for value_name in unsatisfied_values:
            description = self._get_value_settings(value_name).description
            raise ConfigError('Required value "%s"(%s) not provided!' % (value_name, description),
                              location=self.content_lines[self.start], exact=False)

        # set default values for unset optional values
        self._unset_optional_values = [k.name for k in self.known_values if k.name not in self]
        for value_name in self._unset_optional_values:
            value = self._get_value_settings(value_name)
            default_value = value.default
            default_value = value.convert(default_value)
            logger.debug('setting optional value "%s" to default "%s"', value_name, default_value)
            self._values.append(
                KeyValueEntry(
                    ConfigFileLine(
                        '%s=%s' % (value_name, default_value)), value_name, default_value))

    def __repr__(self):  # pragma: no cover
        return '%s("%s")' % (self.__class__.__name__, self.name)

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self._values)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._values[item]
        for entry in self._values:
            if isinstance(entry, KeyValueEntry):
                if entry.key == item:
                    return entry.value
        raise KeyError('Key "%s" not found' % item)

    def items(self):
        items_list = []
        for entry in self._values:
            if isinstance(entry, KeyValueEntry):
                items_list.append((entry.key, entry.value))
        return items_list

    def __contains__(self, item):
        for entry in self._values:
            if isinstance(entry, KeyValueEntry):
                if entry.key == item:
                    return True
        return False

    def _expand_variables(self):
        """Expands all variables in the section's key=value settings."""
        variables = mscglobals.CONFIG.variables.copy()
        variables.update(self.items())
        rerun = True
        while rerun:
            rerun = False
            for entry in self._values:
                if not isinstance(entry, KeyValueEntry):
                    continue
                key = entry.key
                value = entry.value
                if isinstance(value, str):
                    # damnit, we might want to use some other template string mechanism one day
                    value = replace_bad_format_characters(value)
                    try:
                        value = value % variables
                    except KeyError as exc:
                        bad_key = exc.args[0]
                        raise ConfigError('Specified unknown variable "%s" in value "%s=%s"!' % (bad_key, key, value))
                    except TypeError as exc:
                        bad_key = exc.args[0]
                        raise ConfigError('Encountered TypeError("%s") in value "%s=%s"!' % (bad_key, key, value))

                    if '%(' in value:
                        # check so we do not need to do not need to consider the order in which we expand variables
                        logger.debug('Could not expand all variables in "%s", rescheduling...', value)
                        rerun = True
                        continue
                entry.value = value
                variables[key] = value

    def _set_attributes_from_values(self):
        """Iterate over all key=value settings and, if an attribute with the same name exists update it's value."""
        items = list(self.items())
        while len(items) > 0:
            key, value = items.pop(0)
            settings = self._get_value_settings(key)
            attribute_name = settings.attribute_name
            if key in self.ignore_value_attributes:
                logger.debug('Ignoring attribute value "%s" in section "%s"...', key, self.name)
                continue
            if not hasattr(self, attribute_name):
                logger.debug('Section "%s" has no attribute "%s", not updating...', self.name, attribute_name)
                continue
            if settings.unique:
                setattr(self, attribute_name, value)
            else:
                attr = getattr(self, attribute_name, [])
                if not isinstance(attr, list):
                    setattr(self, attribute_name, [])
                    attr = getattr(self, attribute_name, [])
                if isinstance(value, (list, tuple, set)):
                    attr.extend(value)
                else:
                    attr.append(value)


class VariablesSectionHandler(ConfigSectionHandlerBase):
    """Contains variables that can be used in other section's values.

    :ivar dict: a dictionary containing all KeyValueEntry values in this section
    """
    description = "Contains variables that can be used in other section's values."
    section_id = 'variables'
    ignore_value_attributes = '*'

    def __init__(self, name, data, content_lines, start, end):
        # FIXME: we have nasty dependencies here if using unique=True:
        # _ensure_unique_values() of ConfigSectionHandlerBase is called in constructor and depends on our
        # __getitem__ which in turn reads from our dict attribute, but dict can only be filled after calling the
        # constructor of ConfigSectionHandlerBase
        super().__init__(name, data, content_lines, start, end)
        self.dict = {}
        for i in self._values:
            if isinstance(i, KeyValueEntry):
                self.dict[i.key] = i.value
            elif isinstance(i, (ConfigComment, ConfigEmpty)):
                pass
            else:
                config_traceback = ''
                if hasattr(i, 'configline'):
                    config_traceback = get_config_traceback_string(i.configline)
                raise ConfigError('Only key=value pairs, '
                                  'comments and empty lines are allowed in variables sections! %s' % config_traceback)

    def __getitem__(self, item):
        return self.dict[item]

    def update(self, other):
        self.dict.update(other)

    def keys(self):
        return self.dict.keys()

    def copy(self):
        return self.dict.copy()

    def items(self):
        return self.dict.items()


class HooksSectionHandler(VariablesSectionHandler):
    """The hooks section contains references to scripts that are called before or after certain build phases."""
    description = ('The hooks section contains references to scripts that are called '
                   'before or after certain build phases.')
    section_id = 'hooks'
    ignore_value_attributes = []
    known_values = (
        KnownValue('pre_handle_sections', 'Is called before any section handler is processed.',
                   post_load_convert=get_section_by_name,
                   required=False, unique=False),
        KnownValue('pre_finalize',
                   '''Is called after all files have been copied to the target filesystem, but before unmounting the '''
                   '''target's filesystem hierarchy. This is a good place to "customize" the system.''',
                   post_load_convert=get_section_by_name,
                   required=False, unique=False),
        KnownValue('pre_build_system',
                   "Is called before building a target, "
                   "i.e. just before creating the target system's disk and mounting it.",
                   post_load_convert=get_section_by_name,
                   required=False, unique=False),
        KnownValue('post_build_system',
                   'Is called after building a target, i.e. just before the next target is built. This is a good place '
                   'to create a ZIP file from the resulting disk image.',
                   post_load_convert=get_section_by_name,
                   required=False, unique=False),
        KnownValue('pre_cleanup',
                   'Is called after all targets are built, but before the build environment has been cleaned.',
                   post_load_convert=get_section_by_name,
                   required=False, unique=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.pre_handle_sections = []
        self.pre_finalize = []
        self.pre_build_system = []
        self.post_build_system = []
        self.pre_cleanup = []
        for key in self.dict:
            if not self.has_known_value(key):
                raise ConfigError('Unknown hook "%s"!' % key, location=self.content_lines[self.start], exact=False)

    def call(self, phase_name, env=None, target=None):
        if env is None:
            env = {}
        env['MSC_phase'] = phase_name
        if target is not None:
            env['MSC_targetsystem'] = target.name
            env['MSC_livedir'] = target.livedir
            # FIXME: if several entries with the same key name exist, only the last one will be available
            env.update({'MSC_TARGET_%s' % k: str(v) for k, v in target.items()})
        section = get_section_by_name(self.dict.get(phase_name))
        if hasattr(section, "run"):
            logger.info('Calling "%s" hook...', phase_name)
            section.run(pipe=False, env=env)
        elif section is not None:
            raise ConfigError('Hook section "%s" is not executable!' % phase_name)


class MainSectionHandler(ConfigSectionHandlerBase):
    """Contains the general config for a system. All lines before the first section make up the main section.

    NOTE: The main section is different from other sections in several ways:
      * Variable expansion will not be applied.
      * ...

    :type targets: list[TargetSystemHandlerBase]
    """

    description = ('Contains the general config for a system. '
                   'All lines before the first section make up the main section. '
                   'The main section must not be specified in a configuration!')
    section_id = 'main'
    known_values = (
        KnownValue('name', 'The name of the target system, will be recorded in /etc/msc-release.'),
        KnownValue('version', 'The version of the target system, will be recorded in /etc/msc-release.'),
        KnownValue('msc_min_version', 'The minimum version of mscreator that is required by this configuration file.',
                   convert=Version, strict_convert=False,
                   default='2.0.0',  # 2.0.0 was the first version to ever support this feature
                   required=False),
        KnownValue('targets', 'A comma separated list containing the names of the target sections to build.'),
        KnownValue('temporary_directory',
                   'The path to a directory where files can be temporarily stored. '
                   '(default: %s)' % mscglobals.DEFAULT_TEMPORARY_DIRECTORY,
                   required=False, default=mscglobals.DEFAULT_TEMPORARY_DIRECTORY),
        KnownValue('download_cache_directory',
                   'The path to a directory where downloaded files can be stored. '
                   '(default: %s)' % mscglobals.DEFAULT_DOWNLOAD_CACHE_DIRECTORY,
                   required=False, default=mscglobals.DEFAULT_DOWNLOAD_CACHE_DIRECTORY),
        KnownValue('build_cache_directory',
                   'The path to a directory where source code files, etc. can be stored. '
                   '(default: %s)' % mscglobals.DEFAULT_BUILD_CACHE_DIRECTORY,
                   required=False, default=mscglobals.DEFAULT_BUILD_CACHE_DIRECTORY),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.temporary_directory = os.path.abspath(self['temporary_directory'])
        self.download_cache_directory = os.path.abspath(self['download_cache_directory'])
        self.build_cache_directory = os.path.abspath(self['build_cache_directory'])
        self.version = self['version']
        self.msc_min_version = self['msc_min_version']
        self.targets = []
        for i in self._values:
            if not isinstance(i, (ConfigComment, ConfigEmpty, KeyValueEntry)):
                raise ConfigError('Only key=value pairs, comments and empty lines are allowed in the main section!',
                                  location=i)

    def post_load_init(self):
        targets = [t.strip() for t in self['targets'].split(',')]
        for target in targets:
            if target != '':
                try:
                    self.targets.append(mscglobals.CONFIG.sections[target])
                except KeyError:
                    raise ConfigError('Could not find section "%s" referenced as target!' % target)


class TargetSystemHandlerBase(ConfigSectionHandlerBase):
    """The base class for all target systems.

    :ivar required_mounts: all paths that have to be treated as mount points when building the system
    :type required_mounts: dict[str, Mount]
    :type bootloader: None or BootloaderHandlerBase
    :type filesources: list[FileSourceHandlerBase]
    """
    category = 'targetsystem'
    known_values = (
        KnownValue('filesources', 'The path of the initramfs image.',
                   post_load_convert=parse_filesource_string,
                   default='',
                   required=False,
                   unique=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.required_mounts = {}
        self.filesources = []
        self.bootloader = None
        self.livedir = None

    def add_mountpoints(self, required_mounts):
        """Record a list of mount point paths to be considered when creating mount handlers such as an fstab file or
        a mount shell script.

        :param required_mounts: an iterable containing paths to the required mount points
        :type required_mounts: dict[str, Mount]
        """
        self.required_mounts.update(required_mounts)

    def deploy_filesource_files(self, target_directory):
        """Copy files from all filesource sections to the "build directory".

        :param target_directory: where the files are deployed
        :type target_directory: str
        """
        for section in self.filesources:
            logger.info('Deploying files for section "%s"...', section.name)
            section.deploy_files(target_directory)
            self.add_mountpoints(section.required_mounts)

    def deploy_kernel_files(self, target_directory):
        for kernel in self.bootloader.boot_entries:
            logger.info('Deploying files for section "%s"...', kernel.name)
            kernel.deploy_files(target_directory)

    def call_hook(self, hook_name):
        mscglobals.CONFIG.hooks.call(hook_name, target=self)

    @property
    @abstractmethod
    def build_system(self):  # pragma: no cover
        """This method makes the system ready for use, e.g. generates a VM image, creates a directory containing the
        system and a start script or whatever the purpose of the handler implementation is."""
        pass


class FileSourceHandlerBase(ConfigSectionHandlerBase):
    """The base class for pure file sources.

    :ivar required_mounts: all paths that reside on an extra mount point
    :type required_mounts: dict[str, Mount]
    """
    category = 'filesource'
    data_description = 'The root path to which all paths in this section relate. Default: "/"'

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.required_mounts = {}
        if self.data == '':
            self.chroot_path = mscglobals.ROOT_PATH
        else:
            self.chroot_path = self.data
        self._dependencies_are_calculated = False

    def post_load_init(self):
        super().post_load_init()
        self.chroot_path = os.path.abspath(self.chroot_path % mscglobals.CONFIG.variables)

    @abstractmethod
    def calculate_file_dependencies(self):  # pragma: no cover
        """Does the actual work of gathering all file dependencies for a config section and stores dependencies in
        self.calculated_dependencies.
        The attribute self._dependencies_are_calculated must also be set to True if the calculation was successful.
        """
        pass

    def filter_pseudo_mounts(self):
        """Scans `self.calculated_dependencies` and removes all paths belonging to a mount point for a pseudo
        filesystem. If paths on a pseudo filesystem are found, the mount point is added to `self.required_mounts`.
        """
        deps = self.calculated_dependencies.copy()
        for path in deps:
            for pseudo_path, mount in mscglobals.PSEUDO_FILESYSTEMS.items():
                if path.startswith(pseudo_path):
                    logger.trace('Path "%s" is on pseudo filesystem, removing from dependency list.', path)
                    self.calculated_dependencies.remove(path)
                    self.calculated_dependencies.add(pseudo_path)
                    self.required_mounts[pseudo_path] = mount
                    break

    def deploy_files(self, path):
        """Copies all file dependencies to a target location using the external rsync utility.
        Classes overriding this method should make sure they test self._dependencies_are_calculated!

        :param path: path where the dependencies should be stored
        :raise RuntimeError: self._dependencies_are_calculated is not set to True
        :raise CommandError: Raised if rsync encounters an error.
        """
        if not self._dependencies_are_calculated:
            raise RuntimeError('Dependencies were not correctly calculated for section "%s"!' % self.name)
        with temporary_file('dependencies.files-', keep=mscglobals.KEEPTEMP) as tempfile:
            tempfile.fd.writelines(['%s\n' % f for f in self.calculated_dependencies])
            tempfile.close()
            rsync(self.chroot_path + os.sep, path + os.sep,
                  parameters=['-ax', '--keep-dirlinks', '--files-from=%s' % tempfile.filename])


class BootloaderHandlerBase(ConfigSectionHandlerBase):
    """The base class for bootloaders.

    :type boot_entries: list[KernelSectionHandler]
    """
    category = 'bootloader'
    boot_entries = []

    @abstractmethod
    def deploy_files(self, path):  # pragma: no cover
        pass

    @abstractmethod
    def write_config(self, path, root_partition_index=1, boot_partition_index=1):  # pragma: no cover
        pass

    @abstractmethod
    def install(self, target_device_path, boot_partition_index=1):  # pragma: no cover
        pass


def list_config_section_handlers():
    """List all loaded ConfigSectionHandlerBase classes.

    :return: a dictionary mapping all section handler's names to their classes.
    :rtype: dict[str, type[ConfigSectionHandlerBase]]
    """
    return ConfigSectionHandlerBase.handlers


def handler_by_section_id(handler_name):
    return ConfigSectionHandlerBase.handlers[handler_name]


def unregister_section_handler(handler_name):
    if handler_name in ConfigSectionHandlerBase.handlers:
        logger.debug('Unregistering section handler "%s".', handler_name)
        try:
            del ConfigSectionHandlerBase.handlers[handler_name]
        except KeyError:
            pass
