# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import errno
import os
import stat

from . import ConfigSectionHandlerBase, VariablesSectionHandler
from .. import mscglobals
from ..commands import Command
from ..config import ConfigFile
from ..exceptions import CommandError
from ..msclogging import get_logger
from ..utils import temporary_file

logger = get_logger()
DEFAULT_PATH = '/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin'


class InlineScriptSectionHandler(ConfigSectionHandlerBase):
    """Handle scripts that are stored in the configuration file."""
    description = 'Handle scripts that are stored in the configuration file.'
    category = 'inlinefile'
    section_id = 'inlinescript'

    def __init__(self, name, data, content_lines, start, end):
        self._lines = []
        super().__init__(name, data, content_lines, start, end)
        self.filename = None

    def _parse_lines(self, lines, start, end):
        self._lines = ['%s\n' % line.content for line in lines[start + 1:end + 1]]

    def run(self, env=None, pipe=True):
        if env is None:
            env = {}
        with temporary_file(prefix='%s-script-' % self.name, keep=mscglobals.KEEPTEMP) as tempfile:
            tempfile.fd.writelines(self._lines)
            tempfile.close()
            stats = os.stat(tempfile.filename)
            os.chmod(tempfile.filename, stats.st_mode | stat.S_IEXEC)

            new_env = {
                'PATH': DEFAULT_PATH,
            }
            new_env.update({'MSC_MAIN_%s' % k: str(v) for k, v in mscglobals.CONFIG.main.items()})
            new_env.update({'MSC_VARIABLES_%s' % k: str(v) for k, v in mscglobals.CONFIG.variables.items()})
            new_env.update({str(k): str(v) for k, v in env.items()})
            script = Command(command_path=tempfile.filename)
            try:
                stdout = script(env=new_env, pipe=pipe)
            except OSError as exc:
                if exc.errno == errno.ENOEXEC:
                    raise CommandError('Encountered an "Exec format error" while calling an inline script. '
                                       'This usually means you missed the shebang line!')
                elif exc.errno == errno.EACCES:
                    raise CommandError('Encountered a "Permission denied error" while calling an inline script. '
                                       'This usually means the execute permissions are missing on the '
                                       'temporary file "%s. Is /tmp mounted with noexec? Try setting the '
                                       'TMP environment variable to something else.' % tempfile.filename)
                else:
                    raise
            self.filename = tempfile.filename  # record the name of the temp file so we can use it in unit tests
        # noinspection PyUnboundLocalVariable
        return stdout


class DynamicVariablesSectionHandler(InlineScriptSectionHandler):
    """Call a script and use all "key=value" lines in its output as variables."""
    description = '''Call a script and use all "key=value" lines in it's output as variables.'''
    category = 'unspecified'
    section_id = 'dynamicvariables'
    ignore_value_attributes = '*'

    def __init__(self, name, data, content_lines, start, end):
        # FIXME: same dependency problem here as in VariablesSectionHandler?!
        super().__init__(name, data, content_lines, start, end)
        self.dict = {}

    def run(self, env=None, pipe=True):
        output = super().run(env, pipe)
        lines = ['::parsed_%s::variables::' % self.name] + output.split('\n')
        configfile = ConfigFile()
        configfile.from_strings(lines, buffername='<dynamic variables "%s">' % self.name)
        variables_section = VariablesSectionHandler(self.name, None, configfile.lines, 0, configfile.len - 1)
        self.dict = variables_section.dict

    def items(self):
        return self.dict.items()
