# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""This module contains some general purpose functions that do not fit in any other module."""

import ctypes
import errno
import os
import re
import sys
import tempfile

from . import mscglobals
from .exceptions import PathNotFoundError, CriticalError
from .msclogging import get_logger

PATH = os.environ.get('PATH', '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin').split(os.path.pathsep)

logger = get_logger()

_libc = ctypes.CDLL('libc.so.6')
_personality = _libc.personality

_supported_arches = {
    'x86': 8,
    'amd64': 0,
}
_supported_personalities = {v: k for k, v in _supported_arches.items()}


def getarch(personality=None):
    """Return an architecture string for a given personality, or for the current linux personality.

    :param personality: int
    :rtype: str
    """
    if personality is None:
        personality = _personality(0xffffffff)
    architecture = _supported_personalities.get(personality, '<unknown>')
    return architecture


def setarch(new_architecture=None):
    """Set the linux personality. This is currently only used to set a 32 or 64 bit environment.

    :param new_architecture: either "x86" or "amd64", only return current architecture if None
    :return: the old architecture
    :rtype: str
    :raise RuntimeError: if the architecture cannot be set
    """
    if new_architecture is None:
        return getarch()
    try:
        new_personality = _supported_arches[new_architecture]
    except KeyError:
        raise ValueError('arch must be one of "%s"' % (_supported_arches.keys(),))
    old_personality = _personality(new_personality)
    if old_personality == -1:
        raise RuntimeError('Could not set architecture "%s"' % new_architecture)
    try:
        # noinspection PyProtectedMember
        sys.modules['platform']._uname_cache = None  # pylint: disable=protected-access
    except (KeyError, AttributeError):
        pass
    return getarch(personality=old_personality)


def which(command):
    """Search PATH for command and return the full path.

    :param command: The basename of a command, e.g. 'ls'.
    :type command: str
    :return: The full path to the command.
    :rtype: str
    :raise PathNotFound: if command is not found in PATH
    """
    if command[0] == os.sep and os.access(command, os.X_OK):
        return command
    for path in PATH:
        full_path = os.path.join(path, command)
        if os.access(full_path, os.X_OK):
            return full_path
    raise PathNotFoundError(command)


def symlink(target, link_name, force=False):
    """Create a symbolic link.

    :param target: The target where the symlink will point.
    :type target: str
    :param link_name: The full path to the symlink.
    :type link_name: str
    :param force: Remove the `link_name` path before creating the symlink, if it exists.
    :type force: bool
    :return: None
    """
    if force and os.path.lexists(link_name):
        os.unlink(link_name)
    os.symlink(target, link_name)


def boolean(bool_string):
    """Convert a string to a boolean value.

    True values: 1, true, on, yes, enabled (all case insensitive) or the boolean value True
    False values: all other strings or the boolean value False

    :type bool_string: str or bool
    :rtype: bool
    :raise ValueError: if the argument is not of type bool or str
    """
    if isinstance(bool_string, bool):
        return bool_string
    try:
        return bool_string.lower() in ('1', 'true', 'on', 'yes', 'enabled')
    except Exception:
        raise ValueError('Invalid boolean value "%s"!' % bool_string)


class Version(object):
    def __init__(self, version_string):
        super().__init__()
        self.major = None
        self.minor = None
        self.patch = None
        self.from_string(version_string)

    def __str__(self):
        return "%s.%s.%s" % (self.major, self.minor, self.patch)

    def __int__(self):
        return int("%09d%09d%09d" % (self.major, self.minor, self.patch))

    def __lt__(self, other):
        return int(self) < int(other)

    def __eq__(self, other):
        return int(self) == int(other)

    def from_string(self, version_string):
        try:
            major, minor, patch = version_string.split('.')
            self.major = int(major)
            self.minor = int(minor)
            self.patch = int(patch)
        except Exception:
            raise ValueError('"%s" is not a valid version string' % version_string)


def partition_from_base(device_base, partitionnumber):
    """Return a partition device string based on the block device path containing the partition and the partition
    number.
    :examples:
    >>> partition_from_base('/dev/sda', 1)
    '/dev/sda1'
    >>> partition_from_base('/dev/loop0', 1)
    '/dev/loop0p1'

    :param device_base:
    :type device_base:
    :param partitionnumber:
    :type partitionnumber:
    :return:
    :rtype:
    """
    if device_base[-1] in '0123456789':
        partition_prefix = 'p'
    else:
        partition_prefix = ''
    return '%s%s%s' % (device_base, partition_prefix, partitionnumber)


def get_uuid_by_partition_path(partition_path):
    """Determines the UUID for a partition.

    :param partition_path: the path to the partition device.
    :type partition_path: str
    :rtype: str
    :raise PathNotFoundError: if /dev/disk/by-uuid does not contain a symlink matching `partition_path`
    """
    by_uuid_path = '/dev/disk/by-uuid'
    for uuid in os.listdir(by_uuid_path):
        if partition_path == os.path.realpath(os.path.join(by_uuid_path, os.readlink('%s/%s' % (by_uuid_path, uuid)))):
            return uuid
    raise PathNotFoundError('Could not determine UUID for "%s"' % partition_path)


def parse_intlist(intlist):
    """Parses a string of comma-separated numbers to an array of integers.

    :param intlist: a comma-separated list of
        - integers
        - integer ranges, e.g. 5-9
    :type intlist: str
    :return: list of integers
    :rtype: list
    :examples:
        >>> parse_intlist('7,11,15-17,20')
        [7, 11, 15, 16, 17, 20]
    """
    result = []
    for i in intlist.split(','):
        split_i = i.split('-')
        if len(split_i) == 1:
            result.append(int(split_i[0]))
        elif len(split_i) == 2:
            result.extend(range(int(split_i[0]), int(split_i[1]) + 1))
        else:
            # ignore bad values
            pass  # pragma: no cover
    if len(result) != len(set(result)):
        raise ValueError('Entries in intlist must be unique!')

    return sorted(result)


def compress_intlist(intlist):
    """Takes a list of integers and returns a string of comma-separated numbers.

    :param intlist: a list of integer values
    :type intlist: list
    :return: a comma-separated list of integers and ranges (x-y)
    :rtype: str
    :examples:
        >>> compress_intlist([7, 11, 15, 16, 17, 20])
        '7,11,15-17,20'
    """
    intlist = sorted(intlist)
    if len(intlist) != len(set(intlist)):
        raise ValueError('Entries in intlist must be unique!')
    if len(intlist) == 1:
        return str(intlist[0])
    parts_list = []
    i = 0
    maxi = len(intlist) - 1
    while i < maxi:
        start = intlist[i]
        while intlist[i] == intlist[i + 1] - 1:
            i += 1
            if i >= maxi:
                break
        end = intlist[i]
        i += 1
        if start == end:
            parts_list.append(str(start))
        else:
            parts_list.append('%s-%s' % (start, end))
        if i == maxi:
            parts_list.append(str(intlist[i]))

    return ','.join(parts_list)

SIZE_RE = re.compile('^([0-9]+) *([kMGT])(i?)B$')


def sizestring_to_bytes(size_string):
    """Converts a size specification given as string to the corresponding amount of bytes as integer.
    A valid size specification consists of a number followed either by a decimal byte prefix or a binary byte prefix.
    If only a number, without byte prefix, is given, it is interpreted as bytes.

    Valid decimal prefixes are currently: kB, MB, GB, TB

    Valid binary prefixes are currently: kiB, MiB, GiB, TiB
    :examples:
    >>> sizestring_to_bytes('1MB')
    1000000
    >>> sizestring_to_bytes('1MiB')
    1048576
    >>> sizestring_to_bytes('1 kiB')
    1024

    :param size_string: a size specification as described above
    :type size_string: str
    :rtype: int
    :raise ValueError: if an invalid size string is specified
    """
    try:
        return int(size_string)
    except ValueError:
        pass

    prefixes = ['k', 'M', 'G', 'T']
    match = SIZE_RE.match(size_string)
    if match is not None:
        if match.groups()[2] == '':
            factor = 10 ** ((prefixes.index(match.groups()[1]) + 1) * 3)
        else:
            factor = 2 ** ((prefixes.index(match.groups()[1]) + 1) * 10)
        return int(match.groups()[0]) * factor
    else:
        raise ValueError('Invalid size specification "%s"!' % size_string)


def bytes_to_sizestring(size, base=2, sep=' ', precision=3, suffix='B'):
    """Convert a number(int) to a size specification.

    :param size: the number to be converted
    :type size: int
    :param base: 2 or 10 (e.g. 2 returns 1k for size==1024, 10 returns 1k for size==1000)
    :type base: int
    :param sep: the separator between the number and the dimension
    :type sep: str
    :param precision: number of digits after the decimal point
    :type precision: int
    :param suffix: the suffix to append to the dimension, e.g. 'B' would return 1kB, 'Byte' would return 1kByte)
    :type suffix: str
    :rtype: str
    :raise ValueError: if base is not in (2, 10)
    """
    dec_prefixes = ['', 'k', 'M', 'G', 'T']
    bin_prefixes = ['', 'Ki', 'Mi', 'Gi', 'Ti']
    if base == 2:
        prefixes = bin_prefixes
        divisor = 1024
    elif base == 10:
        prefixes = dec_prefixes
        divisor = 1000
    else:
        raise ValueError('base must be 2 or 10!')
    short_size = size

    plen = len(prefixes)
    for i in range(plen):  # pragma: no branch
        if i + 1 >= plen:
            break
        if short_size >= divisor / 2:
            short_size /= divisor
        else:
            break
    # i is always defined because the prefixes lists are always non-empty
    # noinspection PyUnboundLocalVariable
    return '%0.*f%s%s%s' % (precision, short_size, sep, prefixes[i], suffix)  # pylint: disable=undefined-loop-variable


def abridge_string(string, length=0, astring='....'):
    """Returns an abridged version of a given string.
    If length should be at least `len(astring)+2` or the unabridged string will be returned!

    :param string: the string to be abridged
    :type string: str
    :param length: the maximum target length of the string
    :type length: int
    :param astring: the string to be printed as placeholder for stripped characters
    :type astring: str
    :return: an abridged version of s
    :rtype: str
    """
    string_len = len(string)
    astring_len = len(astring)
    if string_len <= length or astring_len + 2 > length:
        return string
    prelen = length // 4 * 3 - astring_len // 2
    postlen = length - prelen - astring_len
    return string[:prelen] + astring + string[-postlen:]


class chroot(object):  # pylint: disable=invalid-name
    """This is a simple context manager providing a way to enter and exit a chroot environment.
    It abuses the "little imperfection" of chroots that allows to chroot to a directory outside a chroot environment
    if a valid filehandle to this directory exists.

    :type old_root: None or int
    :type old_cwd: None or str
    """

    def __init__(self, path, chroot_to_root=False, arch=None):
        """"chroot" to the given path. Optionally set the architecture (x86, amd64) for the target environment.

        :param path: The new root path.
        :type path: str
        :param chroot_to_root: Usually we do not chroot to '/', set to True if this shall be forced.
        :type chroot_to_root: bool
        :param arch: If other than None, the architecture for the target environment (x86 or amd64).
        :type arch: str
        """
        super().__init__()
        self.path = path
        self.old_root = None
        self.chroot_to_root = chroot_to_root
        self.old_cwd = None
        self.arch = arch
        self.oldarch = None

    def __enter__(self):
        if self.arch is not None:
            self.oldarch = setarch(self.arch)
        if self.path != mscglobals.ROOT_PATH or self.chroot_to_root:
            self.old_root = os.open(mscglobals.ROOT_PATH, os.O_RDONLY)
            logger.debug('Chrooting to "%s"...', self.path)
            self.old_cwd = os.getcwd()
            os.chroot(self.path)
        else:
            logger.debug('Not chrooting to "/".')
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type == SystemExit:  # pragma: no cover
            logger.debug('Chroot exit handler called due to program exit, ignoring!')
            return False
        if self.old_root is not None:
            logger.debug('Leaving chroot "%s"...', self.path)
            os.fchdir(self.old_root)
            os.chroot('.')
            os.close(self.old_root)
        else:
            logger.debug('Did not actually chroot, ignoring!')
        if self.old_cwd is not None:
            os.chdir(self.old_cwd)
        if self.arch is not None:
            setarch(self.oldarch)
        return False


class temporary_file(object):  # pylint: disable=invalid-name
    """A context manager that creates a temporary file in the temporary directory configured in the main section of a
    config file. The file is deleted after leaving the "with" context if not specified otherwise.

    :param prefix: The prefix the filename should start with.
    :type prefix: str
    :param keep: Set this to True to keep the file after leaving the "with" context.
    :type keep: bool

    :ivar fd: the file object to the temporary file
    :type fd: file
    :ivar filename: The absolute path to the temporary file.
    :type filename: str
    """

    def __init__(self, prefix='tmp-', keep=False):
        super().__init__()
        self._prefix = prefix
        self._keep = keep
        self.fd = None
        self.filename = None

    def write(self, data):
        self.fd.write(data)

    def flush(self):
        self.fd.flush()

    def close(self):
        return self.fd.close()

    def __enter__(self):
        temp_dir = mscglobals.CONFIG.main.temporary_directory
        os.makedirs(temp_dir, exist_ok=True)
        fd, name = tempfile.mkstemp(dir=temp_dir, prefix=self._prefix)
        self.fd = os.fdopen(fd, 'w')
        self.filename = name

        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fd.close()
        if not self._keep:
            try:
                os.remove(self.filename)
            except OSError as exc:
                if exc.errno != errno.ENOENT:
                    raise
        return False


def print_at_column(text, position, width=None, absolute_width=True, breakable='\n\t '):  # pragma: no cover
    """Print some text starting at a specified "screen" column. The text will be wrapped if too long.
    The maximum width of the text is determined from the current terminal width if not specified.


    :param text: the text to be printed
    :type text: str
    :param position: at which "screen" column the text will be printed
    :type position: int
    :param width: The maximum width after which text will be wrapped, if None it is set to the current terminal's width.
    :type width: int or None
    :param absolute_width: If True, width represents the maximum width of the wrapped text + position.
                           If False width is only used for the maximum width of the wrapped text.
    :type absolute_width: bool
    :param breakable: a string containing all characters where the string can be spaced
    :type breakable: str
    """

    if width is None:
        width = os.get_terminal_size().columns

    def get_next_chunk(subtext):
        """Return the next chunk of text trimmed to fit width.
        If the first word of subtext exceeds the given width, it is returned anyway and output will look ugly.

        :param subtext: the text to be printed
        :type subtext: str
        :rtype: str
        """
        if len(subtext) <= width:
            return subtext
        chunks = re.split('[%s]' % breakable, subtext)
        if len(chunks[0]) >= width:
            return chunks[0]
        for i in range(len(chunks)):  # pylint: disable=consider-using-enumerate
            sub_length = len(' '.join(chunks[:i + 1]))
            if sub_length > width:
                return subtext[:sub_length - len(chunks[i]) - 1]

    if sys.stdout.isatty():
        if position == 0:
            indent_string = ''
        else:
            indent_string = '\r\033[%sC' % position
    else:
        indent_string = ' ' * position

    if absolute_width:
        width -= position
    start = 0
    while True:
        chunk = get_next_chunk(text[start:])
        print('%s%s' % (indent_string, chunk))
        start += len(chunk) + 1
        if start >= len(text):
            break


def root_is_initial():
    """Check if we are currently running in the same "chroot" as on startup.

    :rtype: bool
    """
    try:
        current_root = os.readlink('/proc/self/root')
        return current_root == mscglobals.INITIAL_ROOT
    except FileNotFoundError:
        return False


def is_script(filepath):
    """Check if a file is interpreted, i.e. starts with #!.

    :type filepath: str
    :rtype: bool
    """
    with open(filepath, 'rb') as fd:
        header = fd.read(2)
    return header == b'#!'

BAD_FORMAT_CHAR_RE = re.compile(r'(?<=%)[^(%]')


def get_bad_format_characters(string):
    """Extract all characters from a given string that follow a % character, except % and (.
    We use this to replace bad % combos when substituting variables in a string.

    :type string: str
    :rtype: str
    """
    return set(BAD_FORMAT_CHAR_RE.findall(string))


def replace_bad_format_characters(string):
    for bad_format_character in get_bad_format_characters(string):
        string = string.replace('%%%s' % bad_format_character, '%%%%%s' % bad_format_character)
    return string


def safe_dir_join(path, *paths):
    """Join given paths and return the result. The result always ends with a trailing /.
    This function runs checks which ensure that the target path does not accidentally
    point to a directory outside the build environment, e.g. /.

    :type path: str
    :rtype: str
    """
    result = os.path.join(path, *paths)
    for subpath in paths:
        if len(subpath) > 0 and subpath[0] == os.sep:
            raise CriticalError('Joining [%s] would result in path "%s/" which might be used for deleting files!' % (
                ', '.join(['"%s"' % p for p in (path,) + paths]), result))

    if result[-1] != os.sep:
        result += os.sep
    return result
