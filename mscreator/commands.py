# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Wrappers for commandline tools."""

import os
import pipes
import shlex
import stat
import subprocess
import sys
import time

from . import mscglobals
from .exceptions import CommandError
from .msclogging import get_logger, TRACE
from .utils import which, root_is_initial, is_script

logger = get_logger()

STAT_TYPES = {
    'socket': stat.S_ISSOCK,
    'symlink': stat.S_ISLNK,
    'file': stat.S_ISREG,
    'block': stat.S_ISBLK,
    'dir': stat.S_ISDIR,
    'char': stat.S_ISCHR,
    'fifo': stat.S_ISFIFO
}
FILE_TYPES = set(STAT_TYPES)


def find(base_paths=None, types=None):  # pylint: disable=too-many-branches
    if types is None:
        types = FILE_TYPES
    else:
        types = set(types)
        if not types.issubset(FILE_TYPES):
            raise ValueError('unknown filetype specified')
    if base_paths is None:
        base_paths = ['.']
    elif isinstance(base_paths, str):
        base_paths = [base_paths]
    for base_path in base_paths:
        if 'dir' in types:
            yield base_path
        for root_dir, subdirs, files in os.walk(base_path):
            if 'dir' in types:
                for subdir in subdirs:
                    yield os.path.join(root_dir, subdir)
            for filename in files:
                for filetype in types:
                    path = os.path.join(root_dir, filename)
                    filestats = os.lstat(path)
                    # noinspection PyCallingNonCallable
                    if STAT_TYPES[filetype](filestats.st_mode):
                        yield path
                        break


def touch(path, times=None):
    with open(path, 'a'):
        os.utime(path, times)


def shell_quote_list(lst):
    return ' '.join([pipes.quote(x) for x in lst])


def retry_umount(mount_command, retry_timeout=0.5):
    """Unmount the given mountpoint or device node. If the call fails, retry after a given time.

    :param mount_command: the arguments to be passed to the umount command (as shell escaped string)
    :type mount_command: str
    :param retry_timeout: the time in seconds after which to retry
    :param retry_timeout: int or float
    """
    try:
        umount(mount_command)
    except CommandError:
        logger.warning('umount failed, retrying in %s second...', retry_timeout)
        time.sleep(retry_timeout)
        umount(mount_command)


def _log_command_output(stderr_data, stdout_data):
    if stdout_data:
        for line in stdout_data.strip().split('\n'):
            logger.trace('>O>: %s', line)
    if stderr_data:
        for line in stderr_data.strip().split('\n'):
            logger.trace('>E>: %s', line)


class CmdPopen(subprocess.Popen):

    def communicate_wait(self, output_encoding, quiet, stdin):
        stdout_data, stderr_data = self.communicate(stdin)
        if not quiet:
            if isinstance(stdout_data, bytes):
                if output_encoding is not None:
                    stdout_data = stdout_data.decode(encoding=output_encoding)
            if isinstance(stderr_data, bytes):
                if output_encoding is not None:
                    stderr_data = stderr_data.decode(encoding=output_encoding)
        return stderr_data, stdout_data


class Command(object):
    """

    :param command_path:
    :type command_path:
    :param detect:
    :type detect:
    """
    _command_path = None

    def __init__(self, command_path=None, detect=False):
        super().__init__()
        if command_path is not None:
            self._command_path = command_path
        if detect:
            self._cmd = which(self._command_path)
        else:
            self._cmd = self._command_path

    def _call(self, parameters=None, stdin=None, stdout=None, pipe=True, env=None, dontfail=False, quiet=False,
              output_encoding='utf-8', cwd=None):
        # pylint: disable=too-many-branches
        if isinstance(parameters, list):
            cmd = [self._cmd] + parameters
        else:
            cmd = [self._cmd]
        if stdin is not None and output_encoding is not None:
            cmd_bash_equivalent = "echo -n '%s' | " % stdin
        else:
            cmd_bash_equivalent = ''
        cmd_bash_equivalent += shell_quote_list(cmd)
        logger.debug('calling "%s"', cmd_bash_equivalent)
        if root_is_initial() and cwd is None:
            cwd = mscglobals.INITIAL_CWD
        if pipe:
            if stdout is None:
                stdout = subprocess.PIPE
            stderr = subprocess.PIPE
        else:
            stderr = None
        if isinstance(env, dict):
            for k, v in env.items():
                if v is None:
                    env[k] = ''
        proc = CmdPopen(cmd, stdout=stdout, stderr=stderr, stdin=subprocess.PIPE,
                        env=env, universal_newlines=True, cwd=cwd)

        stderr_data, stdout_data = proc.communicate_wait(output_encoding, quiet, stdin)
        if proc.returncode != 0:
            if dontfail:
                return None
            else:
                raise CommandError('Error calling "%s":\n---STDERR---\n%s\n---STDOUT---\n%s' %
                                   (cmd_bash_equivalent, stderr_data, stdout_data),
                                   returncode=proc.returncode)
        else:
            if mscglobals.LOGLEVEL == TRACE:
                _log_command_output(stderr_data, stdout_data)
        return stdout_data

    __call__ = _call


class SFDisk(Command):
    _command_path = 'sfdisk'

    def partition_disk(self, device_path, stdin):
        parameters = ['-uS', '-L', device_path]
        self._call(parameters=parameters, stdin=stdin)

    def reread(self, device_path):
        parameters = ['-R', device_path]
        self._call(parameters=parameters)


class LoSetup(Command):
    _command_path = 'losetup'

    def find_free(self):
        parameters = ['-f']
        stdout = self._call(parameters=parameters)
        return stdout.strip()

    def attach(self, path, device_path=None):
        if device_path is None:
            device_path = self.find_free()
        parameters = [device_path, path]
        try:
            self._call(parameters=parameters)
        except CommandError:
            logger.info('Try reloading the loop kernel module.')
            raise
        return device_path

    def detach(self, device_path):
        parameters = ['-d', device_path]
        self._call(parameters=parameters)


class MountCmd(Command):
    _command_path = 'mount'

    def __call__(self, source, mountpoint, parameters=None):
        if parameters is None:
            parameters = []
        parameters += [source, mountpoint]
        stdout = self._call(parameters=parameters)
        return stdout.strip()


class Umount(Command):
    _command_path = 'umount'

    def __call__(self, target, parameters=None):
        if parameters is None:
            parameters = []
        parameters.append(target)
        stdout = self._call(parameters=parameters)
        return stdout.strip()


class PartX(Command):
    _command_path = 'partx'

    def update(self, device_path):
        parameters = ['-u', device_path]
        self._call(parameters=parameters)


class MkFs(Command):
    _command_path = 'mkfs'

    def __call__(self, device_path, fstype):
        self._call(parameters=['-t', fstype, device_path])


class Rsync(Command):
    _command_path = 'rsync'

    def __call__(self, sources, target, parameters=None):
        if target == mscglobals.ROOT_PATH:
            raise RuntimeError('Refusing to rsync to root "/"!')
        if isinstance(sources, str):
            sources = [sources]
        if parameters is None:
            parameters = ['-ax']
        elif isinstance(parameters, str):
            parameters = shlex.split(parameters)
        self._call(parameters=parameters + sources + [target])


class Grub(Command):
    _command_path = 'grub'

    def __call__(self, device_map=None, parameters=None, stdin=None):
        if parameters is None:
            parameters = []
        if device_map is not None:
            parameters.append('--device-map=%s' % device_map)
        self._call(parameters=parameters, stdin=stdin)


class GenericLdd(Command):
    _command_path = 'ldd'

    def __call__(self, filepath, dontfail=False, quiet=False):
        stdout = self._call(parameters=[filepath], dontfail=dontfail, quiet=quiet)
        return stdout.strip()


class QemuImg(Command):
    """This class represents """
    _command_path = 'qemu-img'

    def convert(self, source_image, output_image, output_type, parameters=None, progress=True):
        if parameters is None:
            parameters = []
        parameters.insert(0, 'convert')
        if progress:
            parameters.append('-p')
        parameters.extend(['-O', output_type, source_image, output_image])
        self._call(parameters=parameters, pipe=not progress)


class LinuxLdd(Command):

    def __call__(self, filepath):
        if is_script(filepath):
            return filepath
        for ld_path in ('/lib/ld-linux.so.2', '/lib32/ld-linux.so.2',
                        '/lib64/ld-linux-x86-64.so.2', '/libx32/ld-linux-x32.so.2'):
            if os.path.exists(ld_path):
                self._cmd = ld_path
                try:
                    stdout = self._call(parameters=[filepath], env={'LD_TRACE_LOADED_OBJECTS': '1'},
                                        dontfail=True, quiet=True)
                    if stdout is None:
                        continue
                except (CommandError, OSError):
                    continue
                return stdout.strip()
        raise CommandError('No suitable linux loader library found!)')

# pragma pylint: disable=invalid-name

sfdisk = SFDisk()
losetup = LoSetup()
mount = MountCmd()
umount = Umount()
partx = PartX()
mkfs = MkFs()
rsync = Rsync()
qemu_img = QemuImg()

if sys.platform.startswith('linux'):
    ldd = LinuxLdd()
else:
    ldd = GenericLdd()
