# MSCreator

MSCreator (Minimal System Creator) is a tool for automatically creating stripped down linux systems.

The project is hosted at https://gitlab.com/insecur/mscreator.
Please report bugs and feature requests using the issues system.


# Background

Most of the code is based on old bash and perl scripts I wrote over the years.
I have rewritten all the stuff in Python and if you look at the code you may
still find many things that are extremely over engineered or use strange
techniques to achieve it's goals.
This is, or was at that time, totally intended! Of course! ;)
MSCreator was not only some sort of packaging together old perl scripts, but I
also tried out many things, so don't be too picky, but give me a good reason to
improve the code.


# Dependencies

mandatory:

 - python 3.4+ (3.3 might work but is not tested and supported)
 - rsync

optional (depending on your configuration):

 - strace (http://strace.sf.net)
 - ldd (glibc ;))
 - grub (e.g. http://alpha.gnu.org/gnu/grub/grub-0.97.tar.gz)
 - qemu-img (http://www.qemu.org/)
 - sfdisk (util-linux)
 - losetup (util-linux)
 - mount/umount (util-linux)
 - partx (util-linux)
 - mkfs (util-linux)

Most of the optional dependencies are common commandline tools and should be available via your distributions package manager.


# Installation

`./setup.py install` should usually do the trick.


# Usage

1. Create a configuration for the target system (see section "Configuration" in the main documentation)
2. run `./mscreate <name_of_the_configuration_file>`


# Development

All code changes must pass the `runqa.sh` tests.
The `runqa.sh` script runs the following tests:
 * some simple checks that are not covered by unit tests or pylint
 * pylint (run manually with `runpylint.sh`)
 * unit tests (run manually with `runtests.sh`)


# License stuff

MSCreator is published under the BSD license, a copy should be available in the LICENSE file.
