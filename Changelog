MSCreator 2.0.0 (2016-12-10)
 * incompatibility: drop python2 support: every distribution supports python3
   and maintaining compatibility code seems just too ridiculous for a
   standalone application
 * incompatibility: drop python 3.3 support: 3.3 is rather old, most distros
   ship with 3.4 or higher. It might still work but is officially unsupported
   now.
 * incompatibility: rename package from MSCreator to mscreator: PEP 8
 * feature: add msc_min_version option to main section, i.e. a config can
   require a certain minimum version of mscreator now
 * feature: add new example config "nwmini", a small VirtualBox VM including
   various networking tools
 * feature: allow lower case loglevel names
 * feature: variables are now expanded for all key=value pairs
 * feature: add new section handler that supports building busybox from source
 * fix: expand variables in staticfiles sections
 * fix: expand variables in kernel sections
 * fix: expand variables in filesource section headers
 * fix: use --keep-dirlinks when running rsync to preserve symlinks like
   bin -> usr/bin
 * fix: ldd dependencies are not calculated correctly if path contains symlink
 * internal: remove recursive flag in ldd sections (linux ldd already is recursive)
 * internal: use relative imports
 * internal: restructured modules to get rid of cyclic imports
 * internal: only process file sections referenced in a target
 * internal: introduce class for known values
 * internal: add option to do value conversion in post_load_init()
 * internal: update attributes if a value with the same name exists
 * internal: use safe_dir_join() function to prevent trashing root filesystems
 * internal: use PEP8 coding style
 * internal: add runqa.sh script which runs unittests and all kinds of other
   checks

MSCreator 1.1.0 (2015-05-10)
 * feature: new initramfs target (#7)
 * feature: added #>source= command to be able to include another config file (#6)
 * fix: more precise error messages when parsing configs
 * fix: use relative path in chroot enter script
 * fix: many small bugfixes (uncaught exceptions, ...)
 * internal: some minor improvements
 * internal: code cleanup

MSCreator 1.0.1 (2014-07-30)
 * fix: ldd files not working on some systems (#26)
 * fix: additional newlines in inlinescripts (#25)

MSCreator 1.0.0 (2014-07-27)
 * internal: code cleanup
 * internal: refactorings
 * internal: added many unittests
 * feature: support for setting architecture in chroots (#22)

MSCreator 0.9.0 (2014-06-22)
 * initial (pre-)release
