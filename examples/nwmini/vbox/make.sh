#!/bin/bash
#Use 4.3.32 at least for kernel >=4.2.x
#- download guest additions iso from
#http://download.virtualbox.org/virtualbox/4.3.34/VBoxGuestAdditions_4.3.34.iso
function get_kernel_version() {
    local image=$(basename $(ls /boot/vmlinuz-4.* /boot/bzImage-4.* 2>/dev/null| sort | tail -1))
    local v=$(echo $image | sed -e 's/.*\(vmlinuz\|bzImage\)-//')
    if [ -z "$v" ];then
	    echo "Could find any kernel version! Aborting..." >&2
	    exit 1
    fi
    echo $v
}

set -e -u

VERSION=${1:-}
if [ -z "$VERSION" ];then
    echo "usage: $0 <vbox_version>"
    exit 1
fi

KERNEL_VERSION=$(get_kernel_version)

# ===== download
wget -c http://download.virtualbox.org/virtualbox/${VERSION}/VBoxGuestAdditions_${VERSION}.iso -O VBoxGuestAdditions_${VERSION}.iso

# ===== extract
mkdir -p x
mount -oloop VBoxGuestAdditions_${VERSION}.iso x
cp x/VBoxLinuxAdditions.run .
umount x
./VBoxLinuxAdditions.run --noexec --target x
mkdir -p vboxguest
tar xvf x/VBoxGuestAdditions-amd64.tar.bz2  -C vboxguest
rm -r x

cd vboxguest/src/vboxguest-${VERSION}
make KERN_DIR=/usr/src/kernels/${KERNEL_VERSION}/ -j $(($(nproc)+1))
xz --check=none vboxguest.ko &
xz --check=none vboxsf.ko &
xz --check=none vboxvideo.ko &
wait
#mkdir -p ../../../../src/lib/modules/4.2.6-301.fc23.x86_64/misc
#cp *.ko ../../../../src/lib/modules/4.2.6-301.fc23.x86_64/misc/
#cd ../../../..

