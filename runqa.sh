#!/bin/bash
# vim: set ts=8 sts=8 sw=8 smarttab noexpandtab :
set -e -u

MISC_RESULT=-1
PEP8_RESULT=-1
PYLINT_RESULT=-1
UNITTEST_RESULT=-1
BB_RESULT=-1
QA_OKAY=0

RED="\e[31;1m"
GREEN="\e[32;1m"
RESET="\e[0m"

function print_test_result() {
	local title=$1
	local -i result=$2

	echo -n "$title: "
	if [ ${result} -eq -1 ];then
		echo -e "${RED}no result${RESET}"
		QA_OKAY=1
	elif [ ${result} -eq 1 ];then
		echo -e "${RED}failed${RESET}"
		QA_OKAY=1
	elif [ ${result} -eq 0 ];then
		echo -e "${GREEN}okay${RESET}"
	fi
}

function print_results() {
	echo
	print_test_result "Misc checks  " $MISC_RESULT
	print_test_result "PEP8 checks  " $PEP8_RESULT
	print_test_result "Pylint checks" $PYLINT_RESULT
	print_test_result "Unittests    " $UNITTEST_RESULT
	print_test_result "busybox build" $BB_RESULT
	echo
	if [ $QA_OKAY == 0 ];then
		echo -e "${GREEN}QA successful${RESET}"
	else
		echo -e "${RED}QA failed ${RESET}"
	fi
}

function exit_handler() {
	print_results
	rm -f pylint.report
	echo "QA run took $SECONDS seconds."
}

trap 'echo -e " $RED========== Unhandled error ==========$RESET"' ERR
trap exit_handler EXIT

echo " * running misc checks..."
if grep -rE '^(from|import) mscreator' mscreator; then
	echo "Found non-relative imports of subpackages in mscreator module"
	MISC_RESULT=1
else
	MISC_RESULT=0
fi

echo " * running pep8 checks..."
#NOTE: automatically format files:
#python3-autopep8 --max-line-length=150 -j0 -r --in-place mscreate mscreator tests
pep8 --max-line-length=150 mscreator mscreate tests && PEP8_RESULT=0 || PEP8_RESULT=1

# pylint is slow and only runs on a single core, so start in background
./runpylint.sh &>/dev/null &
PYLINT_PID=$!

echo " * running unit tests..."
./runtests.sh && UNITTEST_RESULT=0 || UNITTEST_RESULT=1

echo " * running busybox checks..."
#TODO: replace with system test
cd tests/manual/busybox
PYTHONPATH=$(realpath ../../..) ../../../mscreate -O -b bb.mscconf && BB_RESULT=0 || BB_RESULT=1
cd -

echo " * running pylint checks..."
wait $PYLINT_PID && PYLINT_RESULT=0 || PYLINT_RESULT=1
cat pylint.report
