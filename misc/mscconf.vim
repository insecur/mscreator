" Vim syntax file
" Language:               MSCreator configuration file
" Original Author:        Tobias Hommel <mscreator@genoetigt.de>
" Current Maintainer:     Tobias Hommel <mscreator@genoetigt.de>
" Homepage:               http://mscreator.genoetigt.de


if exists("b:current_syntax")
  finish
endif

function! TextEnableCodeSnip(filetype,start,end,textSnipHl) abort
  let ft=toupper(a:filetype)
  let group='textGroup'.ft
  if exists('b:current_syntax')
    let s:current_syntax=b:current_syntax
    " Remove current syntax definition, as some syntax files (e.g. cpp.vim)
    " do nothing if b:current_syntax is defined.
    unlet b:current_syntax
  endif
  execute 'syntax include @'.group.' syntax/'.a:filetype.'.vim'
  try
    execute 'syntax include @'.group.' after/syntax/'.a:filetype.'.vim'
  catch
  endtry
  if exists('s:current_syntax')
    let b:current_syntax=s:current_syntax
  else
    unlet b:current_syntax
  endif
  execute 'syntax region textSnip'.ft.'
        \ matchgroup='.a:textSnipHl.'
        \ start="'.a:start.'" end="'.a:end.'"
        \ contains=@'.group
endfunction

" shut case off
syn case match

syn match mscconfHeaderName /^::[^:]*/hs=s+1 contains=mscconfHeaderColon nextgroup=mscconfHeaderId
syn match mscconfHeaderId /::[^:]*/hs=s+1 contains=mscconfHeaderColon nextgroup=mscconfHeaderData contained
syn match mscconfHeaderData /::.*$/hs=s+1 contains=mscconfLabel,mscconfHeaderColon contained
syn match mscconfHeaderColon /::/ contained "in=mscconfHeaderName,mscconfHeaderId,mscconfHeaderData
syn match mscconfLabel    "%([^)]\+)s"
syn match mscconfValueName /^[a-zA-Z0-9_]\+=/ contains=mscconfEqualSign
syn match mscconfEqualSign /=/ containedin=mscconfValueName
syn match mscconfComment  "^\s*[#;].*$" contains=mscconfTodo
syn match mscconfSourceLine "^\s*#>source=.*$"
syn keyword mscconfTodo FIXME TODO XXX NOTE NOTES contained

syn match mscconfError /^::::/
syn match mscconfError /^::[^:]\+::::/
syn match mscconfError /^::[^:]\+::grub1::$/


" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if !exists("did_mscconf_syntax_inits")
  command -nargs=+ HiLink hi def link <args>

  HiLink mscconfHeaderName   Statement
  HiLink mscconfHeaderId  Special
  HiLink mscconfHeaderData String
  HiLink mscconfHeaderColon WarningMsg
  HiLink mscconfValueName Identifier
  HiLink mscconfComment  Comment
  HiLink mscconfSourceLine Type
  HiLink mscconfLabel    Type
  HiLink mscconfEqualSign Delimiter
  HiLink mscconfTodo Todo
  HiLink mscconfError Error
  delcommand HiLink

  call TextEnableCodeSnip('sh', '^#!/bin/bash', '\(^:\)\@=', 'SpecialComment')
  call TextEnableCodeSnip('sh', '^#!/bin/sh', '\(^:\)\@=', 'SpecialComment')
  call TextEnableCodeSnip('python', '^#!\(/usr\)\?/bin/\(env \)\?python\(\d\+\(\.\d\+\)\?\)\?\( .*\|\)$', '\(^:\)\@=', 'SpecialComment')
  call TextEnableCodeSnip('perl', '^#!\(/usr\)/bin/\(perl|env perl\)\d?', '\(^:\)\@=', 'SpecialComment')
endif

let b:current_syntax = "mscconf"

" vim: sts=2 sw=2 et
