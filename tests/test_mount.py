# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

from unittest import TestCase

from mscreator.mount import CommaList


class TestCommaList(TestCase):

    def test_wrong_initial_type(self):
        with self.assertRaisesRegex(TypeError, 'Initial argument to CommaList must be either None, list or a string.'):
            cl = CommaList(123)

    def test_init_empty(self):
        cl = CommaList()
        self.assertListEqual(list(cl), [])

    def test_init_commastring(self):
        cl = CommaList('abc, def,ghi,123')
        self.assertListEqual(list(cl), ['abc', ' def', 'ghi', '123'])

    def test_init_list(self):
        l = ['abc', '5', '234', 'x']
        cl = CommaList(l)
        self.assertListEqual(list(cl), l)

    def test_to_string(self):
        l = ['abc', '5', '234', 'x']
        cl = CommaList(l)
        self.assertEqual(str(cl), 'abc,5,234,x')

    def test_slice_copy(self):
        l = ['abc', '5', '234', 'x']
        cl = CommaList(l)
        cls = cl[:]
        self.assertIsInstance(cls, CommaList)
        self.assertListEqual(cls, l)

    def test_slice(self):
        l = ['abc', '5', '234', 'x']
        cl = CommaList(l)

        cls = cl[:]
        self.assertIsInstance(cls, CommaList)
        self.assertListEqual(cls, l)

        ls = l[:2]
        cls = cl[:2]
        self.assertIsInstance(cls, CommaList)
        self.assertListEqual(cls, ls)

        ls = l[:-1]
        cls = cl[:-1]
        self.assertIsInstance(cls, CommaList)
        self.assertListEqual(cls, ls)

        ls = l[-2:]
        cls = cl[-2:]
        self.assertIsInstance(cls, CommaList)
        self.assertListEqual(cls, ls)
