# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os
from mscreator.exceptions import CommandError

import mscreator
from mscreator.config_handlers.inlinescript import InlineScriptSectionHandler, DEFAULT_PATH
import mscreator.msclogging
from mscreator import MscConfig
from mscreator.mscglobals import DEFAULT_TEMPORARY_DIRECTORY
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


class TestInlinescript(TestBase):

    def test_variables(self):
        """Check that some important variables are available in an inlinescript."""
        self.script_output_path = os.path.join(self.tempdir, 'script_output_standard_variables.txt')

        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::variables::
        blah=blub

        ::TestScript::inlinescript::
#!/usr/bin/python
import os

with open('%(outfile)s', 'w') as fd:
    for key, value in os.environ.items():
        fd.write('%%s=%%s\\n' %% (key, value))
''' % {'outfile': self.script_output_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        script_section = config.sections['TestScript']
        self.assertIsInstance(script_section, InlineScriptSectionHandler)

        script_section.run()
        with open(self.script_output_path) as fd:
            lines = [line.rstrip() for line in fd.readlines()]
        self.assertIn('PATH=%s' % DEFAULT_PATH, lines)
        self.assertIn('MSC_MAIN_targets=', lines)
        self.assertIn('MSC_MAIN_name=testlinux', lines)
        self.assertIn('MSC_MAIN_version=1', lines)
        self.assertIn('MSC_MAIN_temporary_directory=%s' % DEFAULT_TEMPORARY_DIRECTORY, lines)
        self.assertIn('MSC_VARIABLES_blah=blub', lines)

    def test_additional_variables(self):
        """Check that additional variables are available in an inlinescript."""
        self.script_output_path = os.path.join(self.tempdir, 'script_output_additional_variables.txt')

        c = '''
        name=testlinux
        version=1
        targets=

        ::TestScript::inlinescript::
#!/usr/bin/python
import os

with open('%(outfile)s', 'w') as fd:
    for key, value in os.environ.items():
        fd.write('%%s=%%s\\n' %% (key, value))
''' % {'outfile': self.script_output_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 2)
        script_section = config.sections['TestScript']
        self.assertIsInstance(script_section, InlineScriptSectionHandler)

        script_section.run(env={'my_test_variable': 'my_test_value'})
        with open(self.script_output_path) as fd:
            lines = [line.rstrip() for line in fd.readlines()]
        self.assertIn('PATH=%s' % DEFAULT_PATH, lines)
        self.assertIn('MSC_MAIN_targets=', lines)
        self.assertIn('MSC_MAIN_name=testlinux', lines)
        self.assertIn('MSC_MAIN_version=1', lines)
        self.assertIn('MSC_MAIN_temporary_directory=%s' % DEFAULT_TEMPORARY_DIRECTORY, lines)
        self.assertIn('my_test_variable=my_test_value', lines)

    def test_script_newlines_from_buffer(self):
        """Check that no additional newlines are added to the script when reading config from a buffer."""
        c = '''
        name=testlinux
        version=1
        targets=

        ::TestScript::inlinescript::
#!/bin/sh
echo 123

echo 234
'''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        mscreator.mscglobals.KEEPTEMP = True

        script_section = config.sections['TestScript']
        self.assertIsInstance(script_section, InlineScriptSectionHandler)
        script_section.run()
        with open(script_section.filename, 'rb') as fd:
            lines = fd.read()
        self.assertEqual(lines, b'#!/bin/sh\necho 123\n\necho 234\n')

    def test_script_newlines_from_file(self):
        """Check that no additional newlines are added to the script when reading config from a file."""
        mscconf = os.path.join(self.tempdir, 'testlinux.mscconf')
        c = '''
        name=testlinux
        version=1
        targets=

        ::TestScript::inlinescript::
#!/bin/sh
echo 123

echo 234
'''
        with open(mscconf, 'wb') as fd:
            fd.write(c.encode('utf-8'))
        config = MscConfig(filename=mscconf)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        mscreator.mscglobals.KEEPTEMP = True

        script_section = config.sections['TestScript']
        self.assertIsInstance(script_section, InlineScriptSectionHandler)
        script_section.run()
        with open(script_section.filename, 'rb') as fd:
            lines = fd.read()
        self.assertEqual(lines, b'#!/bin/sh\necho 123\n\necho 234\n')

    def test_script_exec_format_error(self):
        """Check that some important variables are available in an inlinescript."""
        self.script_output_path = os.path.join(self.tempdir, 'script_output_standard_variables.txt')

        c = '''
        name=testlinux
        version=1
        targets=

        ::TestScript::inlinescript::
        echo test
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        script_section = config.sections['TestScript']
        self.assertIsInstance(script_section, InlineScriptSectionHandler)

        with self.assertRaisesRegex(CommandError, 'Encountered an.*shebang line!'):
            script_section.run()


class TestDynamicVariablesSection(TestBase):

    def test_basic(self):
        """Check that key/value pairs created by a dynamic variables section are present."""
        c = '''
        name=testlinux
        version=1
        targets=

        ::dyn vars::dynamicvariables::
#!/bin/sh
echo foo=bar
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertTrue(all(i in config.variables.items() for i in [('foo', 'bar')]))
