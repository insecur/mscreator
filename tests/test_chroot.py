# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
import stat
from unittest import skipIf
import logging

import mscreator
from mscreator import MscConfig
from mscreator.config_handlers.filesource_lddfiles import LddFilesSectionHandler
from mscreator.config_handlers.filesource_staticfiles import StaticFilesSectionHandler
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from mscreator.exceptions import PathExistsError
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


@skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
class TestChrootSection(TestBase):

    def test_basic(self):
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%(target_dir)s
        filesources=static files
        start_script=%(target_dir)s/root/enter_chroot.sh

        ::static files::static_files::/
        /etc/fstab
        ''' % {'target_dir': target_directory}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        files_section = config.sections['static files']
        files_section.calculate_file_dependencies()
        chroot_section = config.sections['Chroot']
        chroot_section.build_system()

        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static files'], StaticFilesSectionHandler)
        self.assertTrue(os.path.exists(os.path.join(target_directory, 'etc/fstab')))
        target_start_script = os.path.join(target_directory, 'root', 'enter_chroot.sh')
        self.assertTrue(os.path.exists(target_start_script))
        st_mode = os.stat(target_start_script).st_mode
        self.assertNotEqual(st_mode & stat.S_IXUSR, 0)
        self.assertNotEqual(st_mode & stat.S_IXGRP, 0)
        self.assertNotEqual(st_mode & stat.S_IXOTH, 0)

    def test_overwrite_existing(self):
        """Check that an existing target is only overwritten if specified explicitly."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%(target_dir)s
        filesources=static files
        start_script=%(target_dir)s/enter_chroot.sh

        ::static files::static_files::/
        /etc/fstab
        ''' % {'target_dir': target_directory}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        files_section = config.sections['static files']
        files_section.calculate_file_dependencies()
        os.makedirs(target_directory)
        with self.assertRaisesRegex(PathExistsError, 'already exists, use -O'):
            config.parse()

        old_overwrite = mscreator.mscglobals.OVERWRITE_TARGETS
        mscreator.mscglobals.OVERWRITE_TARGETS = True
        config.parse()
        mscreator.mscglobals.OVERWRITE_TARGETS = old_overwrite

    def test_deps_on_mountpoints(self):
        """Check that paths on mountpoints are filtered."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%(target_dir)s
        filesources=static files
        start_script=%(target_dir)s/enter_chroot.sh

        ::static files::static_files::/
        /etc/fstab
        /dev/null
        ''' % {'target_dir': target_directory}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        files_section = config.sections['static files']
        self.assertIsInstance(files_section, StaticFilesSectionHandler)
        files_section.calculate_file_dependencies()
        files_section.filter_pseudo_mounts()
        self.assertIn('/dev', files_section.required_mounts.keys())
        self.assertNotIn('/dev/null', files_section.calculated_dependencies)

        chroot_section = config.sections['Chroot']
        chroot_section.build_system()
        self.assertTrue(os.path.exists(os.path.join(target_directory, 'dev')))
        self.assertFalse(os.path.exists(os.path.join(target_directory, 'dev', 'null')))

    def test_deploy_symlinked_lib_dir(self):
        """Check that symlinks are correctly deployed."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=ldd files

        ::ldd files::ldd_files::%s
        /bin/bitbinary64
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        ldd_section = config.sections['ldd files']
        chroot_section = config.sections['Chroot']
        self.assertIsInstance(chroot_section, ChrootSectionHandler)
        self.assertIsInstance(ldd_section, LddFilesSectionHandler)

        ldd_section.calculate_file_dependencies()
        chroot_section.build_system()

        lib64_path = os.path.join(chroot_section.target_directory, 'lib64')
        self.assertTrue(os.path.islink(lib64_path), msg="%s is expected to be a symlink." % lib64_path)
