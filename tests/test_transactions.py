# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from unittest import TestCase

from mscreator.transactions import transaction
# noinspection PyUnresolvedReferences
import tests.tools


class MyTestError(Exception):
    pass


class TestTransaction(TestCase):

    def setUp(self):
        self.x = 0
        self.y = 0

    def _decx(self):
        self.x -= 1

    def _decy(self):
        self.y -= 1

    def test_transaction_undo_on_success(self):
        """Test if action is executed if undo_on_success is True."""
        with transaction(undo_on_success=True) as t:
            self.x += 1
            t.add(self._decx)
        self.assertEqual(self.x, 0)

    def test_transaction_no_undo_on_success(self):
        """Test if action is not executed if undo_on_success is False."""
        with transaction(undo_on_success=False) as t:
            self.x += 1
            t.add(self._decx)
        self.assertEqual(self.x, 1)

    def test_transaction_undo_on_fail(self):
        """Test if rollback action is executed on fail."""
        try:
            with transaction() as t:
                self.x += 1
                t.add(self._decx)
                raise MyTestError('unimportant')
                # noinspection PyUnreachableCode
                self.x = 2
        except MyTestError:
            pass
        self.assertEqual(self.x, 0)

    def test_transaction_override_undo_on_success(self):
        """Test that action is not executed on success, if undo_on_success is overridden with False."""
        with transaction(undo_on_success=True) as t:
            self.x += 1
            t.add(self._decx)
            self.y += 1
            t.add(self._decy, undo_on_success=False)
        self.assertEqual(self.x, 0)
        self.assertEqual(self.y, 1)

    def test_transaction_override_undo_on_success_if_fail(self):
        """Test if action is executed on fail, even if undo_on_success is overridden with False."""
        try:
            with transaction(undo_on_success=True) as t:
                self.x += 1
                t.add(self._decx)
                self.y += 1
                t.add(self._decy, undo_on_success=False)
                raise MyTestError('unimportant')
        except MyTestError:
            pass
        self.assertEqual(self.x, 0)
        self.assertEqual(self.y, 0)

    def test_transaction_single_argument(self):
        """Test if adding an undo action with one single argument works."""
        x = [0]

        def set_x(value):
            x[0] = value
        self.assertEqual(x[0], 0)
        with transaction(undo_on_success=True) as t:
            t.add(set_x, args=1)
        self.assertEqual(x[0], 1)

    def test_transaction_named_argument(self):
        """Test if adding an undo action with named arguments works."""
        x = [0]

        def set_x(new_value=None):
            x[0] = new_value
        self.assertEqual(x[0], 0)
        with transaction(undo_on_success=True) as t:
            t.add(set_x, kwargs={'new_value': 1})
        self.assertEqual(x[0], 1)
