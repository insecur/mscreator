/*
 * ===========================================================================
 *
 *       Filename:  testbinary.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/05/2014 10:19:29 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * ===========================================================================
 */
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <mylib2.c>

int main(int argc, char *argv[]) {
	int ret = -1;
	if (argc > 1) {
		ret = atoi(argv[1]);
	}
	do_nothing2();
	int fd = open("/sbin/init", O_RDONLY|O_CLOEXEC);
	close(fd);
	exit(ret);
}
