#!/bin/bash

cd chroot
for symlink in bin/mybinary lib lib64 usr/lib64 usr/lib/ld-linux-x86-64.so.2 usr/lib/libc.so.6 usr/lib/libmylib1.so usr/lib/libmylib2.so usr/local usr/local2/lib ;do
    echo "checking symlink ${symlink}..."
    [ -e $symlink ] || { echo $symlink does not exist;exit 1;}
    [ -L $symlink ] || { echo $symlink is not a symlink;exit 1;}
done
for dir in bin usr usr/lib usr/local2 usr/local2/lib64 ;do 
    echo "checking directory ${dir}..."
    [ -e $dir ] || { echo $dir does not exist;exit 1;}
    [ -d $dir ] || { echo $dir is not a directory;exit 1;}
done
for file in bin/testbinary usr/lib/ld-2.22.so usr/lib/libc-2.22.so usr/local2/lib64/libmylib1.so usr/local2/lib64/libmylib2.so ;do
    echo "checking regular file ${file}..."
    [ -e $file ] || { echo $file does not exist;exit 1;}
    [ -f $file ] || { echo $file is not a regular file;exit 1;}
done
