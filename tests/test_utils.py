# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
from unittest import TestCase, skipIf, skipUnless

import mscreator.mscglobals
from mscreator.exceptions import PathNotFoundError, CriticalError
from mscreator.utils import (
    abridge_string, which, parse_intlist, compress_intlist, sizestring_to_bytes, temporary_file, root_is_initial,
    chroot, symlink, boolean, setarch, getarch, partition_from_base, get_uuid_by_partition_path, bytes_to_sizestring,
    safe_dir_join, Version)
from .tools import TestBase, saferm


class TestAbridge_string(TestCase):

    def test_even_string_even_astring(self):
        test_string = '1234567890abcdef'
        s = abridge_string(test_string, length=10, astring='..')
        self.assertEqual(len(s), 10)

    def test_even_string_odd_astring(self):
        test_string = '1234567890abcdef'
        s = abridge_string(test_string, length=10, astring='...')
        self.assertEqual(len(s), 10)

    def test_odd_string_odd_astring(self):
        test_string = '1234567890abcde'
        s = abridge_string(test_string, length=10, astring='...')
        self.assertEqual(len(s), 10)

    def test_odd_string_even_astring(self):
        test_string = '1234567890abcde'
        s = abridge_string(test_string, length=10, astring='..')
        self.assertEqual(len(s), 10)

    def test_short_enough(self):
        test_string = '12345890'
        s = abridge_string(test_string, length=10, astring='...')
        self.assertEqual(s, test_string)

    def test_just_short_enough(self):
        test_string = '1234567890'
        s = abridge_string(test_string, length=10, astring='...')
        self.assertEqual(s, test_string)

    def test_one_too_big(self):
        test_string = '1234567890a'
        s = abridge_string(test_string, length=10, astring='...')
        self.assertEqual(len(s), 10)

    def test_astring_exact(self):
        test_string = '1234567890'
        s = abridge_string(test_string, length=7, astring='.X.X.')
        self.assertEqual(s, '1.X.X.0')

    def test_astring_too_long(self):
        test_string = '1234567890'
        s = abridge_string(test_string, length=5, astring='.X.X.')
        self.assertEqual(s, test_string)


class TestWhich(TestCase):

    def test_which_init(self):
        test_command = 'init'
        s = which(test_command)
        self.assertIn(s, ['/sbin/init', '/usr/sbin/init'])

    def test_not_existing(self):
        test_command = 'lets_hope_this_command_does_not_exist'
        with self.assertRaisesRegex(PathNotFoundError, test_command):
            s = which(test_command)

    def test_absolute_init(self):
        test_command = '/sbin/init'
        s = which(test_command)
        self.assertEqual(s, test_command)

    def test_absolute_not_existing(self):
        test_command = '/bin/does_not_exist'
        with self.assertRaisesRegex(PathNotFoundError, test_command):
            s = which(test_command)


class TestParse_intlist(TestCase):

    def test_last_and_first_ranges(self):
        list_string = '3-5,10,14-17'
        intlist = parse_intlist(list_string)
        self.assertListEqual(intlist, [3, 4, 5, 10, 14, 15, 16, 17])

    def test_last_and_first_single(self):
        list_string = '3,5-10,12-14,17'
        intlist = parse_intlist(list_string)
        self.assertListEqual(intlist, [3, 5, 6, 7, 8, 9, 10, 12, 13, 14, 17])

    def test_overlapping_ranges(self):
        list_string = '3-5,4-7'
        with self.assertRaisesRegex(ValueError, 'Entries in intlist must be unique!'):
            intlist = parse_intlist(list_string)

    def test_duplicate_items(self):
        list_string = '3,5,7,5'
        with self.assertRaisesRegex(ValueError, 'Entries in intlist must be unique!'):
            intlist = parse_intlist(list_string)

    def test_unsorted_single(self):
        list_string = '3,7,5,6'
        intlist = parse_intlist(list_string)
        self.assertListEqual(intlist, [3, 5, 6, 7])

    def test_unsorted_ranges(self):
        list_string = '5-7,2-4'
        intlist = parse_intlist(list_string)
        self.assertListEqual(intlist, [2, 3, 4, 5, 6, 7])

    def test_garbage_range(self):
        list_string = '3-5,10,20-30-40,14-17'
        intlist = parse_intlist(list_string)
        self.assertListEqual(intlist, [3, 4, 5, 10, 14, 15, 16, 17])


class TestCompress_intlist(TestCase):

    def test_last_and_first_ranges(self):
        intlist = [3, 4, 5, 10, 14, 15, 16, 17]
        list_string = compress_intlist(intlist)
        self.assertEqual(list_string, '3-5,10,14-17')

    def test_last_and_first_single(self):
        intlist = [3, 5, 6, 7, 8, 9, 10, 12, 13, 14, 17]
        list_string = compress_intlist(intlist)
        self.assertEqual(list_string, '3,5-10,12-14,17')

    def test_duplicate_items(self):
        intlist = [3, 4, 5, 4, 5, 6, 7]
        with self.assertRaisesRegex(ValueError, 'Entries in intlist must be unique!'):
            list_string = compress_intlist(intlist)

    def test_unsorted_single(self):
        intlist = [3, 7, 5, 9]
        list_string = compress_intlist(intlist)
        self.assertEqual(list_string, '3,5,7,9')

    def test_unsorted_ranges(self):
        intlist = [2, 6, 3, 7, 4, 8]
        list_string = compress_intlist(intlist)
        self.assertEqual(list_string, '2-4,6-8')

    def test_one_element_list(self):
        intlist = [1]
        list_string = compress_intlist(intlist)
        self.assertEqual(list_string, '1')


class TestSizestring_to_bytes(TestCase):

    def test_number_to_bytes(self):
        s = '123'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 123)

    def test_kb_to_bytes(self):
        s = '1kB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1000)

    def test_mb_to_bytes(self):
        s = '1MB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1000 * 1000)

    def test_gb_to_bytes(self):
        s = '1GB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1000 * 1000 * 1000)

    def test_tb_to_bytes(self):
        s = '1TB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1000 * 1000 * 1000 * 1000)

    def test_kib_to_bytes(self):
        s = '1kiB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1024)

    def test_mib_to_bytes(self):
        s = '1MiB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1024 * 1024)

    def test_gib_to_bytes(self):
        s = '1GiB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1024 * 1024 * 1024)

    def test_tib_to_bytes(self):
        s = '1TiB'
        b = sizestring_to_bytes(s)
        self.assertEqual(b, 1 * 1024 * 1024 * 1024 * 1024)

    def test_unsupported_to_bytes(self):
        with self.assertRaisesRegex(ValueError, 'Invalid size specification "'):
            b = sizestring_to_bytes('1YiB')

    def test_garbage_to_bytes(self):
        with self.assertRaisesRegex(ValueError, 'Invalid size specification "'):
            b = sizestring_to_bytes('garbage')


class TestBytes_to_sizestring(TestCase):

    def test_bad_base(self):
        with self.assertRaisesRegex(ValueError, 'base must be 2 or 10!'):
            bytes_to_sizestring(1, base=3)

    def test_emptysep(self):
        b = 1
        s = bytes_to_sizestring(b, base=10, sep='', precision=0, suffix='B')
        self.assertEqual(s, '1B')

    def test_base2_b(self):
        b = 123
        s = bytes_to_sizestring(b, base=2, precision=0, suffix='B')
        self.assertEqual(s, '123 B')

    def test_base2_kib(self):
        b = 1234
        s = bytes_to_sizestring(b, base=2, precision=0, suffix='B')
        self.assertEqual(s, '1 KiB')

    def test_base10_512kib(self):
        b = 512 * 1024 - 1
        s = bytes_to_sizestring(b, base=2, precision=0, suffix='B')
        self.assertEqual(s, '512 KiB')

    def test_base10_05Mib(self):
        b = 512 * 1024
        s = bytes_to_sizestring(b, base=2, precision=1, suffix='B')
        self.assertEqual(s, '0.5 MiB')

    def test_base2_1tib(self):
        b = 1024 ** 4
        s = bytes_to_sizestring(b, base=2, precision=0, suffix='B')
        self.assertEqual(s, '1 TiB')

    def test_base2_1024tib(self):
        b = 1024 ** 5
        s = bytes_to_sizestring(b, base=2, precision=0, suffix='B')
        self.assertEqual(s, '1024 TiB')

    def test_base10_b(self):
        b = 123
        s = bytes_to_sizestring(b, base=10, precision=0, suffix='B')
        self.assertEqual(s, '123 B')

    def test_base10_kb(self):
        b = 1234
        s = bytes_to_sizestring(b, base=10, precision=0, suffix='B')
        self.assertEqual(s, '1 kB')

    def test_base10_500kb(self):
        b = 500 * 1000 - 1
        s = bytes_to_sizestring(b, base=10, precision=0, suffix='B')
        self.assertEqual(s, '500 kB')

    def test_base10_05mb(self):
        b = 500 * 1000
        s = bytes_to_sizestring(b, base=10, precision=1, suffix='B')
        self.assertEqual(s, '0.5 MB')

    def test_base10_1tb(self):
        b = 1000 ** 4
        s = bytes_to_sizestring(b, base=10, precision=0, suffix='B')
        self.assertEqual(s, '1 TB')

    def test_base10_1000tb(self):
        b = 1000 ** 5
        s = bytes_to_sizestring(b, base=10, precision=0, suffix='B')
        self.assertEqual(s, '1000 TB')


class TestTemporaryFile(TestBase):

    def test_tempfile_check_dirname(self):
        with temporary_file() as fd:
            filename = fd.filename
            # self.assertTrue(filename.startswith('/tmp/'))
            self.assertRegex(filename, '^%s' % mscreator.mscglobals.CONFIG.main.temporary_directory)

    def test_tempfile_check_prefix(self):
        with temporary_file('prefix-') as fd:
            filename = fd.filename
            self.assertTrue(os.path.basename(filename).startswith('prefix-'))

    def test_tempfile_removed(self):
        with temporary_file() as fd:
            filename = fd.filename
            self.assertTrue(os.path.exists(filename))
        self.assertFalse(os.path.exists(filename))

    def test_tempfile_not_removed(self):
        with temporary_file(keep=True) as fd:
            filename = fd.filename
            self.assertTrue(os.path.exists(filename))
            self.addCleanup(saferm, filename)
        self.assertTrue(os.path.exists(filename))

    def test_tempfile_already_deleted(self):
        with temporary_file() as fd:
            filename = fd.filename
            saferm(filename)
        self.assertFalse(os.path.exists(filename))

    def test_tempfile_already_closed(self):
        with temporary_file() as fd:
            filename = fd.filename
            fd.close()
        self.assertFalse(os.path.exists(filename))


class TestRootIsInitial(TestCase):

    @classmethod
    def setUpClass(cls):
        import mscreator.mscglobals
        mscreator.mscglobals.INITIAL_ROOT = os.readlink('/proc/%s/root' % os.getpid())

    def test_isinitial(self):
        self.assertTrue(root_is_initial())

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_ischrooted(self):
        with chroot('/tmp'):
            self.assertFalse(root_is_initial())
        self.assertTrue(root_is_initial())


@skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
class TestChroot(TestCase):

    def test_chroot(self):
        root_files = set(os.listdir('/'))
        with chroot('/tmp'):
            chroot_files = set(os.listdir('/'))
            self.assertTrue(len(root_files - chroot_files) != 0)

    def test_chroot_to_root(self):
        root_files = set(os.listdir('/'))
        with chroot('/') as new_root:
            chroot_files = set(os.listdir('/'))
            self.assertIsNone(new_root.old_root)
            self.assertTrue(len(root_files - chroot_files) == 0)

    def test_chroot_to_root_forced(self):
        root_files = set(os.listdir('/'))
        with chroot('/', chroot_to_root=True) as new_root:
            chroot_files = set(os.listdir('/'))
            self.assertIsNotNone(new_root.old_root)
            self.assertTrue(len(root_files - chroot_files) == 0)

    def test_chroot_no_setarch(self):
        old_arch = os.uname()[4]
        with chroot('/tmp'):
            self.assertEqual(os.uname()[4], old_arch)
        self.assertEqual(os.uname()[4], old_arch)

    @skipIf(os.uname()[4] != 'x86_64', 'Only check this on 64 bit architecture.')
    def test_chroot_setarch(self):
        self.assertEqual(os.uname()[4], 'x86_64')
        with chroot('/tmp', arch='x86'):
            self.assertEqual(os.uname()[4], 'i686')
        self.assertEqual(os.uname()[4], 'x86_64')


class TestSymlink(TestBase):

    def setUp(self):
        super().setUp()
        self.symlink_path = os.path.join(self.tempdir, 'symlink')
        if os.path.lexists(self.symlink_path):
            os.unlink(self.symlink_path)

    def test_create_symlink(self):
        symlink('something the link points to', self.symlink_path)
        self.assertTrue(os.path.islink(self.symlink_path))

    def test_force_create_symlink(self):
        symlink('something the link points to', self.symlink_path)
        self.assertTrue(os.path.islink(self.symlink_path))

        symlink('another target', self.symlink_path, force=True)
        self.assertTrue(os.path.islink(self.symlink_path))
        self.assertEqual(os.readlink(self.symlink_path), 'another target')

    def test_create_existing_symlink(self):
        symlink('something the link points to', self.symlink_path)
        self.assertTrue(os.path.islink(self.symlink_path))

        with self.assertRaisesRegex(OSError, 'File exists'):
            symlink('another target', self.symlink_path)


class TestBoolean(TestCase):

    def test_true(self):
        self.assertTrue(boolean('true'))
        self.assertTrue(boolean('TRUE'))
        self.assertTrue(boolean('True'))
        self.assertTrue(boolean('oN'))
        self.assertTrue(boolean('1'))
        self.assertTrue(boolean('yes'))
        self.assertTrue(boolean('enabled'))
        self.assertTrue(boolean(True))

    def test_false(self):
        self.assertFalse(boolean('false'))
        self.assertFalse(boolean('true '))
        self.assertFalse(boolean(' true'))
        self.assertFalse(boolean(' 1'))
        self.assertFalse(boolean('1 '))
        self.assertFalse(boolean('1-1'))
        self.assertFalse(boolean('2'))
        self.assertFalse(boolean(False))

    def test_weird_string(self):
        self.assertFalse(boolean('something completely different'))

    def test_not_string(self):
        with self.assertRaisesRegex(ValueError, 'Invalid boolean value "'):
            self.assertFalse(boolean(2))


class TestSetArch(TestCase):

    @skipUnless(os.uname()[4] == 'x86_64', 'setarch is only useful for 64 bit systems')
    def test_set32(self):
        self.assertEqual(os.uname()[4], 'x86_64')
        self.assertEqual(getarch(), 'amd64')

        setarch('x86')
        self.assertEqual(os.uname()[4], 'i686')
        self.assertEqual(getarch(), 'x86')

        setarch('amd64')
        self.assertEqual(os.uname()[4], 'x86_64')
        self.assertEqual(getarch(), 'amd64')

    def test_setinvalid(self):
        with self.assertRaisesRegex(ValueError, 'arch must be one of "'):
            setarch('aoeusnth')


class TestPartitions(TestCase):

    def test_partition_by_base_hd(self):
        p = partition_from_base('/dev/sda', 1)
        self.assertEqual(p, '/dev/sda1')

    def test_partition_by_base_loop(self):
        p = partition_from_base('/dev/loop0', 1)
        self.assertEqual(p, '/dev/loop0p1')

    def test_get_uuid_non_existant(self):
        with self.assertRaisesRegex(PathNotFoundError, 'Could not determine UUID for "'):
            get_uuid_by_partition_path('/dev/hopefully_not_existant')

    @skipUnless(os.path.exists('/dev/sda1'), '/dev/sda1 not found, currently required by test')
    def test_get_uuid(self):
        uuid = get_uuid_by_partition_path('/dev/sda1')
        self.assertRegex(uuid, '^%(hc)s{8}-%(hc)s{4}-%(version)s%(hc)s{3}-%(variant)s%(hc)s{3}-%(hc)s{12}$' % {'hc': '[0-9a-zA-Z]',
                                                                                                               'version': '[12345]',
                                                                                                               'variant': '[89abAB]'})


class TestSafe_dir_join(TestCase):

    def test_valid_simple(self):
        result = safe_dir_join('/path/to/tmp', 'subdirectory')
        self.assertEqual(result, '/path/to/tmp/subdirectory/')

    def test_valid_only_relatives(self):
        result = safe_dir_join('path/to/tmp', 'subdirectory')
        self.assertEqual(result, 'path/to/tmp/subdirectory/')

    def test_single_absolute_item(self):
        with self.assertRaisesRegex(CriticalError,
                                    'Joining .* would result in path "/subdirectory/" which might be used for deleting files!'):
            safe_dir_join('/path/to/tmp', '/subdirectory')

    def test_doubleslash_item(self):
        with self.assertRaisesRegex(CriticalError,
                                    'Joining .* would result in path "//subdirectory/" which might be used for deleting files!'):
            safe_dir_join('/path/to/tmp', '//subdirectory')

    def test_only_one_absolute_path(self):
        result = safe_dir_join('/path/to/tmp')
        self.assertEqual(result, '/path/to/tmp/')

    def test_only_one_relative_path(self):
        result = safe_dir_join('path/to/tmp')
        self.assertEqual(result, 'path/to/tmp/')

    def test_empty_path_1(self):
        result = safe_dir_join('', 'path/to/tmp')
        self.assertEqual(result, 'path/to/tmp/')

    def test_empty_path_2(self):
        result = safe_dir_join('/path/to/tmp', '')
        self.assertEqual(result, '/path/to/tmp/')


class TestVersion(TestCase):
    def test_simple(self):
        version = Version("1.2.3")
        self.assertEqual(str(version), "1.2.3")

    def test_largeversion(self):
        version = Version("123.456.789")
        self.assertEqual(str(version), "123.456.789")

    def test_bad_version(self):
        with self.assertRaisesRegex(ValueError, ' is not a valid version string'):
            Version("1.2")
        with self.assertRaisesRegex(ValueError, ' is not a valid version string'):
            Version("abc")
        with self.assertRaisesRegex(ValueError, ' is not a valid version string'):
            Version(None)

    def test_compare_simple(self):
        version1 = Version("1.2.3")
        version2 = Version("2.2.3")
        self.assertTrue(version1 < version2)
        self.assertFalse(version1 > version2)
        self.assertFalse(version2 < version1)
        self.assertTrue(version2 > version1)
        self.assertTrue(version1 != version2)

    def test_compare_min_patch_zero(self):
        version1 = Version("1.2.3")
        version2 = Version("2.0.0")
        self.assertTrue(version1 < version2)
        self.assertFalse(version1 > version2)
        self.assertFalse(version2 < version1)
        self.assertTrue(version2 > version1)

    def test_compare_equal(self):
        version1 = Version("1.2.3")
        version2 = Version("1.2.3")
        self.assertTrue(version1 == version2)
