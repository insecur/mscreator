# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os

import mscreator
from mscreator.config import ConfigError
from mscreator.config_handlers.bootloader_grub import Grub1SectionHandler
from mscreator.exceptions import PathNotFoundError
import mscreator.msclogging
from mscreator import MscConfig
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from tests.tools import TestBase


logger = logging.getLogger('mscreator')


class TestGrub(TestBase):

    def _set_paths(self, target_directory):
        self.kernel_version1 = '1.2.3'
        self.bzimage1 = 'bzImage-%s' % self.kernel_version1
        self.initrd1 = 'initramfs-%s' % self.kernel_version1
        self.image_path1 = os.path.join(self.test_chroot_path, 'boot', 'bzImage-%(kernelversion)s')
        self.image_path_expanded1 = os.path.join(self.test_chroot_path, 'boot', self.bzimage1)
        self.initrd_path1 = os.path.join(self.test_chroot_path, 'boot', self.initrd1)
        self.modules_path1 = os.path.join(self.test_chroot_path, 'lib', 'modules', self.kernel_version1)
        self.target_boot_path1 = os.path.join(target_directory, 'boot')
        self.target_modules_path1 = os.path.join(target_directory, 'lib', 'modules', self.kernel_version1)

        self.kernel_version2 = '2.3.4'
        self.bzimage2 = 'bzImage-%s' % self.kernel_version2
        self.initrd2 = 'initramfs-%s' % self.kernel_version2
        self.image_path2 = os.path.join(self.test_chroot_path, 'boot', 'bzImage-%(kernelversion)s')
        self.image_path_expanded2 = os.path.join(self.test_chroot_path, 'boot', self.bzimage2)
        self.initrd_path2 = os.path.join(self.test_chroot_path, 'boot', self.initrd2)
        self.modules_path2 = os.path.join(self.test_chroot_path, 'lib', 'modules', self.kernel_version2)
        self.target_boot_path2 = os.path.join(target_directory, 'boot')
        self.target_modules_path2 = os.path.join(target_directory, 'lib', 'modules', self.kernel_version2)

        self.stage1_path = os.path.join(self.test_chroot_path, 'boot', 'grub', 'stage1')
        self.stage2_path = os.path.join(self.test_chroot_path, 'boot', 'grub', 'stage2')
        self.grub_path = os.path.join(self.test_chroot_path, 'bin', 'grub')

        self.target_menulst_path = os.path.join(target_directory, 'boot', 'grub', 'menu.lst')
        self.target_stage1_path = os.path.join(target_directory, 'boot', 'grub', 'stage1')
        self.target_stage2_path = os.path.join(target_directory, 'boot', 'grub', 'stage2')

    def test_minimal(self):
        """Check that grub files are correctly copied and minimal menu.lst is correctly written."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::Kernel::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        cmdline=quiet root=/dev/sda1
        modules_path=%s

        ::Grub::grub1::/
        stage1=%s
        stage2=%s
        grub=%s
        ''' % (target_directory,
               self.kernel_version1, self.image_path1, self.initrd_path1, self.modules_path1,
               self.stage1_path, self.stage2_path, self.grub_path
               )

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 4)
        grub_section = config.sections['Grub']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(grub_section, Grub1SectionHandler)

        grub_section.write_config(target_directory)
        self.assertTrue(os.path.exists(self.target_menulst_path))
        with open(self.target_menulst_path) as fd:
            lines = fd.readlines()
            self.assertListEqual(lines, ['default=0\n'])

        grub_section.deploy_files(target_directory)
        self.assertTrue(os.path.exists(self.target_stage1_path))
        self.assertTrue(os.path.exists(self.target_stage2_path))

    def test_invalid_grub_path(self):
        """Check that exception is raised during parsing if grub binary is not found."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=

        ::Kernel::kernel::
        kernelversion=123
        image=image
        initrd=initrd
        cmdline=
        modules_path=/lib/modules

        ::Grub::grub1::/
        stage1=%s
        stage2=%s
        grub=/does/not/exist
        timeout=0
        boot_entry=Kernel
        ''' % (self.stage1_path, self.stage2_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(PathNotFoundError, 'Grub binary .* not found!'):
            config.parse()

    def test_one_boot_entry(self):
        """Check that 1 boot entry is correctly written to menu.lst"""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::Kernel::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        cmdline=quiet root=/dev/sda1
        modules_path=%s

        ::Grub::grub1::/
        stage1=%s
        stage2=%s
        grub=%s
        timeout=0
        boot_entry=Kernel
        ''' % (target_directory,
               self.kernel_version1, self.image_path1, self.initrd_path1, self.modules_path1,
               self.stage1_path, self.stage2_path, self.grub_path
               )

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        grub_section = config.sections['Grub']

        grub_section.write_config(target_directory)
        self.assertTrue(os.path.exists(self.target_menulst_path))
        with open(self.target_menulst_path) as fd:
            lines = fd.readlines()
            self.assertListEqual(lines, [
                'default=0\n',
                'timeout=0\n',
                '\n',
                'title Kernel\n',
                'root (hd0,0)\n',
                'kernel (hd0,0)/boot/%s quiet root=/dev/sda1\n' % self.bzimage1,
                'initrd /boot/%s\n' % self.initrd1
            ])

    def test_several_boot_entries(self):
        """Check that more than 1 boot entries are correctly written to menu.lst and the first is the default item."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::Kernel1::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        cmdline=quiet root=/dev/sda1
        modules_path=%s

        ::Kernel2::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        modules_path=%s

        ::Grub::grub1::/
        stage1=%s
        stage2=%s
        grub=%s
        timeout=0
        boot_entry=Kernel2
        boot_entry=Kernel1
        ''' % (target_directory,
               self.kernel_version1, self.image_path1, self.initrd_path1, self.modules_path1,
               self.kernel_version2, self.image_path2, self.initrd_path2, self.modules_path2,
               self.stage1_path, self.stage2_path, self.grub_path
               )

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        grub_section = config.sections['Grub']

        grub_section.write_config(target_directory)
        self.assertTrue(os.path.exists(self.target_menulst_path))
        with open(self.target_menulst_path) as fd:
            lines = fd.readlines()
            self.assertListEqual(lines, [
                'default=0\n',
                'timeout=0\n',
                '\n',
                'title Kernel2\n',
                'root (hd0,0)\n',
                'kernel (hd0,0)/boot/%s\n' % self.bzimage2,
                'initrd /boot/%s\n' % self.initrd2,
                '\n',
                'title Kernel1\n',
                'root (hd0,0)\n',
                'kernel (hd0,0)/boot/%s quiet root=/dev/sda1\n' % self.bzimage1,
                'initrd /boot/%s\n' % self.initrd1,
            ])

    def test_boot_entry_not_kernel_section(self):
        """Check that an error is raised if a boot_entry is not of type kernel."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::No Kernel::static_files::/

        ::Grub::grub1::/
        stage1=%s
        stage2=%s
        grub=%s
        boot_entry=No Kernel
        ''' % (target_directory, self.stage1_path, self.stage2_path, self.grub_path
               )

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'A boot entry must reference a kernel section'):
            config.parse()
