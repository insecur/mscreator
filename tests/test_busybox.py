# -*- coding: utf-8 -*-
# Copyright (c) 2016-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os
import stat

import mscreator
from mscreator import MscConfig
from mscreator.config_handlers.filesource_busybox import BusyBoxSectionHandler
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


class TestChrootSection(TestBase):
    def test_basic(self):
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%(target_dir)s
        filesources=busybox

        ::busybox::busybox::
        bb_version=1.24.2
        ''' % {'target_dir': target_directory}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        files_section = config.sections['busybox']
        # we kind of fake compiling as we do not want to go through the whole build process of busybox in our tests,
        # it would take far too long, anyway, it makes it possible to test if the deploy_files() method works when
        # called down below in build_system()
        os.makedirs(files_section.build_path, exist_ok=True)
        with open(files_section.binary_path, 'a'):
            pass
        files_section._dependencies_are_calculated = True

        chroot_section = config.sections['Chroot']
        chroot_section.build_system()

        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['busybox'], BusyBoxSectionHandler)

        binary_target_path = os.path.join(target_directory, 'bin', 'busybox')
        self.assertTrue(os.path.exists(binary_target_path))
        st_mode = os.stat(binary_target_path).st_mode
        self.assertNotEqual(st_mode & stat.S_IXUSR, 0)
        self.assertNotEqual(st_mode & stat.S_IXGRP, 0)
        self.assertNotEqual(st_mode & stat.S_IXOTH, 0)
