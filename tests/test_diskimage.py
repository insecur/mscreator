# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
from unittest import skipIf, SkipTest
from unittest.mock import patch
import logging

import mscreator
from mscreator import MscConfig
from mscreator.commands import touch
from mscreator.config_handlers.target_diskimage import DiskImageSectionHandler
from mscreator.exceptions import PathExistsError, PathNotFoundError
from mscreator.utils import which
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


@skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
class TestDiskImageSection(TestBase):

    def test_basic(self):
        try:
            which('qemu-img')
        except PathNotFoundError:
            raise SkipTest('qemu-img needed to create/convert disk image')
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'image.vdi')
        c = '''
        name=testlinux
        version=1
        targets=vbox image

        ::vbox image::diskimage::
        target_path=%(image_path)s
        image_type=vdi
        disk_layout=vdi layout
        filesources=static files

        ::vdi layout::disklayout::
        disksize=2 MiB
        partition=83:remaining:ext2:/:

        ::static files::static_files::/
        /etc/fstab
        ''' % {'image_path': image_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        files_section = config.sections['static files']
        image_section = config.sections['vbox image']
        self.assertIsInstance(image_section, DiskImageSectionHandler)
        self.assertEqual(len(config.sections), 4)
        self.assertEqual(len(config.main.targets), 1)

        files_section.calculate_file_dependencies()
        image_section.build_system()

        self.assertTrue(os.path.exists(image_path))
        expected_size = 2 * 1024 * 1024
        max_delta = expected_size // 100 * 5  # accept ~ 5% difference in size
        self.assertAlmostEqual(os.path.getsize(image_path), expected_size, delta=max_delta)

    def test_overwrite_existing(self):
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'image.raw')
        c = '''
        name=testlinux
        version=1
        targets=raw image

        ::raw image::diskimage::
        target_path=%(image_path)s
        image_type=raw
        disk_layout=raw layout

        ::raw layout::disklayout::
        gap=1
        disksize=500000
        partition=83:remaining:ext2:/:
        ''' % {'image_path': image_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        if not os.path.exists(image_path):
            touch(image_path)
        self.assertTrue(os.path.exists(image_path))
        with self.assertRaisesRegex(PathExistsError, 'already exists, use -O'):
            config.parse()

        old_overwrite = mscreator.mscglobals.OVERWRITE_TARGETS
        mscreator.mscglobals.OVERWRITE_TARGETS = True
        config.parse()
        image_section = config.sections['raw image']
        image_section.build_system()
        mscreator.mscglobals.OVERWRITE_TARGETS = old_overwrite

        with open(image_path, 'rb') as fd:
            bootcode = fd.read(440)
            self.assertEqual(bootcode, b'\x00' * 440)

    def test_with_bootloader_and_kernel(self):
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'image.raw')
        c = '''
        name=testlinux
        version=1
        targets=raw image

        ::raw image::diskimage::
        target_path=%(image_path)s
        image_type=raw
        disk_layout=raw layout
        bootloader=grub

        ::raw layout::disklayout::
        gap=1
        disksize=500 kB
        partition=83:remaining:ext2:/:

        ::grub::grub1::/
        stage1=%(testchroot)s/boot/grub/stage1
        stage2=%(testchroot)s/boot/grub/stage2
        grub=%(testchroot)s/bin/grub
        ''' % {'image_path': image_path,
               'testchroot': self.test_chroot_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        image_section = config.sections['raw image']
        self.assertIsInstance(image_section, DiskImageSectionHandler)

        image_section.build_system()

        self.assertTrue(os.path.exists(image_path))
        expected_size = 500000
        max_delta = expected_size // 100 * 5  # accept ~ 5% difference in size
        self.assertAlmostEqual(os.path.getsize(image_path), expected_size, delta=max_delta)

        with open(image_path, 'rb') as fd:
            bootcode = fd.read(440)
            self.assertNotEqual(bootcode, b'\x00' * 440)

    def test_with_bootloader_and_kernel(self):
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'image.raw')
        c = '''
        name=testlinux
        version=1
        targets=raw image

        ::raw image::diskimage::
        target_path=%(image_path)s
        image_type=raw
        disk_layout=raw layout
        bootloader=grub

        ::raw layout::disklayout::
        gap=1
        disksize=500 kB
        partition=83:remaining:ext2:/:

        ::grub::grub1::/
        stage1=%(testchroot)s/boot/grub/stage1
        stage2=%(testchroot)s/boot/grub/stage2
        grub=%(testchroot)s/bin/grub
        boot_entry=kernel

        ::kernel::kernel::
        kernelversion=1.2.3
        image=unimportant
        ''' % {'image_path': image_path,
               'testchroot': self.test_chroot_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        image_section = config.sections['raw image']
        kernel_section = config.sections['kernel']
        self.assertIsInstance(image_section, DiskImageSectionHandler)

        # noinspection PyUnresolvedReferences
        with patch.object(kernel_section, 'deploy_files') as kernel_deploy_files_mock:
            image_section.build_system()
            kernel_deploy_files_mock.assert_called_with(image_section.livedir)
