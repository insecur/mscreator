# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging

from mscreator.commands import losetup
from mscreator.config import ConfigFile, ConfigError
from mscreator.config_handlers.disklayout import pad_to_sectorsize, DiskLayoutSectionHandler
from mscreator.mbr import Mbr
from mscreator.mount import Mount
from mscreator.transactions import transaction
from mscreator.utils import temporary_file
from tests.tools import TestBase


logger = logging.getLogger('mscreator')


class TestPadSectorToSize(TestBase):

    def test_smaller_than_sectorsize(self):
        result = pad_to_sectorsize(123, 4096)
        self.assertEqual(result, 4096)

    def test_equals_sectorsize(self):
        result = pad_to_sectorsize(4096, 4096)
        self.assertEqual(result, 4096)

    def test_bigger_than_sectorsize(self):
        result = pad_to_sectorsize(4096 + 1, 4096)
        self.assertEqual(result, 2 * 4096)

    def test_multiple_sectorsize(self):
        result = pad_to_sectorsize(4 * 4096, 4096)
        self.assertEqual(result, 4 * 4096)


class TestDisklayoutSection(TestBase):

    def test_simple(self):
        # default gap is currently 2048 sectors * 512 bytes = 1048576 bytes
        lines = [
            '::dl::disklayout::',
            'disksize=12 MiB',
            'partition=83:1 MiB:ext2:/boot:ro',
            'partition=83:10 MiB:ext4:/:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.gap, 2048)
        self.assertEqual(layout.sectorsize, 512)
        self.assertEqual(layout.disksize, 1024 ** 2 * 12)

        self.assertEqual(len(layout.partitions), 2)
        p = layout.partitions[0]
        self.assertEqual(p.size, 1024 ** 2 * 1)
        p = layout.partitions[1]
        self.assertEqual(p.size, 1024 ** 2 * 10)

    def test_with_remaining(self):
        lines = [
            '::dl::disklayout::',
            'disksize=12 MiB',
            'partition=83:1 MiB:ext2:/boot:ro',
            'partition=83:5 MiB:ext4:/:',
            'partition=83:remaining:ext3:/home:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.gap, 2048)
        self.assertEqual(layout.sectorsize, 512)
        self.assertEqual(layout.disksize, 1024 ** 2 * 12)

        self.assertEqual(len(layout.partitions), 3)
        p = layout.partitions[0]
        self.assertEqual(p.size, 1024 ** 2 * 1)
        p = layout.partitions[1]
        self.assertEqual(p.size, 1024 ** 2 * 5)
        p = layout.partitions[2]
        self.assertEqual(p.size, 1024 ** 2 * 5)

    def test_partitions_too_big(self):
        lines = [
            '::dl::disklayout::',
            'disksize=12 MiB',
            'partition=83:10 MiB:ext2:/boot:ro',
            'partition=83:10 MiB:ext4:/:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        with self.assertRaisesRegex(ConfigError, 'Configured partitions need more space than disk size'):
            layout.post_load_init()

    def test_no_space_for_remaining(self):
        lines = [
            '::dl::disklayout::',
            'disksize=12 MiB',
            'partition=83:1 MiB:ext2:/boot:ro',
            'partition=83:10 MiB:ext4:/:',
            'partition=83:remaining:ext3:/home:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        with self.assertRaisesRegex(ConfigError, 'Size for "remaining" partition would be 0'):
            layout.post_load_init()

    def test_invalid_size_string(self):
        lines = [
            '::dl::disklayout::',
            'disksize=12 Mb',
            'partition=83:1 MiB:ext4:/:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        with self.assertRaisesRegex(ValueError, 'Invalid size specification "12 Mb"!'):
            layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)

    def test_percentage(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:10%:ext2:/boot:ro',
            'partition=83:50%:ext4:/:',
            'partition=83:remaining:ext3:/home:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.gap, 2048)
        self.assertEqual(layout.sectorsize, 512)
        self.assertEqual(layout.disksize, 1024 ** 2 * 11)

        self.assertEqual(len(layout.partitions), 3)
        p = layout.partitions[0]
        self.assertEqual(p.size, 1024 ** 2 * 1)
        p = layout.partitions[1]
        self.assertEqual(p.size, 1024 ** 2 * 5)
        p = layout.partitions[2]
        self.assertEqual(p.size, 1024 ** 2 * 4)

    def test_root_boot_index(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:10%:ext2:/boot:ro',
            'partition=83:50%:ext4:/:',
            'partition=83:remaining:ext3:/home:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.root_index, 2)
        self.assertEqual(layout.boot_index, 1)

    def test_root_boot_index_no_separate_boot(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:50%:ext4:/:',
            'partition=83:remaining:ext3:/home:'
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.root_index, 1)
        self.assertEqual(layout.boot_index, 1)

    def test_create_mount_tree(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:1 MiB:ext3:/home:',
            'partition=83:1 MiB:ext2:/boot:ro',
            'partition=83:1 MiB:ext3:/home/nörd:',
            'partition=83:1 MiB:ext4:/:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()
        self.assertEqual(layout.gap, 2048)
        self.assertEqual(layout.sectorsize, 512)
        self.assertEqual(layout.disksize, 1024 ** 2 * 11)

        self.assertEqual(len(layout.partitions), 4)

        root = layout.get_mount_tree('/dev/my_device')
        self.assertIsInstance(root, Mount)
        root_kids = root.children
        # the following uses IDs created in get_mount_tree(), this makes the test simpler but may be fragile
        home = None
        boot = None
        for child in root_kids.values():
            self.assertIn(child.mount_point, ('/home', '/boot'))
            if child.mount_point == '/home':
                home = child
            elif child.mount_point == '/boot':
                boot = child
        home_noerd = list(home.children.values())[0]
        self.assertIsInstance(home, Mount)
        self.assertIsInstance(home_noerd, Mount)
        self.assertIsInstance(boot, Mount)

    def test_create_mount_tree_no_root_given(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:1 MiB:ext3:/home:',
            'partition=83:1 MiB:ext2:/boot:ro',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        with self.assertRaisesRegex(ConfigError, 'No root partition given'):
            layout.post_load_init()

    def test_too_many_partitions(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'partition=83:1 MiB:ext3:/home:',
            'partition=83:1 MB:ext2:/boot:ro',
            'partition=83:10%:ext3:/home/nörd:',
            'partition=83:1 MB:ext4:/tmp:',
            'partition=83:remaining:ext4:/:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        with self.assertRaisesRegex(ConfigError, 'disk can have a maximum of 4 partitions'):
            layout.post_load_init()

    def test_ignore_unknown_key(self):
        """Check that unknown keys do not raise an exception."""
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'unknown_key=123',
            'partition=83:1 MiB:ext3:/home:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)

    def test_invalid_sectorsize(self):
        lines = [
            '::dl::disklayout::',
            'disksize=11 MiB',
            'sectorsize=4096',
            'partition=83:1 MiB:ext3:/home:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        with self.assertRaisesRegex(ConfigError, 'sector size must be 512'):
            layout.post_load_init()

    def test_apply_part(self):
        lines = [
            '::dl::disklayout::',
            'disksize=5 MiB',
            'gap=1',
            'partition=83:1 MiB:ext3:/home:',
            'partition=83:1 MB:ext2:/boot:ro',
            'partition=83:10%:ext3:/home/nörd:',
            'partition=83:remaining:ext4:/:',
        ]
        config = ConfigFile()
        config.from_strings(lines)
        layout = DiskLayoutSectionHandler('disklayout', '', config, 0, len(lines) - 1)
        layout.post_load_init()

        with temporary_file() as fd:
            with transaction() as t:
                fd.write('\0' * 5 * 1024 * 1024)
                fd.flush()
                loop_path = losetup.attach(fd.filename)
                t.add(losetup.detach, args=(loop_path,), undo_on_success=True)
                layout.apply(loop_path)
            m = Mbr(path=fd.filename)
            part1 = m.partitions[1]
            self.assertEqual(part1.size_in_bytes, 1024 * 1024)
            self.assertEqual(part1.partition_type, 0x83)
            self.assertEqual(part1.start_sector, 1)
            part2 = m.partitions[2]
            self.assertEqual(part2.size_in_bytes, 1000448)  # padded to sector size
            self.assertEqual(part2.partition_type, 0x83)
            part3 = m.partitions[3]
            self.assertEqual(part3.size_in_bytes, (5 * 1024 * 1024) / 10)
            self.assertEqual(part3.partition_type, 0x83)
            part4 = m.partitions[4]
            self.assertEqual(part4.size_in_bytes,
                             (5 * 1024 * 1024) - 512 - (part1.size_in_bytes + part2.size_in_bytes + part3.size_in_bytes))
            self.assertEqual(part4.partition_type, 0x83)
