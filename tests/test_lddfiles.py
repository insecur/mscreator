# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os
from unittest import skipIf

import mscreator
from mscreator import MscConfig
from mscreator.config_handlers.filesource_lddfiles import LddFilesSectionHandler, get_all_dependencies_for_files, \
    get_intermediaries
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from mscreator.utils import chroot
from tests.tools import TestBase


logger = logging.getLogger('mscreator')


def _mock_get_direct_dependencies_for_file(path):
    ldd_deps = {
        '/bin/bitbinary64': {
            '/lib64/libc.so.6',
            '/lib64/ld-linux-x86-64.so.2'
        },
        '/bin/bitbinary32': {
            '/lib/libc.so.6',
            '/lib/ld-linux.so.2'
        },
        '/lib/libc.so.6': {
            '/lib/ld-linux.so.2'
        },
        '/usr/lib/libc.so.6': {
            '/lib/ld-linux.so.2'
        },
        '/lib64/libc.so.6': {
            '/lib64/ld-linux-x86-64.so.2'
        },
        '/usr/lib64/libc.so.6': {
            '/lib64/ld-linux-x86-64.so.2'
        }
    }
    return ldd_deps[path]  # do not use get so we notice calls with unknown path


def _mock_os_readlink(path):
    links = {
        '/lib/libc.so.6': 'libc-2.22.so',
        '/lib': 'usr/lib',
        '/lib64': 'usr/lib64',
    }
    return links[path]


class TestGetIntermediaries(TestBase):

    def test_simple(self):
        paths = get_intermediaries('/usr/lib/python2.7/site-packages/magic.py')
        self.assertListEqual(paths, [
            '/usr',
            '/usr/lib',
            '/usr/lib/python2.7',
            '/usr/lib/python2.7/site-packages'
        ])


class TestGetAllDependencies(TestBase):

    def test_really_complex_stuff(self):
        """Checks really weird stuff, symlinks to symlinks to files, symlinks to symlinks that symlink to a relative path and much more."""
        desired_deps = {
            '/bin',
            '/bin/mybinary',
            '/bin/testbinary',
            '/lib',
            '/lib64',
            '/usr',
            '/usr/lib',
            '/usr/lib64',
            '/usr/lib/ld-2.22.so',
            '/usr/lib/ld-linux-x86-64.so.2',
            '/usr/lib/libc-2.22.so',
            '/usr/lib/libc.so.6',
            '/usr/lib/libmylib1.so',
            '/usr/lib/libmylib2.so',
            '/usr/local',
            '/usr/local2',
            '/usr/local2/lib',
            '/usr/local2/lib64',
            '/usr/local2/lib64/libmylib1.so',
            '/usr/local2/lib64/libmylib2.so',
        }

        with chroot(self.test_chroot_path):
            deps = get_all_dependencies_for_files({'/bin/mybinary'})
        self.assertSetEqual(deps, desired_deps)


class TestLddFiles(TestBase):

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_lddfiles_globbing(self):
        """Check that globbing pattern's work in an ldd_files section."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=ldd files

        ::ldd files::ldd_files::%s
        /lib*/libmylib?.so
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        ldd_section = config.sections['ldd files']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(ldd_section, LddFilesSectionHandler)

        ldd_section.calculate_file_dependencies()

        for path in ('/usr/local2/lib64/libmylib1.so', '/usr/local2/lib64/libmylib2.so'):
            self.assertIn(path, ldd_section.calculated_dependencies)

    def test_non_elf_file(self):
        """Check that non-elf file in ldd section returns the file but does not fail."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=ldd files

        ::ldd files::ldd_files::%s
        /bin/do_nothing.sh
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        ldd_section = config.sections['ldd files']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(ldd_section, LddFilesSectionHandler)

        ldd_section.calculate_file_dependencies()

        self.assertSetEqual(ldd_section.calculated_dependencies, {'/bin', '/bin/do_nothing.sh'})

    def test_correct_loader_32bit(self):
        """Check that the correct loader is determined for 32 bit binaries."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=ldd files

        ::ldd files::ldd_files::%s
        /bin/bitbinary32
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        ldd_section = config.sections['ldd files']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(ldd_section, LddFilesSectionHandler)

        ldd_section.calculate_file_dependencies()

        libdir = '/usr/lib32'
        libc_link = '%s/libc.so.6' % libdir
        with chroot(self.test_chroot_path):
            libc_link_target = '%s/%s' % (libdir, os.readlink(libc_link))

        for path in (libc_link, libc_link_target, '/bin/bitbinary32'):
            self.assertIn(path, ldd_section.calculated_dependencies)

    def test_parameter_is_in_symlinked_dir(self):
        """If one of the given files in ldd_files section is in a dir that is a symlink, check that dependencies are calculated correctly."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=ldd files

        ::ldd files::ldd_files::%s
        /usr/local/bin/bitbinary64
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        ldd_section = config.sections['ldd files']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(ldd_section, LddFilesSectionHandler)

        ldd_section.calculate_file_dependencies()

        libdir = '/usr/lib'
        libc_link = '%s/libc.so.6' % libdir
        with chroot(self.test_chroot_path):
            libc_link_target = '%s/%s' % (libdir, os.readlink(libc_link))

        for path in (libc_link, libc_link_target, '/usr/local2/bin', '/usr/bin', '/bin', '/bin/bitbinary64'):
            self.assertIn(path, ldd_section.calculated_dependencies)
