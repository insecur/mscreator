# -*- coding: utf-8 -*-
# Copyright (c) 2015-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
from unittest import skipIf

import mscreator
from mscreator import MscConfig
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from mscreator.config_handlers.filesource_staticfiles import StaticFilesSectionHandler, StaticDirSectionHandler

from tests.tools import TestBase


class TestStaticFiles(TestBase):

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_staticfiles(self):
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=static files

        ::static files::static_files::/
        /etc/fstab
        ''' % target_directory

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.build_targets()

        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static files'], StaticFilesSectionHandler)
        self.assertTrue(os.path.exists(os.path.join(target_directory, 'etc/fstab')))

    def test_root_path_in_section_data(self):
        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=/not/existing
        filesources=static files

        ::static files::static_files::/
        /etc/fstab
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(config.sections['static files'].chroot_path, '/')

    def test_empty_path_in_section_data(self):
        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=/not/existing
        filesources=static files

        ::static files::static_files::
        /etc/fstab
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(config.sections['static files'].chroot_path, '/')

    def test_non_root_path_in_section_data(self):
        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=/not/existing
        filesources=static files

        ::static files::static_files::/path/to/some/chroot
        /etc/fstab
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(config.sections['static files'].chroot_path, '/path/to/some/chroot')

    def test_variable_non_root_in_section_data(self):
        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::variables::variables::
        chrootpath=/path/to/some/chroot

        ::Chroot::chroot::
        target_directory=/not/existing
        filesources=static files

        ::static files::static_files::%(chrootpath)s
        /etc/fstab
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(config.sections['static files'].chroot_path, '/path/to/some/chroot')

    def test_variable_root_in_section_data(self):
        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::variables::variables::
        chrootpath=/

        ::Chroot::chroot::
        target_directory=/not/existing
        filesources=static files

        ::static files::static_files::%(chrootpath)s
        /etc/fstab
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(config.sections['static files'].chroot_path, '/')


class TestStaticDir(TestBase):
    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_staticdirs(self):
        tempdir = self.get_temp_target_directory(create=False)
        target_directory = os.path.join(tempdir, 'target')
        source_directory = os.path.join(tempdir, 'source')

        os.makedirs(os.path.join(source_directory, '1', '2', '3'))
        source_dir_abc = os.path.join(source_directory, 'a', 'b', 'c')
        os.makedirs(source_dir_abc)
        with open(os.path.join(source_dir_abc, 'd'), 'w') as fd:
            fd.write('test')
        with open(os.path.join(source_directory, 'testfile'), 'w') as fd:
            fd.write('test2')

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=static dirs

        ::static dirs::static_dir::
        source=%s
        target=/testfiles
        ''' % (target_directory, source_directory)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.build_targets()

        self.assertEqual(config.sections['static dirs'].excludes, [])
        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static dirs'], StaticDirSectionHandler)

        self.assertTrue(os.path.isdir(os.path.join(target_directory, 'testfiles', '1', '2', '3')))
        self.assertTrue(os.path.isfile(os.path.join(target_directory, 'testfiles', 'a', 'b', 'c', 'd')))
        self.assertTrue(os.path.isfile(os.path.join(target_directory, 'testfiles', 'testfile')))

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_staticdirs_two_targets_root(self):
        """Check that building two targets with the same static_dir section that has / as target works."""
        # We need that test because we potentially modify self.target
        tempdir = self.get_temp_target_directory(create=False)
        target_directory = os.path.join(tempdir, 'target')
        source_directory = os.path.join(tempdir, 'source')

        os.makedirs(os.path.join(source_directory, '1', '2', '3'))
        source_dir_abc = os.path.join(source_directory, 'a', 'b', 'c')
        os.makedirs(source_dir_abc)
        with open(os.path.join(source_dir_abc, 'd'), 'w') as fd:
            fd.write('test')
        with open(os.path.join(source_directory, 'testfile'), 'w') as fd:
            fd.write('test2')

        c = '''
        name=testlinux
        version=1
        targets=Chroot,Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=static dirs

        ::static dirs::static_dir::
        source=%s
        target=/
        ''' % (target_directory, source_directory)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.build_targets()
        # no assert, just see if it builds without errors

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_staticdirs_exclude(self):
        tempdir = self.get_temp_target_directory(create=False)
        target_directory = os.path.join(tempdir, 'target')
        source_directory = os.path.join(tempdir, 'source')

        os.makedirs(os.path.join(source_directory, '1', '2', '3'))
        source_dir_abc = os.path.join(source_directory, 'a', 'b', 'c')
        os.makedirs(source_dir_abc)
        with open(os.path.join(source_dir_abc, 'd'), 'w') as fd:
            fd.write('test')
        with open(os.path.join(source_directory, 'testfile'), 'w') as fd:
            fd.write('test2')
        os.makedirs(os.path.join(source_directory, 'foo', 'bar', 'blah'))

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=static dirs

        ::static dirs::static_dir::
        source=%s
        target=/testfiles
        exclude=a/**
        exclude=foo
        ''' % (target_directory, source_directory)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.build_targets()

        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static dirs'], StaticDirSectionHandler)

        self.assertTrue(os.path.isdir(os.path.join(target_directory, 'testfiles', '1', '2', '3')))
        self.assertTrue(os.path.isdir(os.path.join(target_directory, 'testfiles', 'a')))
        self.assertTrue(not os.path.exists(os.path.join(target_directory, 'testfiles', 'a', 'b')))
        self.assertTrue(os.path.isfile(os.path.join(target_directory, 'testfiles', 'testfile')))
        self.assertTrue(not os.path.exists(os.path.join(target_directory, 'testfiles', 'foo')))

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_staticdirs_target_root(self):
        tempdir = self.get_temp_target_directory(create=False)
        target_directory = os.path.join(tempdir, 'target')
        source_directory = os.path.join(tempdir, 'source')

        os.makedirs(os.path.join(source_directory, '1', '2', '3'))
        with open(os.path.join(source_directory, 'testfile'), 'w') as fd:
            fd.write('test2')

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=static dirs

        ::static dirs::static_dir::
        source=%s
        target=/
        ''' % (target_directory, source_directory)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.build_targets()

        self.assertEqual(len(config.sections), 3)
        self.assertEqual(len(config.main.targets), 1)
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static dirs'], StaticDirSectionHandler)

        self.assertTrue(os.path.isdir(os.path.join(target_directory, '1', '2', '3')))
        self.assertTrue(os.path.isfile(os.path.join(target_directory, 'testfile')))
