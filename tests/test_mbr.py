# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import hashlib
from unittest import TestCase

from mscreator.mbr import bytes_to_long, bytes_to_short, long_to_bytes, short_to_bytes, PartitionEntry, Mbr

# $ md5sum tests/test_binaries/misc/mbr
# b2f7b51c242d6ed7fc1db1ce960c67e0  tests/test_binaries/misc/mbr
# $ sfdisk -l tests/test_binaries/misc/mbr -uS
#                        Device Boot    Start       End   #sectors  Id  System
# tests/test_binaries/misc/mbr1             1   8401994    8401994  82  Linux swap / Solaris
# tests/test_binaries/misc/mbr2   *   8401995  25189919   16787925  83  Linux
# tests/test_binaries/misc/mbr3      25189920 312576704  287386785  83  Linux
# tests/test_binaries/misc/mbr4             0         -          0   0  Empty

# 1: ~4GB
# 2. ~8GB
# 3. ~140GB


class TestConversions(TestCase):

    def test_bytes_to_long(self):
        self.assertEqual(bytes_to_long(bytearray(b'\x04\x03\x02\x01')), 0x01020304)
        self.assertEqual(bytes_to_long(bytearray(b'\x04\x00\x00\x00')), 0x00000004)
        self.assertEqual(bytes_to_long(bytearray(b'\x00\x00\x00\x04')), 0x04000000)
        self.assertEqual(bytes_to_long(b'\x00\x00\x00\x04'), 0x04000000)

    def test_bytes_to_short(self):
        self.assertEqual(bytes_to_short(bytearray(b'\x02\x01')), 0x0102)
        self.assertEqual(bytes_to_short(bytearray(b'\x04\x00')), 0x0004)
        self.assertEqual(bytes_to_short(bytearray(b'\x00\x04')), 0x0400)

    def test_long_to_bytes(self):
        self.assertEqual(long_to_bytes(0x01020304), bytearray(b'\x04\x03\x02\x01'))
        self.assertEqual(long_to_bytes(0x00000004), bytearray(b'\x04\x00\x00\x00'))
        self.assertEqual(long_to_bytes(0x04000000), bytearray(b'\x00\x00\x00\x04'))

    def test_short_to_bytes(self):
        self.assertEqual(short_to_bytes(0x0102), bytearray(b'\x02\x01'))
        self.assertEqual(short_to_bytes(0x0004), bytearray(b'\x04\x00'))
        self.assertEqual(short_to_bytes(0x0400), bytearray(b'\x00\x04'))


class TestPartitionEntry(TestCase):

    @classmethod
    def setUpClass(cls):
        with open('tests/test_binaries/misc/mbr', 'rb') as fd:
            buf = bytearray(fd.read(512))
        cls.p1 = buf[446:462]
        cls.p2 = buf[462:478]
        cls.p3 = buf[478:494]
        cls.p4 = buf[494:510]

    def test_partition_entry(self):
        entry1 = PartitionEntry(1, self.p1)
        self.assertFalse(entry1.active)
        self.assertEqual(entry1.partition_type, 0x82)
        self.assertEqual(entry1.start_sector, 1)
        self.assertEqual(entry1.size_in_sectors, 8401994)
        self.assertEqual(entry1.size_in_bytes, 8401994 * 512)

        entry2 = PartitionEntry(2, self.p2)
        self.assertTrue(entry2.active)
        self.assertEqual(entry2.partition_type, 0x83)
        self.assertEqual(entry2.start_sector, 8401995)
        self.assertEqual(entry2.size_in_sectors, 16787925)
        self.assertEqual(entry2.size_in_bytes, 16787925 * 512)

        entry3 = PartitionEntry(3, self.p3)
        self.assertFalse(entry3.active)
        self.assertEqual(entry3.partition_type, 0x83)
        self.assertEqual(entry3.start_sector, 25189920)
        self.assertEqual(entry3.size_in_sectors, 287386785)
        self.assertEqual(entry3.size_in_bytes, 287386785 * 512)

        entry4 = PartitionEntry(4, self.p4)
        self.assertFalse(entry4.active)
        self.assertEqual(entry4.partition_type, 0)
        self.assertEqual(entry4.start_sector, 0)
        self.assertEqual(entry4.size_in_sectors, 0)
        self.assertEqual(entry4.size_in_bytes, 0)

    def test_partition_entry_set_active(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.active = True
        self.assertEqual(entry1.bytes[0], 0x80)
        entry1.active = False
        self.assertEqual(entry1.bytes[0], 0x00)

    def test_partition_entry_set_type(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.partition_type = 0x83
        self.assertEqual(entry1.bytes[4], 0x83)

    def test_partition_entry_set_start_sector(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.start_sector = 0x1234
        self.assertEqual(entry1.bytes[8:12], bytearray([0x34, 0x12, 0x00, 0x00]))

    def test_partition_entry_set_size_in_sectors(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.size_in_sectors = 0x1234
        self.assertEqual(entry1.bytes[12:16], bytearray([0x34, 0x12, 0x00, 0x00]))

    def test_partition_entry_set_size_in_bytes_bigger(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.size_in_bytes = 0x1234
        # 0x1234=4460 => with padding: 5120 => 10 sectors
        self.assertEqual(entry1.bytes[12:16], bytearray([0x0a, 0x00, 0x00, 0x00]))

    def test_partition_entry_set_size_in_bytes_equal(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.size_in_bytes = 512
        self.assertEqual(entry1.bytes[12:16], bytearray([0x01, 0x00, 0x00, 0x00]))

    def test_partition_entry_set_size_in_bytes_smaller(self):
        entry1 = PartitionEntry(1, bytearray([0] * 16))
        entry1.size_in_bytes = 123
        self.assertEqual(entry1.bytes[12:16], bytearray([0x01, 0x00, 0x00, 0x00]))


class TestMbr(TestCase):

    def test_read_from_file(self):
        mbr = Mbr(path='tests/test_binaries/misc/mbr')
        checksum = hashlib.md5()
        checksum.update(mbr._bytes)

    def test_no_buf_no_path(self):
        with self.assertRaisesRegex(ValueError, 'At least one of path and buf must be not None!'):
            mbr = Mbr()

    def test_read_from_buffer(self):
        with open('tests/test_binaries/misc/mbr', 'rb') as fd:
            buf = bytearray(fd.read(512))
        mbr = Mbr(buf=buf)
        checksum = hashlib.md5()
        checksum.update(mbr._bytes)

        entry1 = mbr.partitions[1]
        self.assertFalse(entry1.active)
        self.assertEqual(entry1.partition_type, 0x82)
        self.assertEqual(entry1.start_sector, 1)
        self.assertEqual(entry1.size_in_sectors, 8401994)
        self.assertEqual(entry1.size_in_bytes, 8401994 * 512)

        entry2 = mbr.partitions[2]
        self.assertTrue(entry2.active)
        self.assertEqual(entry2.partition_type, 0x83)
        self.assertEqual(entry2.start_sector, 8401995)
        self.assertEqual(entry2.size_in_sectors, 16787925)
        self.assertEqual(entry2.size_in_bytes, 16787925 * 512)

        entry3 = mbr.partitions[3]
        self.assertFalse(entry3.active)
        self.assertEqual(entry3.partition_type, 0x83)
        self.assertEqual(entry3.start_sector, 25189920)
        self.assertEqual(entry3.size_in_sectors, 287386785)
        self.assertEqual(entry3.size_in_bytes, 287386785 * 512)

        entry4 = mbr.partitions[4]
        self.assertFalse(entry4.active)
        self.assertEqual(entry4.partition_type, 0)
        self.assertEqual(entry4.start_sector, 0)
        self.assertEqual(entry4.size_in_sectors, 0)
        self.assertEqual(entry4.size_in_bytes, 0)

        self.assertEqual(mbr.boot_signature, 0xaa55)
        self.assertEqual(mbr.disk_signature, 0x00000000)
        self.assertTrue(mbr.bootable)
        self.assertEqual(mbr.bytes, buf)

    def test_set_boot_signature(self):
        mbr = Mbr(path='tests/test_binaries/misc/mbr')
        mbr.boot_signature = 0x1234
        self.assertEqual(mbr._bytes[510:512], bytearray([0x34, 0x12]))

    def test_set_disk_signature(self):
        mbr = Mbr(path='tests/test_binaries/misc/mbr')
        mbr.disk_signature = 0x12345678
        self.assertEqual(mbr._bytes[440:444], bytearray([0x78, 0x56, 0x34, 0x12]))

    def test_set_partition(self):
        with open('tests/test_binaries/misc/mbr', 'rb') as fd:
            buf = bytearray(fd.read(512))
        mbr = Mbr(buf=buf)
        entry1 = PartitionEntry(1, buf[446:462])
        mbr.partitions[1] = entry1
        self.assertEqual(mbr.bytes, buf)

    def test_set_partition_wrong_type(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        with self.assertRaisesRegex(TypeError, 'Partitions must be of type PartitionEntry!'):
            mbr.partitions[1] = 'string is of the wrong type'

    def test_set_partition_index_wrong_type(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        entry1 = PartitionEntry(1, buf[446:462])
        with self.assertRaisesRegex(TypeError, 'Partition index must be an integer!'):
            mbr.partitions['x'] = entry1

    def test_set_partition_index_out_of_bounds(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        entry1 = PartitionEntry(1, buf[446:462])
        for i in (0, -1, 5):
            with self.assertRaisesRegex(ValueError, 'Partition index must be in .*'):
                mbr.partitions[i] = entry1

    def test_get_partition_index_ok(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        for i in range(len(mbr.partitions)):
            x = mbr.partitions[i + 1]

    def test_get_partition_index_out_of_bounds(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        for i in (0, -1, 5):
            with self.assertRaisesRegex(ValueError, 'Partition index must be in .*'):
                x = mbr.partitions[i]

    def test_get_partition_index_wrong_type(self):
        buf = bytearray([0] * 512)
        mbr = Mbr(buf=buf)
        with self.assertRaisesRegex(TypeError, 'Partition index must be an integer!'):
            x = mbr.partitions['abc']
