# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
import logging

import mscreator
from mscreator import MscConfig
from mscreator.commands import touch, Command
from mscreator.exceptions import PathExistsError
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


def list_cpio_contents(image_path, decompress_command=None):
    if decompress_command is None:
        decompress_command = 'cat %(filename)s'
    cpio = Command(command_path='/bin/sh')
    archive_files = cpio(
        parameters=['-c', '%s|cpio -t' % (decompress_command % {'filename': image_path})]).strip().split('\n')
    return set(archive_files)


class TestInitramfsSection(TestBase):

    def test_overwrite_existing(self):
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'initramfs')
        c = '''
        name=testlinux
        version=1
        targets=initramfs

        ::initramfs::initramfs::
        image_path=%(image_path)s
        compression_command=
        filesources=static files

        ::static files::static_files::/
        /etc/fstab
        ''' % {'image_path': image_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        if not os.path.exists(image_path):
            touch(image_path)
        self.assertTrue(os.path.exists(image_path))
        with self.assertRaisesRegex(PathExistsError, 'already exists, use -O'):
            config.parse()

        old_overwrite = mscreator.mscglobals.OVERWRITE_TARGETS
        mscreator.mscglobals.OVERWRITE_TARGETS = True
        config.parse()
        image_section = config.sections['initramfs']
        files_section = config.sections['static files']
        files_section.calculate_file_dependencies()
        image_section.build_system()
        mscreator.mscglobals.OVERWRITE_TARGETS = old_overwrite

        archive_files = list_cpio_contents(image_path)
        self.assertSetEqual(archive_files, {'.', 'etc', 'etc/fstab'})

    def test_with_compression(self):
        target_directory = self.get_temp_target_directory()

        image_path = os.path.join(target_directory, 'initramfs')
        c = '''
        name=testlinux
        version=1
        targets=initramfs

        ::initramfs::initramfs::
        image_path=%(image_path)s
        compression_command=gzip
        filesources=static files

        ::static files::static_files::/
        /etc/fstab
        ''' % {'image_path': image_path}

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        config.parse()
        image_section = config.sections['initramfs']
        files_section = config.sections['static files']
        files_section.calculate_file_dependencies()
        image_section.build_system()

        archive_files = list_cpio_contents(image_path, decompress_command='zcat %(filename)s')
        self.assertSetEqual(archive_files, {'.', 'etc', 'etc/fstab'})
