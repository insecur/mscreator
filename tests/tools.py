# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import inspect
import logging
import os
import shutil
import tempfile
from unittest import TestCase
import mscreator
from mscreator.utils import boolean

config = mscreator.ConfigNamespace()
try:
    config.loglevel = int(os.environ.get('LOGLEVEL', 999))
except TypeError:
    config.loglevel = 999
TMP = os.path.abspath(os.environ.get('TMP', '.'))
config.keep_temp = boolean(os.environ.get('KEEPTEMP', False))
mscreator.init(config)
logger = logging.getLogger('mscreator')

# noinspection PyBroadException
try:
    skipslow_limit = int(os.environ['skipslow'])
except Exception:
    skipslow_limit = 999999


def _id(x):
    return x


class FakeMain(object):
    temporary_directory = os.path.join(TMP, 'tmp')  # tempfile.mkdtemp(suffix='-%s' % os.getpid())
    download_cache_directory = os.path.join(TMP, 'cache', 'download')


class FakeConfig(object):
    main = FakeMain()
    variables = {
        'just_some_variable': True
    }


class TestBase(TestCase):
    tempdir = None

    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.mkdtemp()
        mscreator.mscglobals.CONFIG = FakeConfig()
        mscreator.prepare_build_environment()
        cls.test_chroot_path = get_chroot_path()

    @classmethod
    def tearDownClass(cls):
        if not mscreator.mscglobals.KEEPTEMP:
            saferm(cls.tempdir)
            mscreator.cleanup_build_environment()

    def setUp(self):
        self.oldcwd = os.getcwd()
        self.tests_cwd = tempfile.mkdtemp()
        os.chdir(self.tests_cwd)

    def tearDown(self):
        os.chdir(self.oldcwd)
        if not mscreator.mscglobals.KEEPTEMP:
            saferm(self.tests_cwd)

    @classmethod
    def super(cls):
        if not inspect.isclass(cls):
            raise TypeError('This method is intended to be used with a class, not an instance.')
        instance = next(c for c in cls.__mro__ if c.__name__ == cls.__name__)  # if c.__module__ == __name__)
        return super(instance, cls)

    def get_temp_target_directory(self, create=True, suffix=''):
        """Calculate a path name based on the calling method's name.
        A directory for this path is created. The path is also marked to be removed after the test finishes.

        :rtype: str or unicode
        """
        target_parent = os.path.join(self.tempdir, self.__class__.__name__)
        target_directory = os.path.join(target_parent, '%s%s' % (inspect.stack()[1][3], suffix))
        # logger.setLevel(0)
        # logger.debug('setting target_directory to: %s' % target_directory)
        # logger.setLevel(999)
        self.addCleanup(saferm, target_directory)
        os.makedirs(target_parent, exist_ok=True)
        if create:
            os.makedirs(target_directory, exist_ok=True)
        return target_directory


def get_chroot_path():
    test_binaries_path = os.path.join(os.path.dirname(__file__), 'test_binaries')
    test_chroot_path = os.path.join(test_binaries_path, 'chroot')
    if not os.path.exists(test_chroot_path):
        try:
            old_cwd = os.getcwd()
            os.chdir(test_binaries_path)
            os.system('make install')
            os.chdir(old_cwd)
        except Exception:
            raise OSError('Test chroot "%s" does not exist, ' % test_chroot_path)
    return test_chroot_path


def safely_call_chrooted_function(func, args=None, kwargs=None):
    if args is None:
        args = tuple()
    if kwargs is None:
        kwargs = dict()
    pid = os.getpid()
    try:
        func(*args, **kwargs)
    except SystemExit:
        pass
    new_pid = os.getpid()
    if new_pid != pid:
        os.kill(new_pid, 9)


def saferm(target):
    try:
        if os.path.isdir(target):
            shutil.rmtree(target)
        else:
            os.unlink(target)
    except OSError:
        pass


def re_replace(s):
    for old, new in (('(', '\('),
                     (')', '\)')
                     ):
        s = s.replace(old, new)
    return s
