# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
import re
from unittest import skipIf
import logging

import mscreator.config as msc_config
from mscreator.utils import temporary_file, Version

import mscreator
from mscreator import MscConfig
from mscreator.config_handlers import (
    ConfigSectionHandlerBase, MainSectionHandler, HooksSectionHandler, VariablesSectionHandler, handler_by_section_id)
from mscreator.config_handlers.filesource_staticfiles import StaticFilesSectionHandler, StaticDirSectionHandler
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from mscreator.exceptions import PathNotFoundError
from tests.tools import TestBase, re_replace

logger = logging.getLogger('mscreator')


class TestMscConfig(TestBase):

    def test_explicit_main_forbidden(self):
        c = '''
        ::main::main::
        name=testlinux
        version=1
        targets=
        '''
        config = MscConfig(lines=c)
        with self.assertRaisesRegex(msc_config.ConfigError, 'The header for the "main" section must not be specified'):
            config.parse()

    def test_filename_and_lines_none(self):
        with self.assertRaisesRegex(ValueError, 'At least one of filename and lines must be not None!'):
            config = MscConfig()

    def test_incomplete_main(self):
        c = '''
        name=testlinux
        version=1
        '''
        config = MscConfig(lines=c)
        with self.assertRaisesRegex(msc_config.ConfigError, 'Required value "targets".*not provided!'):
            config.parse()

    def test_too_few_header_fields(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::my section::static_files
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'not of form "::name::type::data"'):
            config.parse()

    def test_unknown_section_type(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::my section::unknown_type::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Unknown section type "unknown_type"! \(In section "my section"\("<buffer>:6"\)\)'):
            config.parse()

    def test_duplicate_main(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::My Main::main::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Section of type "main" must only be defined once'):
            config.parse()

    def test_duplicate_section_name(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::s1::static_files::/
        ::s1::inlinescript::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Duplicate section name "s1"! \(In section "s1"\("<buffer>:7"\)\)'):
            config.parse()

    def test_register_handler(self):
        class MyHandler(ConfigSectionHandlerBase):
            section_id = 'register_handler_test'

        self.assertIs(handler_by_section_id('register_handler_test'), MyHandler)

    def test_register_duplicate_handler(self):
        class MyHandler1(ConfigSectionHandlerBase):
            section_id = 'register_duplicate_handler_test'

        with self.assertLogs(logger='mscreator.config_handlers', level='WARNING') as cm:
            class MyHandler2(ConfigSectionHandlerBase):
                section_id = 'register_duplicate_handler_test'
            self.assertEqual(cm.records[0].message, 'Ignoring additional handler for "register_duplicate_handler_test"!')

    def test_minimal_working(self):
        c = '''
        # This is no real world example, of course, but it should not cause errors anyway.
        name=testlinux
        version=1
        targets=
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 1)
        self.assertIsInstance(list(config.sections.values())[0], MainSectionHandler)
        self.assertIsInstance(config.hooks, HooksSectionHandler)
        self.assertIsInstance(config.variables, VariablesSectionHandler)

        config.build_targets()

    def test_version_too_low(self):
        msc_version = Version(mscreator.mscglobals.MSC_VERSION)
        min_version = '%s.0.0' % (msc_version.major + 1)
        c = '''
        name=testlinux
        version=1
        msc_min_version=%s
        targets=
        ''' % min_version
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Configuration requires at least mscreator version "%s", the current version is "%s"' % (
                min_version, msc_version)):
            config.parse()

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_two_targets(self):
        """Check that two different targets are built, if specified."""
        target_directory1 = self.get_temp_target_directory(create=False)
        target_directory2 = self.get_temp_target_directory(create=False, suffix=2)

        c = '''
        name=testlinux
        version=1
        targets=Chroot1,Chroot2

        ::Chroot1::chroot::
        target_directory=%s
        filesources=static files,static dirs

        ::Chroot2::chroot::
        target_directory=%s
        filesources=static files

        ::static files::static_files::/
        /etc/fstab

        ::static dirs::static_dir::
        source=/dev
        target=/dev
        ''' % (target_directory1, target_directory2)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 5)
        self.assertEqual(len(config.main.targets), 2)
        self.assertIsInstance(config.sections['Chroot1'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['Chroot2'], ChrootSectionHandler)
        self.assertIsInstance(config.sections['static files'], StaticFilesSectionHandler)
        self.assertIsInstance(config.sections['static dirs'], StaticDirSectionHandler)

        config.build_targets()
        self.assertTrue(os.path.exists(os.path.join(target_directory1, 'etc/fstab')))
        self.assertTrue(os.path.exists(os.path.join(target_directory1, 'dev/null')))
        self.assertTrue(os.path.exists(os.path.join(target_directory2, 'etc/fstab')))
        self.assertFalse(os.path.exists(os.path.join(target_directory2, 'dev/null')))

    def test_bad_target_reference(self):
        c = '''
        name=testlinux
        version=1
        targets=TargetSystem

        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Could not find section "TargetSystem" referenced as target'):
            config.parse()

        config.build_targets()

    def test_duplicate_unique_value(self):
        c = '''
        name=testlinux
        version=1
        version=2
        targets=
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, 'Duplicate key "version"! \(In section "main"\("<buffer>:4"\)'):
            config.parse()

    def test_value_starting_with_equal_sign(self):
        class TestStringSectionHandler(ConfigSectionHandlerBase):
            section_id = 'test_string_section'
            description = 'simple handler that holds key=value pairs'

        c = '''
        name=testlinux
        version=1
        targets=

        ::test = value::test_string_section::
        =blah=blub
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        test_section = config.sections['test = value']
        the_equal_value = test_section[0]
        self.assertIsInstance(the_equal_value, msc_config.KeyValueEntry)
        self.assertEqual(the_equal_value.key, '')
        self.assertEqual(the_equal_value.value, 'blah=blub')

    def test_line_value_types(self):
        class TestStringSectionHandler(ConfigSectionHandlerBase):
            section_id = 'test_string_section'
            description = 'simple handler that holds key=value pairs'

        c = '''
        name=testlinux
        version=1
        targets=

        ::test = value::test_string_section::
        =nokey
        # this is a comment

        key=value
        rawline
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        test_section = config.sections['test = value']
        self.assertIsInstance(test_section._values[0], msc_config.ConfigValue)
        self.assertIsInstance(test_section._values[1], msc_config.ConfigValue)
        self.assertIsInstance(test_section._values[2], msc_config.ConfigValue)
        self.assertIsInstance(test_section._values[3], msc_config.ConfigValue)
        self.assertIsInstance(test_section._values[4], msc_config.ConfigValue)


class TestPreprocessor(TestBase):

    def test_source_existing_file(self):
        c = '''
        name=testlinux
        version=1
        targets=
        #>source=%s
        '''

        sourced_lines = '''
::sourced section::static_files::/
/etc/fstab
        '''
        with temporary_file(prefix='test_preprocessor_source_existing_file.sourced_file') as tempfile:
            tempfile.fd.writelines(sourced_lines)
            tempfile.close()

            config = MscConfig(lines=c % tempfile.filename)
            mscreator.mscglobals.CONFIG = config
            config.parse()

            test_section = config.sections['sourced section']
            self.assertIsInstance(test_section, StaticFilesSectionHandler)

    def test_source_nested_files(self):
        c = '''
        name=testlinux
        version=1
        targets=
        #>source=%s
        '''

        sourced_section_lines = '''
::sourced section::static_files::/
/etc/fstab
#>source=%s
        '''

        sourced_files_lines = '''
/etc/passwd
/etc/group
        '''

        with temporary_file(prefix='test_preprocessor_source_nested_files.files_file') as files_file:
            files_file.fd.writelines(sourced_files_lines)
            files_file.close()
            with temporary_file(prefix='test_preprocessor_source_nested_files.section_file') as section_file:
                section_file.fd.writelines(sourced_section_lines % files_file.filename)
                section_file.close()

                config = MscConfig(lines=c % section_file.filename)
                mscreator.mscglobals.CONFIG = config
                config.parse()

                test_section = config.sections['sourced section']
                self.assertIsInstance(test_section, StaticFilesSectionHandler)
                test_section.calculate_file_dependencies()
                self.assertSetEqual(test_section.calculated_dependencies, {'/etc/fstab', '/etc/passwd', '/etc/group'})

    def test_source_non_existing_file(self):
        c = '''
        name=testlinux
        version=1
        targets=
        #>source=/path/to/some_non_existant_file
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(PathNotFoundError, 'Can not read file "/path/to/some_non_existant_file"!'):
            config.parse()

    def test_source_nested_error(self):
        c = '''
        name=testlinux
        version=1
        targets=
        #>source=%s
        '''

        sourced_section_lines = '''
::sourced section::static_files::/
/etc/fstab
#>source=%s
        '''

        sourced_files_lines = '''
/etc/passwd
/etc/group

::broken section header::
        '''

        with temporary_file(prefix='test_preprocessor_source_nested_error.files_file') as files_file:
            files_file.fd.writelines(sourced_files_lines)
            files_file.close()
            with temporary_file(prefix='test_preprocessor_source_nested_error.section_file') as section_file:
                section_file.fd.writelines(sourced_section_lines % files_file.filename)
                section_file.close()

                config = MscConfig(lines=c % section_file.filename)
                mscreator.mscglobals.CONFIG = config
                with self.assertRaisesRegex(msc_config.ConfigError, 'Section line .* not of form .*sourced from .* => sourced from .*'):
                    config.parse()


class TestConfigError(TestBase):
    def test_simple(self):
        with self.assertRaisesRegex(msc_config.ConfigError, 'Error in config!'):
            raise msc_config.ConfigError('^Error in config!$')

    def test_location_is_str(self):
        with self.assertRaisesRegex(msc_config.ConfigError, '^Error in config! "somewhere in the config file"$'):
            raise msc_config.ConfigError('Error in config!', location="somewhere in the config file")

    def test_error_within_section(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::section::chroot::/
        xy=
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError,
                                    re_replace('Required value .* not provided! (In section "section"(starting at "<buffer>:6"))')):
            config.parse()

    def test_error_within_section_exact(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::variables::
        x=y
        blabl
        a=b
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, re_replace('Only key=value pairs, .*! (In section "variables"("<buffer>:8"))')):
            config.parse()

    def test_error_first_section_line(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::variables::
        blabl
        x=y
        a=b
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, re_replace('Only key=value pairs, .*! (In section "variables"("<buffer>:7"))')):
            config.parse()

    def test_error_last_section_line(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::variables::
        a=b
        blabl

        ::hooks::hooks::
        '''

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(msc_config.ConfigError, re_replace('Only key=value pairs, .*! (In section "variables"("<buffer>:8"))')):
            config.parse()

    def test_doubly_included_file(self):
        c = '''
        name=testlinux
        version=1
        targets=

        #>source=%s

        ::section::static_files::/
        /bla/blub
        '''

        sourced_lines = '''
::sourced section::static_files::/
/etc/fstab

#>source=%s
        '''

        sourced_sourced_lines = '''
::another section:error::/
        '''

        with temporary_file(prefix='test_preprocessor_source_nested_files.files_file') as sourced_sourced_file:
            sourced_sourced_file.fd.writelines(sourced_sourced_lines)
            sourced_sourced_file.close()
            with temporary_file(prefix='test_preprocessor_source_nested_files.section_file') as sourced_file:
                sourced_file.fd.writelines(sourced_lines % sourced_sourced_file.filename)
                sourced_file.close()

                config = MscConfig(lines=c % sourced_file.filename)
                mscreator.mscglobals.CONFIG = config
                with self.assertRaisesRegex(msc_config.ConfigError, re_replace(
                                            """^Section line (::another section:error::/) is not of form "::name::type::data"! """
                                            """(In section "another section:error"(".*/test_preprocessor_source_nested_files.files_file.*:2"): """
                                            """sourced from ".*/test_preprocessor_source_nested_files.section_file.*:5" => """
                                            """sourced from "<buffer>:6")$""")
                                            ):
                    config.parse()
