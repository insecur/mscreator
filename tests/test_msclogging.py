# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

from io import StringIO

from unittest import TestCase
import logging
from mscreator.msclogging import parse_loglevel, TRACE, DEBUG, INFO, TraceLogger


class DummyClass(object):
    pass


class TestParse_loglevel(TestCase):

    def test_int(self):
        l = parse_loglevel(1)
        self.assertEqual(l, 1)

    def test_intstring(self):
        l = parse_loglevel('1')
        self.assertEqual(l, 1)

    def test_lowercase_TRACE(self):
        l = parse_loglevel('trace')
        self.assertEqual(l, TRACE)

    def test_DEBUG(self):
        l = parse_loglevel('DEBUG')
        self.assertEqual(l, DEBUG)

    def test_TRACE(self):
        l = parse_loglevel('TRACE')
        self.assertEqual(l, TRACE)

    def test_unknown(self):
        l = parse_loglevel('unknown_loglevel')
        self.assertEqual(l, INFO)

    def test_arbitrary_object_as_loglevel(self):
        l = parse_loglevel(DummyClass())
        self.assertEqual(l, INFO)


class TestTraceLogger(TestCase):
    streamfd = None

    @classmethod
    def setUpClass(cls):
        # noinspection PyCallingNonCallable
        cls.streamfd = StringIO()
        cls.logger = TraceLogger('testlogger')
        stream_handler = logging.StreamHandler(cls.streamfd)
        cls.logger.addHandler(stream_handler)

    @classmethod
    def tearDownClass(cls):
        cls.streamfd.close()

    def test_trace_when_trace_enabled(self):
        self.logger.setLevel(TRACE)
        self.logger.trace('blabla')
        self.assertEqual(self.streamfd.getvalue(), 'blabla\n')

    def test_trace_when_trace_disabled(self):
        self.logger.setLevel(DEBUG)
        self.logger.trace('blabla')
        self.assertEqual(self.streamfd.getvalue(), '')
