# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os
from unittest import TestCase, skipIf
import time

import mscreator
from mscreator.config import KeyValueEntry, ConfigError
import mscreator.msclogging
from mscreator import MscConfig, ConfigFileLine
from mscreator.config_handlers.filesource_stracefiles import StraceFilesSectionHandler, run_external_command, \
    TERMINATE_TIMEOUT, StraceFile
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from mscreator.utils import chroot
from tests.tools import safely_call_chrooted_function, TestBase


class TestStraceFile(TestBase):

    def test_no_options_given(self):
        sf = StraceFile('/bin/true', [])
        self.assertListEqual(sf.commandline, ['/bin/true'])
        self.assertEqual(sf.stopcommand, None)
        self.assertEqual(sf.timeout, None)

    def test_all_options_given(self):
        sf = StraceFile('/bin/true', [
            KeyValueEntry(ConfigFileLine(''), 'timeout', '1'),
            KeyValueEntry(ConfigFileLine(''), 'stop_command', 'killall true')])
        self.assertListEqual(sf.commandline, ['/bin/true'])
        self.assertEqual(sf.stopcommand, 'killall true')
        self.assertEqual(sf.timeout, 1)

    def test_broken_option_ignored(self):
        timeout_kv = KeyValueEntry(ConfigFileLine(''), 'timeout', '1')
        del timeout_kv.value

        sf = StraceFile('/bin/true', [timeout_kv])
        self.assertListEqual(sf.commandline, ['/bin/true'])
        self.assertEqual(sf.stopcommand, None)
        self.assertEqual(sf.timeout, None)

    def test_unknown_option_given(self):
        with self.assertRaisesRegex(ConfigError, 'Unknown value "x" in .*'):
            StraceFile('/bin/true', [KeyValueEntry(ConfigFileLine(''), 'x', 'y')])


class TestStraceFilesSection(TestBase):

    @skipIf(os.getuid() != 0, 'Need root privileges for chrooting.')
    def test_stracefiles(self):
        """Check that test binary and it's dependencies are considered correctly in strace_files section."""
        target_directory = self.get_temp_target_directory(create=False)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s
        filesources=strace files, test temp files

        ::strace files::strace_files::%s
        /bin/testbinary

        ::test temp files::strace_files::/
        touch /tmp/x
        rm /tmp/x
        ''' % (target_directory, self.test_chroot_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 4)
        strace_section = config.sections['strace files']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(strace_section, StraceFilesSectionHandler)

        safely_call_chrooted_function(strace_section.calculate_file_dependencies)

        libdir = '/lib64'
        libc_link = '%s/libc.so.6' % libdir
        with chroot(self.test_chroot_path):
            libc_link_target = '%s/%s' % (libdir, os.readlink(libc_link))
        for path in ('%s/libmylib1.so' % libdir, '%s/libmylib2.so' % libdir,
                     libc_link, libc_link_target,
                     '/bin/testbinary', '/sbin/init'):
            self.assertIn(path, strace_section.calculated_dependencies)

        safely_call_chrooted_function(config.build_targets)

        self.assertTrue(os.path.islink(os.path.join(target_directory, libc_link.lstrip('/'))))
        self.assertFalse(os.path.exists(os.path.join(target_directory, 'tmp', 'x')))


class TestRunExternalCommand(TestCase):

    def setUp(self):
        self.old_loglevel = mscreator.mscglobals.LOGLEVEL

    def tearDown(self):
        mscreator.mscglobals.LOGLEVEL = self.old_loglevel

    def test_timeout_not_exceeded_no_trace(self):
        mscreator.mscglobals.LOGLEVEL = logging.INFO
        start = time.time()
        run_external_command(['/bin/sleep', '0.00000000001'], timeout=10)
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0, delta=0.3)

    def test_timeout_not_exceeded_trace(self):
        mscreator.mscglobals.LOGLEVEL = mscreator.msclogging.TRACE
        start = time.time()
        run_external_command(['/bin/sleep', '0.00000000001'], timeout=10)
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0, delta=0.3)

    def test_no_timeout_no_stopcommand_no_trace(self):
        # just run to see if we do not get an exception
        mscreator.mscglobals.LOGLEVEL = logging.INFO
        run_external_command(['/bin/sleep', '0.00000000001'])

    def test_no_timeout_no_stopcommand_trace(self):
        # just run to see if we do not get an exception
        mscreator.mscglobals.LOGLEVEL = mscreator.msclogging.TRACE
        run_external_command(['/bin/sleep', '0.00000000001'])

    def test_timeout_no_stopcommand_no_trace(self):
        mscreator.mscglobals.LOGLEVEL = logging.INFO
        start = time.time()
        run_external_command(['/bin/sleep', '1'], timeout=0.1)
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.1 + TERMINATE_TIMEOUT, delta=0.1)

    def test_timeout_no_stopcommand_trace(self):
        mscreator.mscglobals.LOGLEVEL = mscreator.msclogging.TRACE
        start = time.time()
        run_external_command(['/bin/sleep', '1'], timeout=0.1)
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.1 + TERMINATE_TIMEOUT, delta=0.1)

    def test_timeout_stopcommand_no_trace(self):
        mscreator.mscglobals.LOGLEVEL = logging.INFO
        start = time.time()
        run_external_command(['/bin/sleep', '1'], timeout=0.1, stopcommand='kill %(PID)s')
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.1, delta=0.1)

    def test_timeout_stopcommand_trace(self):
        mscreator.mscglobals.LOGLEVEL = mscreator.msclogging.TRACE
        start = time.time()
        run_external_command(['/bin/sleep', '1'], timeout=0.1, stopcommand='kill %(PID)s')
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.1, delta=0.1)

    def test_non_existant_no_trace(self):
        self.assertFalse(os.path.exists('/hopefully/not/existing'))
        mscreator.mscglobals.LOGLEVEL = logging.INFO
        start = time.time()
        with self.assertRaisesRegex(OSError, 'No such file or directory'):
            run_external_command(['/hopefully/not/existing'], timeout=0.1, stopcommand='kill %(PID)s')
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.0, delta=0.1)

    def test_non_existant_trace(self):
        self.assertFalse(os.path.exists('/hopefully/not/existing'))
        mscreator.mscglobals.LOGLEVEL = mscreator.msclogging.TRACE
        start = time.time()
        with self.assertRaisesRegex(OSError, 'No such file or directory'):
            run_external_command(['/hopefully/not/existing'], timeout=0.1, stopcommand='kill %(PID)s')
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.0, delta=0.1)
