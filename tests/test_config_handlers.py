# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging

import mscreator
from mscreator import MscConfig
from mscreator.config import ConfigError
from mscreator.config_handlers import ConfigSectionHandlerBase, KnownValue, unregister_section_handler, list_config_section_handlers
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


class Handler_OneRequiredValueHandler(ConfigSectionHandlerBase):
    section_id = 'one_required_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None


class Handler_TwoRequiredValuesHandler(ConfigSectionHandlerBase):
    section_id = 'two_required_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
        KnownValue('v2', 'desc v2'),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None
        self.v2 = None


class Handler_NonUniqueValueHandler(ConfigSectionHandlerBase):
    section_id = 'unique_value_test'
    known_values = (
        KnownValue('v1', 'desc v1', unique=False),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None


class Handler_NoMatchingAttributeValueHandler(ConfigSectionHandlerBase):
    section_id = 'no_matching_attribute_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
    )


class Handler_RenamedValueHandler(ConfigSectionHandlerBase):
    section_id = 'renamed_value_test'
    known_values = (
        KnownValue('v1', 'desc v1', attribute_name='v1s'),
    )

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1s = None


class Handler_IgnoredValueHandler(ConfigSectionHandlerBase):
    section_id = 'ignored_value_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
        KnownValue('v2', 'desc v2'),
        KnownValue('v3', 'desc v3'),
    )
    ignore_value_attributes = ['v2']

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None
        self.v2 = None
        self.v3 = None


class Handler_IgnoredAndRenamedValueHandler(ConfigSectionHandlerBase):
    section_id = 'ignored_and_renamed_value_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
        KnownValue('v2', 'desc v2'),
        KnownValue('v3', 'desc v3', attribute_name='v3s'),
    )
    ignore_value_attributes = ['v3']

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None
        self.v2 = None
        self.v3s = None


class Handler_AllIgnoredValueHandler(ConfigSectionHandlerBase):
    section_id = 'all_ignored_value_test'
    known_values = (
        KnownValue('v1', 'desc v1'),
        KnownValue('v2', 'desc v2'),
        KnownValue('v3', 'desc v3'),
    )
    ignore_value_attributes = '*'

    def __init__(self, name, data, content_lines, start, end):
        super().__init__(name, data, content_lines, start, end)
        self.v1 = None
        self.v2 = None
        self.v3 = None


class TestSectionBaseHandlerSetAttributes(TestBase):

    def test_unique_one_required_item(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        ''' % Handler_OneRequiredValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        x_section = config.sections['x']
        self.assertEqual(x_section.v1, '123')

    def test_unique_several_items(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        v2=234
        ''' % Handler_TwoRequiredValuesHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        x_section = config.sections['x']
        self.assertEqual(x_section.v1, '123')
        self.assertEqual(x_section.v2, '234')

    def test_non_unique_one_item(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        ''' % Handler_NonUniqueValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        x_section = config.sections['x']
        self.assertListEqual(x_section.v1, ['123'])

    def test_non_unique_several_items(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=456
        v1=123
        ''' % Handler_NonUniqueValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        x_section = config.sections['x']
        self.assertListEqual(x_section.v1, ['456', '123'])

    def test_no_matching_attribute_item(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        ''' % Handler_NoMatchingAttributeValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertLogs(logger='mscreator.config_handlers', level='DEBUG') as cm:
            config.parse()
            self.assertIn('DEBUG:mscreator.config_handlers:Section "x" has no attribute "v1", not updating...', cm.output)

        x_section = config.sections['x']
        self.assertFalse(hasattr(x_section, 'v1'))
        self.assertListEqual(x_section.items(), [('v1', '123')])

    def test_renamed_attribute(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        ''' % Handler_RenamedValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        x_section = config.sections['x']
        self.assertEqual(x_section.v1s, '123')
        self.assertFalse(hasattr(x_section, 'v1'))
        self.assertListEqual(x_section.items(), [('v1', '123')])

    def test_ignored_attribute(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        v2=456
        v3=789
        ''' % Handler_IgnoredValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertLogs(logger='mscreator.config_handlers', level='DEBUG') as cm:
            config.parse()
            self.assertIn('DEBUG:mscreator.config_handlers:Ignoring attribute value "v2" in section "x"...', cm.output)

        x_section = config.sections['x']
        self.assertEqual(x_section.v1, '123')
        self.assertEqual(x_section.v2, None)
        self.assertEqual(x_section.v3, '789')
        self.assertListEqual(x_section.items(), [('v1', '123'), ('v2', '456'), ('v3', '789')])

    def test_all_ignored_attribute(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        v2=456
        v3=789
        ''' % Handler_AllIgnoredValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertLogs(logger='mscreator.config_handlers', level='DEBUG') as cm:
            config.parse()

            self.assertIn('DEBUG:mscreator.config_handlers:Not updating any attributes from values.', cm.output)

        x_section = config.sections['x']
        self.assertEqual(x_section.v1, None)
        self.assertEqual(x_section.v2, None)
        self.assertEqual(x_section.v3, None)
        self.assertListEqual(x_section.items(), [('v1', '123'), ('v2', '456'), ('v3', '789')])

    def test_ignored_and_renamed_attribute(self):
        c = '''
        name=testlinux
        version=199
        targets=

        ::x::%s::
        v1=123
        v2=456
        v3=789
        ''' % Handler_IgnoredAndRenamedValueHandler.section_id
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertLogs(logger='mscreator.config_handlers', level='DEBUG') as cm:
            config.parse()
            self.assertIn('DEBUG:mscreator.config_handlers:Ignoring attribute value "v3" in section "x"...', cm.output)

        x_section = config.sections['x']
        self.assertEqual(x_section.v1, '123')
        self.assertEqual(x_section.v2, '456')
        self.assertEqual(x_section.v3s, None)
        self.assertListEqual(x_section.items(), [('v1', '123'), ('v2', '456'), ('v3', '789')])


class TestSectionBaseHandler(TestBase):

    def test_main_expand(self):
        """Check that variables from the main section are expanded."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/%(version)s/some/image
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.image, '/path/199/some/image')

    def test_nested_expand(self):
        """Check that variables that contain a string which also contains a variable reference are fully expanded."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::kernel::kernel::
        image=/path/%(kernelversion)s/some/image
        kernelversion=1.%(version)s.3
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertLogs(logger='mscreator.config_handlers', level='DEBUG') as cm:
            config.parse()
            self.assertIn('DEBUG:mscreator.config_handlers:Could not expand all variables in "/path/1.%(version)s.3/some/image", rescheduling...', cm.output)

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.kernelversion, '1.199.3')
        self.assertEqual(kernel_section.image, '/path/1.199.3/some/image')

    def test_bad_format_character(self):
        """Check that known "bad format characters" are not expanded."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/%:xx/some/image
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.image, '/path/%:xx/some/image')

    def test_static_variables(self):
        """Check that variables from the variables section are expanded."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::variables::variables::
        foo=bar

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/to/%(foo)s/image
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.image, '/path/to/bar/image')

    def test_dynamic_variables(self):
        """Check that variables from a dynamicvariables section are expanded."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::dynvars::dynamicvariables::
#!/bin/sh
echo foo=bar

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/to/%(foo)s/image
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.image, '/path/to/bar/image')

    def test_foreign_section_variables_not_visible(self):
        """Check that in a section no variables from another section are visible."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::dynvars::dynamicvariables::
#!/bin/sh
echo foo=bar

        ::dirs::static_dir::
        source=/tmp/1
        target=/tmp/2

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/to/%(source)s/image
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Specified unknown variable "source"'):
            config.parse()

    def test_percentage(self):
        """Check that strings containing bare % sign are supported."""
        c = '''
        name=testlinux
        version=199
        targets=

        ::kernel::kernel::
        kernelversion=1.2.3
        image=/path/%(version)s/some/15%-2% is larger than 10%: 3%.
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        kernel_section = config.sections['kernel']
        self.assertEqual(kernel_section.image, '/path/199/some/15%-2% is larger than 10%: 3%.')


class TestVariablesSection(TestBase):

    def test_invalid_value(self):
        """Check that only key/values, empty and comment lines are allowed in variables section."""
        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::variables::
        # valid comment

        blah=blub
        foo=bar
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        c += '\na_string_is_not_allowed_in_variables\n'
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Only key=value pairs, comments and empty lines are allowed in variables sections'):
            config.parse()

    def test_variables_name_wrong(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::vars::variables::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Section of type "variables" must be named "variables"'):
            config.parse()

    def test_variables_type_wrong(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::variables::static_files::/
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Special section "variables" must be of type "variables"'):
            config.parse()


class TestHooksSection(TestBase):

    def test_hooks_name_wrong(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::myhooks::hooks::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Section of type "hooks" must be named "hooks"'):
            config.parse()

    def test_hooks_type_wrong(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::hooks::static_files::/
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Special section "hooks" must be of type "hooks"'):
            config.parse()

    def test_bad_hook_reference(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::hooks::hooks::
        pre_finalize=not_existing
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Could not find section "not_existing"'):
            config.parse()

        config.build_targets()

    def test_unknown_hook_name(self):
        c = '''
        name=testlinux
        version=1
        targets=

        ::hooks::hooks::
        undefined_hook_name_blabla=blabla_hook

        ::blabla_hook::inlinescript::
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        with self.assertRaisesRegex(ConfigError, 'Unknown hook "undefined_hook_name_blabla"'):
            config.parse()

    def test_hook_called(self):
        """Verify that a configured hook can actually be called."""
        c = '''
        name=testlinux
        version=1
        targets=

        ::hooks::hooks::
        pre_handle_sections=pre_handle_sections_hook

        ::pre_handle_sections_hook::inlinescript::
#!/bin/sh
        '''
        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        config.hooks.call('pre_handle_sections')


class TestTargetSystemHandlerBase(TestBase):
    def test_livedir_is_absolute(self):
        """Verify that the livedir attribute of targetsystem handlers is an absolute path."""
        c = '''
        name=testlinux
        version=1
        targets=

        '''
        targetsystem_handlers = [h for h in list_config_section_handlers().values() if h.category == 'targetsystem']

        for targetsystem_handler in targetsystem_handlers:
            c += '::%(name)s::%(name)s::\n' % {'name': targetsystem_handler.section_id}
            for known_value in targetsystem_handler.known_values:
                if known_value.required:
                    c += '%s=SOME_UNUSED_FAKE_VALUE\n' % known_value.name

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        for section in config.sections.values():
            if section.category != 'targetsystem':
                continue
            self.assertTrue(section.livedir.startswith('/'),
                            msg='Attribute livedir of section handler "%s" is not an absolute path!' % section.section_id)


def tearDown():
    for member_name, member in globals().items():
        if member_name.startswith('Handler_'):
            unregister_section_handler(member.section_id)
