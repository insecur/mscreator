# -*- coding: utf-8 -*-
# Copyright (c) 2014-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import logging
import os

import mscreator
from mscreator.config_handlers.kernel import KernelSectionHandler
import mscreator.msclogging
from mscreator import MscConfig
from mscreator.config_handlers.target_chroot import ChrootSectionHandler
from tests.tools import TestBase


logger = logging.getLogger('mscreator')


class TestKernel(TestBase):

    def _set_paths(self, target_directory):
        self.kernel_version = '1.2.3'
        self.bzimage = 'bzImage-%s' % self.kernel_version
        self.initrd = 'initramfs-%s' % self.kernel_version
        self.image_path = os.path.join(self.test_chroot_path, 'boot', 'bzImage-%(kernelversion)s')
        self.image_path_expanded = os.path.join(self.test_chroot_path, 'boot', self.bzimage)
        self.initrd_path = os.path.join(self.test_chroot_path, 'boot', self.initrd)
        self.modules_path = os.path.join(self.test_chroot_path, 'lib', 'modules', self.kernel_version)
        self.target_boot_path = os.path.join(target_directory, 'boot')
        self.target_modules_path = os.path.join(target_directory, 'lib', 'modules', self.kernel_version)

    def test_kernel(self):
        """Check that the kernel files are copied correctly to the target location."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::Kernel::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        cmdline=quiet root=/dev/sda1
        modules_path=%s
        ''' % (target_directory, self.kernel_version, self.image_path, self.initrd_path, self.modules_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()

        self.assertEqual(len(config.sections), 3)
        kernel_section = config.sections['Kernel']
        self.assertIsInstance(config.sections['Chroot'], ChrootSectionHandler)
        self.assertIsInstance(kernel_section, KernelSectionHandler)

        self.assertEqual(kernel_section.kernelversion, self.kernel_version)
        self.assertEqual(kernel_section.image, self.image_path_expanded)
        self.assertEqual(kernel_section.modules_path, self.modules_path)
        self.assertEqual(kernel_section.initrd, self.initrd_path)

        kernel_section.deploy_files(target_directory)
        self.assertTrue(os.path.exists(os.path.join(self.target_boot_path, self.bzimage)))
        self.assertTrue(os.path.exists(os.path.join(self.target_boot_path, self.initrd)))
        self.assertTrue(os.path.exists(self.target_modules_path))

    def test_kernel_dirs_existing_in_target(self):
        """Check that the kernel files are copied correctly to the target location if boot/modules paths already exist."""
        target_directory = self.get_temp_target_directory(create=False)
        self._set_paths(target_directory)

        c = '''
        name=testlinux
        version=1
        targets=Chroot

        ::Chroot::chroot::
        target_directory=%s

        ::Kernel::kernel::
        kernelversion=%s
        image=%s
        initrd=%s
        cmdline=quiet root=/dev/sda1
        modules_path=%s
        ''' % (target_directory, self.kernel_version, self.image_path, self.initrd_path, self.modules_path)

        config = MscConfig(lines=c)
        mscreator.mscglobals.CONFIG = config
        config.parse()
        kernel_section = config.sections['Kernel']

        os.makedirs(self.target_boot_path)
        os.makedirs(self.target_modules_path)
        kernel_section.deploy_files(target_directory)
        self.assertTrue(os.path.exists(os.path.join(self.target_boot_path, self.bzimage)))
        self.assertTrue(os.path.exists(os.path.join(self.target_boot_path, self.initrd)))
        self.assertTrue(os.path.exists(self.target_modules_path))
