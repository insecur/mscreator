# -*- coding: utf-8 -*-
# Copyright (c) 2013-2017, Tobias Hommel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#  * Neither the name of the author nor the names of its contributors may
#    be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""."""

import os
import logging
import time

import mscreator
from mscreator.commands import find, Command, retry_umount
from mscreator.exceptions import CommandError
from mscreator.msclogging import TRACE
from tests.tools import TestBase

logger = logging.getLogger('mscreator')


class TestFind(TestBase):

    @classmethod
    def setUpClass(cls):
        cls.super().setUpClass()
        cls.dirs = set()
        cls.files = set()
        cls.links = set()
        cls.all = set()

        cls.dirs.add(cls.tempdir)

        for a in ('a', 'b', 'c', 'd'):
            ap = os.path.join(cls.tempdir, a)
            os.makedirs(ap)
            cls.dirs.add(ap)
            for b in ('aa', 'bb', 'cc', 'dd'):
                bp = os.path.join(ap, b)
                os.makedirs(bp)
                cls.dirs.add(bp)
                linkname = os.path.join(bp, 'symlink')
                os.symlink('aaa/x.txt', linkname)
                cls.links.add(linkname)
                for c in ('aaa', 'bbb', 'ccc', 'ddd'):
                    cp = os.path.join(bp, c)
                    os.makedirs(cp)
                    cls.dirs.add(cp)

                    f = os.path.join(cp, 'x.txt')
                    with open(f, 'w'):
                        pass
                    cls.files.add(f)

        cls.all.update(cls.dirs)
        cls.all.update(cls.files)
        cls.all.update(cls.links)

    def test_find_directories(self):
        dirlist = set(find(self.tempdir, types=['dir']))
        self.assertSetEqual(dirlist, self.dirs)

    def test_find_files(self):
        filelist = set(find(self.tempdir, types=['file']))
        self.assertSetEqual(filelist, self.files)

    def test_find_files_and_links(self):
        filelist = set(find(self.tempdir, types=['file', 'symlink']))
        self.assertSetEqual(filelist, self.files | self.links)

    def test_find_files_all(self):
        filelist = set(find(self.tempdir))
        self.assertSetEqual(filelist, self.all)

    def test_find_files_unknown_type(self):
        with self.assertRaisesRegex(ValueError, 'unknown filetype specified'):
            # noinspection PyUnusedLocal
            filelist = set(find(self.tempdir, types=['badtype']))

    def test_find_all_default_path(self):
        oldcwd = os.getcwd()
        os.chdir(self.tempdir)
        filelist = set(find(self.tempdir))
        os.chdir(oldcwd)
        self.assertSetEqual(filelist, self.all)


class TestCommand(TestBase):

    def test_stdout_stderr_trace(self):
        oldlevel = mscreator.mscglobals.LOGLEVEL
        mscreator.mscglobals.LOGLEVEL = TRACE
        cmd = Command(command_path='python3')
        with self.assertLogs(logger='mscreator.commands', level=TRACE) as cm:
            cmd(parameters=['-c', 'import sys;print("123");print("234", file=sys.stderr)'])
        self.assertEqual(cm.output[-2:], ["TRACE:mscreator.commands:>O>: 123",
                                          "TRACE:mscreator.commands:>E>: 234"])
        mscreator.mscglobals.LOGLEVEL = oldlevel

    def test_call_failing_command_without_dontfail(self):
        cmd = Command(command_path='false')
        with self.assertRaisesRegex(CommandError, 'Error calling "'):
            cmd()

    def test_call_failing_command_with_dontfail(self):
        cmd = Command(command_path='false')
        cmd(dontfail=True)

    def test_env_value_containing_none(self):
        cmd = Command(command_path='bash', detect=True)
        stdout = cmd(parameters=['-c', 'set -u;echo -n $BLAH'], env={'BLAH': None})
        self.assertEqual(stdout, '')


class TestCommands(TestBase):

    def test_retry_mount_firstfail(self):
        start = time.time()
        try:
            retry_umount('/does_not_exist_or_is_no_mountpoint', retry_timeout=0.3)
        except CommandError:
            pass
        stop = time.time()
        self.assertAlmostEqual(stop - start, 0.3, delta=0.1)
