#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(name='mscreator',
      version='2.0.0',
      description='Minimal System Creator',
      author='Tobias Hommel',
      author_email='mscreator@genoetigt.de',
      url='http://mscreator.genoetigt.de/',
      license='BSD',
      platforms='Linux',
      packages=find_packages(exclude=['tests']),
      data_files=[('/usr/share/vim/vimfiles/syntax', ['misc/mscconf.vim'])],
      requires=[],
      scripts=['mscreate'],
      )
